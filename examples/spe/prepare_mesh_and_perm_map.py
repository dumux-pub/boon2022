import sys

if len(sys.argv) < 3:
    sys.stderr.write("Need two arguments: number of subdomains in x- and y-direction\n")
    sys.exit(1)

base_mesh_file_name = "mesh" if len(sys.argv) < 4 else sys.argv[3]
num_subdomains_x = int(sys.argv[1])
num_subdomains_y = int(sys.argv[2])

perm_file = "spe_perm.dat"  # taken from https://www.spe.org/web/csp/datasets/set02.htm#download
num_cells_x = 60
num_cells_y = 220
num_cells_z = 85
layer_idx = 20
perm_dir = 0

offset = perm_dir*(num_cells_x*num_cells_y*num_cells_z)
perm_values = [float(v) for v in open(perm_file).read().split()]
perm_values = [
    [v for v in perm_values[offset + (layer_idx+i)*num_cells_x:offset + (layer_idx+i+1)*num_cells_x]]
    for i in range(num_cells_y)
]

if num_cells_x%num_subdomains_x != 0:
    sys.stderr.write("Number of subdomains in x-direction not supported")
    sys.exit(1)
if num_cells_y%num_subdomains_y != 0:
    sys.stderr.write("Number of subdomains in y-direction not supported")
    sys.exit(1)

n_cells_x_per_subdomain = int(num_cells_x/num_subdomains_x)
n_cells_y_per_subdomain = int(num_cells_y/num_subdomains_y)

def _get_phys_id_for_subdomain_cell(domain_x_id, domain_y_id, x_idx_cell, y_idx_cell):
    x_idx_0 = domain_x_id*n_cells_x_per_subdomain
    y_idx_0 = domain_y_id*n_cells_y_per_subdomain
    return (y_idx_0 + y_idx_cell)*num_cells_x + x_idx_0 + x_idx_cell

for domain_id_y in range(num_subdomains_y):
    for domain_id_x in range(num_subdomains_x):
        subdomain_id = domain_id_y*num_subdomains_x + domain_id_x
        _nx = n_cells_x_per_subdomain
        _ny = n_cells_y_per_subdomain
        _x_offset = n_cells_x_per_subdomain*domain_id_x
        _y_offset = n_cells_y_per_subdomain*domain_id_y
        with open(f"{base_mesh_file_name}_{subdomain_id}.msh", "w") as mesh_file:
            mesh_file.write("$MeshFormat\n")
            mesh_file.write("2.2 0 8\n")
            mesh_file.write("$EndMeshFormat\n")

            mesh_file.write("$Nodes\n")
            mesh_file.write("{}\n".format((_nx+1)*(_ny+1)))
            for j in range(_ny+1):
                for i in range(_nx+1):
                    mesh_file.write(
                        f"{j*(_nx+1) + i} "
                        f"{_x_offset + float(i)} "
                        f"{_y_offset + float(j)} 0.0\n"
                    )
            mesh_file.write("$EndNodes\n")

            mesh_file.write("$Elements\n")
            mesh_file.write("{}\n".format(_nx*_ny))
            for j in range(_ny):
                for i in range(_nx):
                    mesh_file.write(
                        f"{j*_nx + i} 3 1 "
                        f"{_get_phys_id_for_subdomain_cell(domain_id_x, domain_id_y, i, j)} "
                        f"{j*(_nx+1) + i} {j*(_nx+1) + i + 1} "
                        f"{(j+1)*(_nx+1) + i + 1} {(j+1)*(_nx+1) + i}\n"
                    )
            mesh_file.write("$EndElements\n")

with open("physical_id_to_permeability_map.txt", "w") as phys_id_map:
    phys_id = 0
    for j in range(num_cells_y):
        for i in range(num_cells_x):
            phys_id_map.write(f"{phys_id} {perm_values[j][i]}\n")
            phys_id += 1
