import sys
import os

from subprocess import run
from argparse import ArgumentParser
from configparser import ConfigParser
from math import sqrt

initPath = os.getcwd()
utilFolder = initPath.split('examples')
utilFolder = os.path.join(utilFolder[0], 'examples/util')
sys.path.append(utilFolder)
from decomposition import DomainDecomposition
from mesh import makeMesh, CellType

parser = ArgumentParser(description="Run script for example 3")
parser.add_argument("-d", "--decomposition", required=True, nargs=2)
parser.add_argument("-m", "--num-cells-mortar", required=False, default=10)
args = vars(parser.parse_args())

run([
    "python3",
    "prepare_mesh_and_perm_map.py",
    args["decomposition"][0],
    args["decomposition"][1]
], check = True)

num_subdomains = int(args["decomposition"][0])*int(args["decomposition"][1])
inputFile = ConfigParser()
inputFile.optionxform = str
inputFile.read("params.input")

for _sd_idx in range(num_subdomains):
    inputFile.add_section(f"SubDomain{_sd_idx}")
    inputFile[f"SubDomain{_sd_idx}"]["Grid.File"] = f"mesh_{_sd_idx}.msh"
    inputFile[f"SubDomain{_sd_idx}"]["Problem.Name"] = f"domain_{_sd_idx}"

# make interface meshes
decomposition = DomainDecomposition(
    [0, 0],
    [60, 220],
    [int(v) for v in args["decomposition"]]
)

for _if_idx in range(decomposition.numInterfaces):
    corners = decomposition.getInterfaceCorners(_if_idx)
    assert len(corners) == 2
    v = [corners[1][i] - corners[0][i] for i in range(len(corners[0]))]
    assert len(v) == 2
    length = sqrt(v[0]*v[0] + v[1]*v[1])
    makeMesh(
        decomposition.getInterfaceCorners(_if_idx),
        f"interface_{_if_idx}.msh",
        length/int(args["num_cells_mortar"])
    )
    inputFile.add_section(f"Interface{_if_idx}")
    inputFile[f"Interface{_if_idx}"]["Grid.File"] = f"interface_{_if_idx}.msh"
    inputFile[f"Interface{_if_idx}"]["Problem.Name"] = f"interface_{_if_idx}"
with open("run_params_decomposed.input", "w") as paramFile:
    inputFile.write(paramFile)

print("Running conforming simulation")
run(["make", "spe_mpfa"], check=True)
run(["./spe_mpfa", "run_params_decomposed.input"], check=True)

print("Running conforming simulation")
run(["python3", "prepare_mesh_and_perm_map.py", "1", "1", "conforming"], check = True)
run(["make", "spe_mpfa_conforming"], check=True)
run(["./spe_mpfa_conforming", "params.input", "-Grid.File", "conforming_0.msh"], check=True)
