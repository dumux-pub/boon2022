// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_MORTAR_DARCY_SUBPROBLEM_HH
#define DUMUX_MORTAR_DARCY_SUBPROBLEM_HH

#include <limits>
#include <dune/common/fvector.hh>

#include <dumux/common/numeqvector.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <examples/common/interface.hh>
#include <examples/common/subdomainproblem.hh>

namespace Dumux {

template <class TypeTag>
class SPESubProblem : public SubDomainProblem<TypeTag>
{
    using ParentType = SubDomainProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimension;
    static_assert(dim == 2 || dim == 3);

    static constexpr double min_x_coordinate = 0.;
    static constexpr double max_x_coordinate = 60.0;

public:
    template<typename SpatialParams>
    SPESubProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                  std::shared_ptr<SpatialParams> spatialParams,
                  const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    {
        permeabilities_.resize(fvGridGeometry->gridView().size(0), -1.0);
        markers_.resize(fvGridGeometry->gridView().size(0), -1.0);
        for (const auto& element : elements(fvGridGeometry->gridView()))
        {
            permeabilities_[fvGridGeometry->elementMapper().index(element)] = this->spatialParams().permeability(element);
            markers_[fvGridGeometry->elementMapper().index(element)] = this->spatialParams().getElementDomainMarker(element);
        }
    }

    //! Specifies the type of boundary condition as function of the position.
    BoundaryTypes<1> boundaryTypesAtFace(const Element& element,
                                         const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes<1> values;
        values.setAllNeumann();
        if (scvf.ipGlobal()[0] < min_x_coordinate + 1e-6)
            values.setAllDirichlet();
        if (scvf.ipGlobal()[0] > max_x_coordinate - 1e-6)
            values.setAllDirichlet();
        return values;
    }

    //! Evaluate the Dirichlet boundary condition at a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        if (globalPos[0] < min_x_coordinate + 1e-6)
            return NumEqVector(1.0);
        return NumEqVector(0.0);
    }

    template<class OutputModule>
    void addOutputFields(OutputModule& outputModule) const
    {
        ParentType::addOutputFields(outputModule);
        outputModule.addField(
            permeabilities_,
            "permeability",
            OutputModule::FieldType::element
        );
        outputModule.addField(
            markers_,
            "domainmarker",
            OutputModule::FieldType::element
        );
    }

private:
    std::vector<Scalar> permeabilities_;
    std::vector<Scalar> markers_;
};

} // end namespace Dumux

#endif
