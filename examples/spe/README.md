Example 3: based on SPE10
=========================

To reproduce the results for this example, go to
the build directory for this example and execute `python3 run.py --decomposition 3 5`.
To visualize the results, you may use the provided ParaView state files (files with
extension `.pvsm`).
