// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief The spatial parameters class for the test problem using the 1p model.
 */
#ifndef DUMUX_MORTAR_DARCY_SPATIALPARAMS_HH
#define DUMUX_MORTAR_DARCY_SPATIALPARAMS_HH

#include <dune/common/exceptions.hh>

#include <dumux/io/grid/griddata.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {
namespace Detail {

std::string rstrip(std::string& s, const std::string& characters)
{ return s.substr(0, s.find_last_not_of(characters)); }

std::string lstrip(std::string& s, const std::string& characters)
{ return s.substr(s.find_first_not_of(characters), std::string::npos); }

std::vector<std::string> readLineValues(const std::string& line,
                                        const std::string& separator = " ")
{
    std::vector<std::string> result;

    std::size_t start = 0;
    std::size_t foundIdx = line.find_first_of(separator, start);

    const auto process = [&] (std::size_t endIdx) {
        const auto numChars = endIdx - start;
        if (numChars > 0)
        {
            auto current = line.substr(start, numChars);
            lstrip(current, " "); rstrip(current, " \n");
            if (current.size() > 0)
                result.push_back(std::move(current));
        }
    };

    while (foundIdx != std::string::npos)
    {
        process(foundIdx);
        start = foundIdx + 1;
        foundIdx = line.find_first_of(separator, start);
    }

    process(line.size());
    return result;
}

} // end namespace Detail

/*!
 * \ingroup BoundaryTests
 * \brief The spatial parameters class for the test problem using the 1p model.
 */
template<class GridGeometry, class Scalar>
class OnePDarcySpatialParams
: public FVSpatialParamsOneP<GridGeometry, Scalar,
                             OnePDarcySpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using ParentType = FVSpatialParamsOneP<GridGeometry, Scalar,
                                           OnePDarcySpatialParams<GridGeometry, Scalar>>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridData = Dumux::GridData<typename GridView::Grid>;

public:
    //! Export permeability type
    using PermeabilityType = Scalar;

    //! Markers of the different domains
    static constexpr int faultMarker = 1;
    static constexpr int barrierMarker = 2;
    static constexpr int layerMarker = 3;

    //! The constructor with grid data
    OnePDarcySpatialParams(std::shared_ptr<const GridGeometry> fvGridGeometry,
                           const std::string& paramGroup,
                           std::shared_ptr<const GridData> gridData)
    : ParentType(fvGridGeometry)
    , gridData_(gridData)
    {
        domainMarkerToPermeability_.reserve(fvGridGeometry->gridView().size(0));

        std::size_t curIdx = 0;
        std::ifstream permFile(getParamFromGroup<std::string>(paramGroup, "SpatialParams.PhysicalIndexToPermeabilityMapFile"));
        std::string line;
        while (std::getline(permFile, line))
        {
            const auto lineValues = Detail::readLineValues(line);
            if (std::atoi(lineValues[0].c_str()) != curIdx++)
                DUNE_THROW(Dune::InvalidStateException, "Wrong id");
            domainMarkerToPermeability_.push_back(std::atof(lineValues[1].c_str()));
        }
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     */
    template<class SubControlVolume, class ElemSol>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElemSol& elemSol) const
    { return permeability(element); }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     */
    PermeabilityType permeability(const Element& element) const
    { return domainMarkerToPermeability_[getElementDomainMarker(element)]; }

    /*!
     * \brief Defines the porosity in [-].
     * \param globalPos The global position
     * \returns the porosity
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

    /*!
     * \brief Return the domain marker of an element.
     */
    int getElementDomainMarker(const Element& element) const
    {
        if (!gridData_)
            DUNE_THROW(Dune::InvalidStateException, "Grid data not available");

        return gridData_->getElementDomainMarker(element);
    }

private:
    std::vector<Scalar> domainMarkerToPermeability_;
    std::shared_ptr<const GridData> gridData_;
};

} // end namespace Dumux

#endif
