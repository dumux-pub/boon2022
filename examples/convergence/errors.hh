// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \todo TODO: Doc me
 */
#ifndef DUMUX_MORTAR_CONVERGENCE_ERRORS_HH
#define DUMUX_MORTAR_CONVERGENCE_ERRORS_HH

#include <cmath>

#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dumux/common/integrate.hh>
#include <dumux/common/parameters.hh>
#include <dumux/geometry/diameter.hh>

#include <examples/common/types.hh>

namespace Dumux {

struct SubDomainErrors
{
    double minElementSize;
    double maxElementSize;
    double pressure;
    double flux;
    double interfaceFlux;
};

template<class SubDomainSolver>
SubDomainErrors computeSubdomainErrors(ContainerOfPointers<SubDomainSolver> sdSolvers, int order)
{
    static constexpr int sdDim = SubDomainSolver::GridGeometry::GridView::dimension;
    static constexpr int mortarDim = sdDim - 1;

    double minElementSize = std::numeric_limits<double>::max();
    double maxElementSize = 0;
    double sumSDFluxNorms = 0.0;
    double sumSDIFFluxNorms = 0.0;
    double sumSDPressureNorms = 0.0;

    for (auto sdSolverPtr : sdSolvers)
    {
        const auto& sdGridGeometry = sdSolverPtr->gridGeometry();
        const auto& sdProblem = sdSolverPtr->problem();
        const auto& x = sdSolverPtr->solution();

        // store the area of the grid facets over which it was integrated
        std::vector<double> gridFacetAreas(sdGridGeometry.gridView().size(1), 0.0);

        typename SubDomainSolver::SolutionVector pExact(sdGridGeometry.gridView().size(0));
        for (const auto& element : elements(sdGridGeometry.gridView()))
        {
            using std::max;
            using std::min;
            const auto curElemSize = diameter(element.geometry());
            maxElementSize = max(maxElementSize, curElemSize);
            minElementSize = min(minElementSize, curElemSize);

            const auto eIdxGlobal = sdGridGeometry.elementMapper().index(element);
            pExact[eIdxGlobal] = sdProblem.exact(element.geometry().center());

            auto fvGeometry = localView(sdGridGeometry);
            auto elemVolVars = localView(sdSolverPtr->gridVariables().curGridVolVars());
            auto elemFluxVarsCache = localView(sdSolverPtr->gridVariables().gridFluxVarsCache());

            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            // integrate error in pressure
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& scvGeometry = scv.geometry();
                const auto& rule = Dune::QuadratureRules<double, sdDim>::rule(scvGeometry.type(), order);
                for (const auto& qp : rule)
                {
                    const auto& ipLocal = qp.position();
                    const auto& ipGlobal = scvGeometry.global(ipLocal);
                    const auto error = elemVolVars[scv].pressure() - sdSolverPtr->problem().exact(ipGlobal);
                    sumSDPressureNorms += error*error*scvGeometry.integrationElement(ipLocal)*qp.weight();
                }
            }

            double elemFluxError = 0.0;
            for (const auto& scvf : scvfs(fvGeometry))
            {
                double discreteFlux;
                if (scvf.boundary() && sdSolverPtr->problem().boundaryTypes(element, scvf).hasOnlyNeumann())
                    discreteFlux = sdProblem.neumann(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf)
                                   /elemVolVars[scvf.insideScvIdx()].density()*scvf.area();
                else
                {
                    typename SubDomainSolver::FluxVariables fluxVars;
                    fluxVars.init(sdProblem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    discreteFlux = fluxVars.advectiveFlux(0, [] (const auto& vv) { return vv.mobility(); });
                }

                // compute flux error
                discreteFlux /= scvf.area();
                double scvfError = 0.0;
                const auto& scvfGeometry = scvf.geometry();
                const auto& rule = Dune::QuadratureRules<double, mortarDim>::rule(scvfGeometry.type(), order);
                for (const auto& qp : rule)
                {
                    const auto& ipLocal = qp.position();
                    const auto& ipGlobal = scvfGeometry.global(ipLocal);
                    const auto error = discreteFlux - sdSolverPtr->problem().exactFlux(ipGlobal)*scvf.unitOuterNormal();
                    scvfError += error*error*qp.weight()*scvfGeometry.integrationElement(ipLocal);
                }

                elemFluxError += scvfError/scvf.area();
                if (scvf.boundary() && sdSolverPtr->problem().isOnMortarInterface(element, scvf))
                    sumSDIFFluxNorms += scvfError;
            }

            elemFluxError *= element.geometry().volume();
            sumSDFluxNorms += elemFluxError;
        }
    }

    using std::sqrt;
    sumSDFluxNorms = sqrt(sumSDFluxNorms);
    sumSDIFFluxNorms = sqrt(sumSDIFFluxNorms);
    sumSDPressureNorms = sqrt(sumSDPressureNorms);

    return SubDomainErrors{
        minElementSize,
        maxElementSize,
        sumSDPressureNorms,
        sumSDFluxNorms,
        sumSDIFFluxNorms
    };
}

template<class Interface, class InterfaceSolution>
double computeInterfaceError(ContainerOfPointers<Interface> interfaces,
                             const Container<InterfaceSolution>& solutions,
                             int order)
{
    using InterfaceGridView = typename Interface::Traits::GridView;

    double sumMortarNorm = 0.0;
    for (auto ifPtr : interfaces)
    {
        // determine normal vector on interface (assumes no kinks per interface!)
        Dune::FieldVector<double, InterfaceGridView::dimensionworld> normal;
        const auto& posGG = ifPtr->positiveNeighborSolver().gridGeometry();
        const auto& posProblem = ifPtr->positiveNeighborSolver().problem();

        bool found = false;
        for (const auto& element : elements(posGG.gridView()))
        {
            auto fvGeometry = localView(posGG);
            fvGeometry.bindElement(element);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto mortarId = posProblem.mortarInterfaceIndex(element, scvf);
                if (mortarId && *mortarId == ifPtr->id())
                { normal = scvf.unitOuterNormal(); found = true; break; }
            }

            if (found) break;
        }
        if (!found)
            DUNE_THROW(Dune::InvalidStateException, "Could not determine normal vector");

        const auto& ifBasis = ifPtr->gridGeometry().feBasis();
        auto exactIF = [&] (const auto& pos) { return posProblem.exactFlux(pos)*normal; };
        auto analyticIFGF = Dune::Functions::makeAnalyticGridViewFunction(exactIF, ifBasis.gridView());
        auto discreteGF = Dune::Functions::makeDiscreteGlobalBasisFunction<typename Interface::Traits::BlockType>(
            ifPtr->gridGeometry().feBasis(),
            solutions[ifPtr->id()]
        );
        const auto norm = Dumux::integrateL2Error(
            ifBasis.gridView(), analyticIFGF, discreteGF, order
        );
        sumMortarNorm += norm*norm;
    }

    using std::sqrt;
    return sqrt(sumMortarNorm);
}

struct Errors : public SubDomainErrors
{
    double interface;
};

template<class SubDomainSolver, class Interface, typename InterfaceSolutions>
Errors computeErrors(ContainerOfPointers<SubDomainSolver> sdSolvers,
                     ContainerOfPointers<Interface> interfaces,
                     const InterfaceSolutions& ifSolutions)
{
    const int order = getParam<int>("L2Error.IntegrationOrder");
    const auto sdErrors = computeSubdomainErrors(sdSolvers, order);
    const auto ifError = computeInterfaceError(interfaces, ifSolutions, order);

    return Errors{
        sdErrors.minElementSize,
        sdErrors.maxElementSize,
        sdErrors.pressure,
        sdErrors.flux,
        sdErrors.interfaceFlux,
        ifError
    };
}

} // end namespace Dumux

#endif //DUMUX_DARCY_SUBPROBLEM_HH
