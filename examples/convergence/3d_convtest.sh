#!/bin/bash

NUM_REF=3
python3 convergencetest.py -r $NUM_REF -d 2 2 2 -o 1 -m cube -s mpfa -t results_cube
python3 convergencetest.py -r $NUM_REF -d 2 2 2 -o 1 -m tet -s mpfa -t results_tet
python3 convergencetest.py -r $NUM_REF -d 2 2 2 -o 1 -m hex -s mpfa -t results_hex
