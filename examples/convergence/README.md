Example 1: convergence test 
=========================

To reproduce the results for the convergence tests in 2d and 3d, go to
the build directory for this example and execute the script `2d_convtest.sh`
and `3d_convtest.sh`, respectively. This will produce subfolders with the
results for different meshes. For visualization, you can use the provided ParaView
state files (files with extension `.pvsm`) by opening ParaView from within
a folder with results and then loading the state (the state files assume all
results files to be present in the folder from which ParaView was launched).
