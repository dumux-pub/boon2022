// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \todo TODO: doc me
 */
#include <config.h>

#include <examples/common/initialization.hh>
#include <examples/common/interface.hh>
#include <examples/common/subdomainsolver.hh>
#include <examples/common/solver.hh>
#include <examples/common/types.hh>

#include "properties.hh"
#include "errors.hh"

#ifndef MORTARBASISORDER
#define MORTARBASISORDER 1
#endif

namespace Dumux {

template<typename SubDomainSolver, typename Interface, typename InterfaceSolution>
void writeErrors(const ContainerOfPointers<SubDomainSolver>& sdSolvers,
                 const ContainerOfPointers<Interface>& interfaces,
                 const InterfaceSolution& ifSolution,
                 int numberOfIterations)
{
    Container<InterfaceSolution> interfaceSolutions;
    extractInterfaceSolutions(interfaces, interfaceSolutions, ifSolution);
    const auto errors = computeErrors(sdSolvers, interfaces, interfaceSolutions);
    std::ofstream resultsFile(getParam<std::string>("Output.ResultsFile"), std::ios::app);
    resultsFile << errors.minElementSize << ","
                << errors.maxElementSize << ","
                << errors.pressure << ","
                << errors.flux << ","
                << errors.interfaceFlux << ","
                << errors.interface << ","
                << numberOfIterations << std::endl;
}

template<class SubDomainSolver, class Interface>
auto solveAndWriteOutput(ContainerOfPointers<SubDomainSolver> subDomainSolvers,
                         ContainerOfPointers<Interface> interfaces)
{
    using InterfaceSolution = typename Interface::Traits::SolutionVector;

    std::vector<InterfaceSolution> interfaceSolutions;
    IterativeMortarSolver<InterfaceSolution> solver;
    const auto [interfaceSolution, iterations] = solver.solve(interfaces, subDomainSolvers);
    extractInterfaceSolutions(interfaces, interfaceSolutions, interfaceSolution);

    std::vector<InterfaceSolution> exactSolutions(subDomainSolvers.size());
    std::for_each(subDomainSolvers.begin(), subDomainSolvers.end(), [&] (auto ptr) {
        exactSolutions[ptr->id()].resize(ptr->gridGeometry().gridView().size(0));
        for (const auto& element : elements(ptr->gridGeometry().gridView()))
            exactSolutions[ptr->id()][ptr->gridGeometry().elementMapper().index(element)]
                = ptr->problem().exact(element.geometry().center());
    });

    MortarVTKOutput vtkWriter(subDomainSolvers, interfaces);
    vtkWriter.addSubDomainFields("p_exact", exactSolutions);
    vtkWriter.addInterfaceFields("mortar_variable", interfaceSolutions);
    vtkWriter.write();
    return std::make_pair(interfaceSolution, iterations);
}

template<class Projector, class SubDomainSolver, class InterfaceTraits>
void solveIteratively()
{
    auto [sdSolvers, interfaces] = makeSubDomainSolversAndInterfaces<Projector,
                                                                     InterfaceTraits,
                                                                     SubDomainSolver>();
    auto [interfaceSolution, iterations] = solveAndWriteOutput(sdSolvers, interfaces);
    writeErrors(sdSolvers, interfaces, interfaceSolution, iterations);
}

} // namespace Dumux

struct ConvergenceTestInterfaceTraits
{
    static constexpr int basisOrder = MORTARBASISORDER;
    using Scalar = double;
    using Grid = MORTARGRIDTYPE;
    using GridView = typename Grid::LeafGridView;
    using BlockType = Dune::FieldVector<Scalar, 1>;
    using SolutionVector = Dune::BlockVector<BlockType>;
    using FEBasis = Dune::Functions::LagrangeBasis<GridView, basisOrder>;
    using GridGeometry = Dumux::FEGridGeometry<FEBasis>;
    static constexpr Dune::VTK::DataMode VTKDataMode = Dune::VTK::conforming;
};

int main(int argc, char** argv)
{
    using namespace Dumux;

    // we use UMFPack here because on very fine grids the iterative solvers struggle
    using InterfaceTraits = ConvergenceTestInterfaceTraits;
    using SubDomainSolver =  Dumux::SubDomainSolver<
        Properties::TTag::TYPETAG,
        Dumux::UMFPackBackend
    >;

    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);
    Parameters::init(argc, argv);

    const auto projectorType = getParam<std::string>("Mortar.ProjectorType");
    if (projectorType == "Flat")
    {
        using P = FlatProjector<typename InterfaceTraits::SolutionVector>;
        solveIteratively<P, SubDomainSolver, InterfaceTraits>();
    }
    else if (projectorType == "Sharp")
    {
        using P = SharpProjector<typename InterfaceTraits::SolutionVector>;
        solveIteratively<P, SubDomainSolver, InterfaceTraits>();
    }
    else
        DUNE_THROW(Dune::NotImplemented, "Unknown projector type");

    if (mpiHelper.rank() == 0)
        Parameters::print();

    return 0;
}
