// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_MORTAR_DARCY_SUBPROBLEM_HH
#define DUMUX_MORTAR_DARCY_SUBPROBLEM_HH

#include <limits>
#include <dune/common/fvector.hh>

#include <dumux/common/numeqvector.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <examples/common/subdomainproblem.hh>

namespace Dumux {

template <class TypeTag>
class ConvergenceSubProblem : public SubDomainProblem<TypeTag>
{
    using ParentType = SubDomainProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr int dim = GridView::dimension;
    static_assert(dim == 2 || dim == 3);

public:
    template<typename SpatialParams>
    ConvergenceSubProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                          std::shared_ptr<SpatialParams> spatialParams,
                          const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    {}

    BoundaryTypes<1> boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes<1> values;
        values.setAllDirichlet();
        return values;
    }

    NumEqVector exact(const GlobalPosition& globalPos) const
    {
        static_assert(dim == 2 || dim == 3);

        using std::sin;
        using std::cos;

        const auto x = globalPos[0];
        const auto y = globalPos[1];

        Scalar p = y*y*(1.0 - 1.0/3.0*y) + x*(2.0-x)*y*sin(2.0*M_PI*x);
        if constexpr (dim == 3)
            p *= cos(2.0*M_PI*globalPos[2]);
        return NumEqVector(p);
    }

    GlobalPosition exactFlux(const GlobalPosition& globalPos) const
    {
        static_assert(dim == 2 || dim == 3);

        using std::sin;
        using std::cos;

        const auto x = globalPos[0];
        const auto y = globalPos[1];
        const auto& k = this->spatialParams().permeabilityAtPos(globalPos);
        static const Scalar mu = getParam<Scalar>("0.Component.LiquidKinematicViscosity");

        GlobalPosition result(0.0);
        result[0] = -1.0*k/mu*(y*((2.0-2.0*x)*sin(2.0*x*M_PI) - 2.0*M_PI*(x-2.0)*x*cos(2.0*M_PI*x)));
        result[1] = -1.0*k/mu*(-(x-2.0)*x*sin(2*M_PI*x) - (y-2.0)*y);

        if constexpr (dim == 3)
        {
            const auto cos2PiZ = cos(2.0*M_PI*globalPos[2]);
            const auto sin2PiZ = sin(2.0*M_PI*globalPos[2]);
            result *= cos2PiZ;
            result[2] = 2.0*M_PI*k/mu*sin2PiZ*exact(globalPos)/cos2PiZ;
        }

        return result;
    }

    NumEqVector sourceAtPos(const GlobalPosition& globalPos) const
    {
        static_assert(dim == 2 || dim == 3);

        using std::sin;
        using std::cos;

        const auto x = globalPos[0];
        const auto y = globalPos[1];
        const auto& k = this->spatialParams().permeabilityAtPos(globalPos);

        static const Scalar rho = getParam<Scalar>("0.Component.LiquidDensity");
        static const Scalar mu = getParam<Scalar>("0.Component.LiquidKinematicViscosity");
        NumEqVector result(0.0);
        result += 2.0*k*rho/mu*( (y - 2.0*M_PI*M_PI*(x-2.0)*x*y)*sin(2.0*M_PI*x)
                  + 2.0*M_PI*(2.0*x-2.0)*y*cos(2.0*M_PI*x) + y - 1.0);

        if constexpr (dim == 3)
        {
            result *= cos(2.0*M_PI*globalPos[2]);
            result += 4.0*M_PI*M_PI*k*rho/mu*exact(globalPos);
        }

        return result;
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return exact(globalPos); }
};

} // end namespace Dumux

#endif //DUMUX_DARCY_SUBPROBLEM_HH
