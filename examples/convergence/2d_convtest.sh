#!/bin/bash

NUM_REF=5
python3 convergencetest.py -r $NUM_REF -o 1 -d 3 3 -m quad -s mpfa -t results_quad
python3 convergencetest.py -r $NUM_REF -o 1 -d 3 3 -m tria -s mpfa -t results_tria
python3 convergencetest.py -r $NUM_REF -o 1 -d 3 3 -m rectangle -s mpfa -t results_rect
