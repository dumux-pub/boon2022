import os
import sys
import math
import argparse
import subprocess
import numpy as np
from copy import deepcopy
from shutil import copy

initPath = os.getcwd()
utilFolder = initPath.split('examples')
utilFolder = os.path.join(utilFolder[0], 'examples/util')
sys.path.append(utilFolder)
from decomposition import DomainDecomposition
from mesh import makeMesh, CellType

if sys.version_info < (3, 0):
    sys.stderr.write("Python 3 required\n")
    sys.exit(1)

parser = argparse.ArgumentParser(
    description='Convergence test with sharp and flat projections. '
                'Works in 2 or 3-dimensional domains with tpfa or mpfa as '
                'discretization schemes.'
)

parser.add_argument('-r', '--numRefinements', required=True, type=int,
                    help="Specify the number of refinements")

parser.add_argument('-s', '--scheme', required=True, choices=['tpfa', 'mpfa'],
                    help="Specify the discretization scheme to be used")

parser.add_argument('-t', '--targetDirectory', required=False, default='.',
                    help="Set the directory in which to place result files")

parser.add_argument('-o', '--mortarOrder', required=True,
                    help="Define the polynomial order used for the mortar variable")

parser.add_argument('-d', '--decomposition', required=True, nargs='*',
                    help="Define the number of subdomains per direction")

parser.add_argument('-m', '--meshType', required=True,
                    choices=['rectangle', 'tria',
                             'quad', 'tet', 'cube', 'hex'],
                    help="Specify the type of mesh to be used\n"
                         "\t- rectangle: structured rectilinear grid\n"
                         "\t- tria: unstructured triangular grid\n"
                         "\t- quad: general quadrilateral grid\n"
                         "\t- tet: unstructured tetrahedral grid\n"
                         "\t- cube: structured cube grid\n"
                         "\t- hex: unstructured hexahedral grid\n")

args = vars(parser.parse_args())
numRefinements = args['numRefinements']
schemeName = args['scheme']
meshType = args['meshType']
decomp = args['decomposition']

# select grid suffix of exe depending on input
gridInfoMap = {'rectangle': {'suffix': 'quad', 'dim': 2, 'ct': CellType.rectangle},
               'quad':      {'suffix': 'quad', 'dim': 2, 'ct': CellType.quadrilateral},
               'tria':      {'suffix': 'tria', 'dim': 2, 'ct': CellType.triangle},
               'tet':       {'suffix': 'tet',  'dim': 3, 'ct': CellType.tetrahedron},
               'cube':      {'suffix': 'hex',  'dim': 3, 'ct': CellType.cube},
               'hex':       {'suffix': 'hex',  'dim': 3, 'ct': CellType.hexahedron}}
gridSuffix = gridInfoMap[meshType]['suffix']
dimension = gridInfoMap[meshType]['dim']
mortarOrder = args['mortarOrder']
if len(decomp) != dimension:
    sys.exit(
        f"The chosen decomposition has {len(decomp)} entries, but "
        f"your choice of element uses a space dimension of {dimension}"
    )

print("\nCompiling the executable\n")
exeName = f"convergence_{schemeName}_{gridSuffix}_order_{mortarOrder}"
subprocess.run(["make", exeName], check=True)
print("\n")

targetRelPath = os.path.relpath(initPath, args['targetDirectory'])
exePath = os.path.join(initPath, exeName)
exeRelPath = os.path.relpath(exePath, args['targetDirectory'])

if not os.path.exists(args['targetDirectory']):
    print("Target directory does not yet exist. Creating it...")
    os.mkdir(args['targetDirectory'])
elif not os.path.isdir(args['targetDirectory']):
    sys.exit("Error: a file with the same name as the target directory exists")
else:
    print("Warning: target folder exists! Data might be overwritten...")

os.chdir(args['targetDirectory'])

lowerLeft = [0.0 for _ in range(dimension)]
upperRight = [2.0 for _ in range(dimension)]
decomposition = DomainDecomposition(lowerLeft, upperRight, [int(v) for v in decomp])
subDomainDeltaX = (upperRight[0] - lowerLeft[0])/float(decomp[0])
initialInterfaceMeshSize = subDomainDeltaX*0.5 if dimension == 3 else subDomainDeltaX/3.0
possibleSubDomainMeshSizes = [
    subDomainDeltaX/3.0 if dimension == 3 else subDomainDeltaX/6.0,
    subDomainDeltaX/5.0 if dimension == 3 else subDomainDeltaX/8.0
]

# memory consumption gets very large for tets, use coarser meshes
if gridInfoMap[meshType]["ct"] == CellType.tetrahedron:
    possibleSubDomainMeshSizes = [subDomainDeltaX/2.0, subDomainDeltaX/3.0]

def _invert_bit_pattern(pattern):
    return [abs(bit - 1) for bit in pattern]

def _make_alternating_bit_pattern_x():
    result = []
    for i in range(int(decomp[0])):
        result.append(i%2)
    return result

def _make_alternating_bit_pattern_xy():
    result = []
    for i in range(int(decomp[1])):
        current = _make_alternating_bit_pattern_x()
        if i%2 == 1:
            current = _invert_bit_pattern(current)
        result.extend(current)
    return result

def _make_alternating_bit_pattern_xyz():
    if dimension == 2:
        return _make_alternating_bit_pattern_xy()
    result = []
    for i in range(int(decomp[2])):
        current = _make_alternating_bit_pattern_xy()
        if i%2 == 1:
            current = _invert_bit_pattern(current)
        result.extend(current)
    return result

initialSubDomainMeshSizes = [possibleSubDomainMeshSizes[i] for i in _make_alternating_bit_pattern_xyz()]

sdCellType = gridInfoMap[meshType]['ct']
ifaceCellType = CellType.segment

# in 2d, let gmsh do the quads by recombining triangles
if dimension == 2 and sdCellType == CellType.quadrilateral:
    sdCellType = CellType.recombinedTriangle

# choose a suitable interface mesh in 3d
if dimension == 3:
    if sdCellType == CellType.cube:
        ifaceCellType = CellType.rectangle
    else:
        ifaceCellType = CellType.quadrilateral

numSubDomains = decomposition.numSubDomains
for sdIndex in range(numSubDomains):
    dx = initialSubDomainMeshSizes[sdIndex%len(initialSubDomainMeshSizes)]
    makeMesh(
        decomposition.getSubDomainCorners(sdIndex),
        f"subdomain_{sdIndex}",
        dx, sdCellType, numRefinements
    )

numInterfaces = decomposition.numInterfaces
for ifaceIndex in range(numInterfaces):
    dx = initialInterfaceMeshSize
    makeMesh(
        decomposition.getInterfaceCorners(ifaceIndex),
        f"interface_{ifaceIndex}.msh",
        dx, ifaceCellType, numRefinements
    )

inputFileName = os.path.join(targetRelPath, 'params.input')
resultsFileFlat = 'results_flat.csv'
resultsFileSharp = 'results_sharp.csv'
for resultsFileName in [resultsFileFlat, resultsFileSharp]:
    with open(resultsFileName, "w") as resultsFile:
        resultsFile.write('hmin,hmax,p,q,q_if,lambda,it\n')

for refIdx in range(0, numRefinements+1):
    runTimeArgs = []
    runTimeArgs.extend(["./" + exeRelPath])
    runTimeArgs.append(inputFileName)

    for i in range(0, decomposition.numInterfaces):
        runTimeArgs.append(f"-Interface{i}.Grid.File")
        runTimeArgs.append(f"interface_{i}_ref_{refIdx}.msh")

    for i in range(0, numSubDomains):
        runTimeArgs.append(f'-SubDomain{i}.Grid.File')
        runTimeArgs.append(f"subdomain_{i}_ref_{refIdx}.msh")

    for proj, resultsFile in zip(["Flat", "Sharp"], [resultsFileFlat, resultsFileSharp]):
        dumuxArgs = deepcopy(runTimeArgs)
        for i in range(0, numSubDomains):
            dumuxArgs.append(f"-SubDomain{i}.Problem.Name")
            dumuxArgs.append(f"subdomain{i}_{refIdx}_{proj}_{schemeName}")
        for i in range(0, numInterfaces):
            dumuxArgs.append(f"-Interface{i}.Problem.Name")
            dumuxArgs.append(f"interface{i}_{refIdx}_{proj}_{schemeName}")
        dumuxArgs.append("-Mortar.ProjectorType")
        dumuxArgs.append(proj)
        dumuxArgs.append("-Output.ResultsFile")
        dumuxArgs.append(resultsFile)

        print(f"\n\nSolving with {proj} projection for refinement {refIdx}")
        subprocess.run(dumuxArgs, check=True)

# read in the errors
errFlat = np.genfromtxt(resultsFileFlat, delimiter=',', names=True)
errSharp = np.genfromtxt(resultsFileSharp, delimiter=',', names=True)

rates = {}
rates['flat'] = {'p': [], 'q': [], 'q_if': [], 'lambda': []}
rates['sharp'] = {'p': [], 'q': [], 'q_if': [], 'lambda': []}

for i in range(0, numRefinements):
    deltaH = np.log(errFlat['hmin'][i+1]) - np.log(errFlat['hmin'][i])

    deltaEP = np.log(errFlat['p'][i+1]) - np.log(errFlat['p'][i])
    deltaEF = np.log(errFlat['q'][i+1]) - np.log(errFlat['q'][i])
    deltaEIF = np.log(errFlat['q_if'][i+1]) - np.log(errFlat['q_if'][i])
    deltaEM = np.log(errFlat['lambda'][i+1]) - np.log(errFlat['lambda'][i])

    rates['flat']['p'].append(deltaEP / deltaH)
    rates['flat']['q'].append(deltaEF / deltaH)
    rates['flat']['q_if'].append(deltaEIF / deltaH)
    rates['flat']['lambda'].append(deltaEM / deltaH)

    deltaEP = np.log(errSharp['p'][i+1]) - np.log(errSharp['p'][i])
    deltaEF = np.log(errSharp['q'][i+1]) - np.log(errSharp['q'][i])
    deltaEIF = np.log(errSharp['q_if'][i+1]) - np.log(errSharp['q_if'][i])
    deltaEM = np.log(errSharp['lambda'][i+1]) - np.log(errSharp['lambda'][i])

    rates['sharp']['p'].append(deltaEP / deltaH)
    rates['sharp']['q'].append(deltaEF / deltaH)
    rates['sharp']['q_if'].append(deltaEIF / deltaH)
    rates['sharp']['lambda'].append(deltaEM / deltaH)

# print results to terminal
print("mesh sizes:     ", errFlat['hmin'])

print("\n")
print("Flat pressure:  ", rates['flat']['p'])
print("Flat flux:      ", rates['flat']['q'])
print("Flat ifFlux:    ", rates['flat']['q_if'])
print("Flat mortar:    ", rates['flat']['lambda'])

print("\n")
print("Sharp pressure: ", rates['sharp']['p'])
print("Sharp flux:     ", rates['sharp']['q'])
print("Sharp ifFlux:   ", rates['sharp']['q_if'])
print("Sharp mortar:   ", rates['sharp']['lambda'])


# write .tex table with the results #
meshDescriptionDict = {
    'rectangle': "structured rectilinear mesh",
    'tria': "unstructured triangular mesh",
    'quad': "unstructured quadrilateral mesh",
    'tet': "unstructured tetrahedral mesh",
    'hex': "unstructured hexahedral mesh",
    'cube': "structured cube mesh"
}
meshInfo = meshDescriptionDict[meshType]

mOrder = str(args['mortarOrder'])
initMortarMeshSizeString = "{:.2}".format(initialInterfaceMeshSize)
csvPath = os.path.join(os.getcwd(), 'table_' + meshType + '_' + schemeName + '.tex')

with open(csvPath, 'w') as tableFile:
    tableFile.write(r"\begin{table}[h]" + "\n")
    tableFile.write(r"    {\footnotesize" + "\n")
    tableFile.write(r"    \caption{\textbf{Convergence test using " + schemeName + " on a " + meshInfo + "}. ")
    tableFile.write(r"Errors and convergence rates for $h_\Gamma^0 = " + initMortarMeshSizeString + r"$ and $\mathcal{P}_" + mOrder + "$ mortars.}" + "\n")
    tableFile.write(r"    \label{tab:errorsRates}" + "\n")
    tableFile.write(r"    \begin{center}" + "\n")
    tableFile.write(r"    \begin{tabular}{l |l l |l l |l l |l l |l}" + "\n")
    tableFile.write(r"        \toprule" + "\n")
    tableFile.write(r"        $h_{\mathrm{min}}$ & $e_u^\flat$ & $r_u^\flat$ & $e_p^\flat$ & $r_p^\flat$ & $e_\lambda^\flat$ & $r_\lambda^\flat$ & $e_{\mathcal{Q}\lambda}^\flat$ & $r_{\mathcal{Q}\lambda}^\flat$ & $\#it$ \\" + "\n")
    tableFile.write(r"        \midrule" + "\n")

    # write results for flat projection
    for i in range(0, numRefinements+1):
        tableFile.write("\t{:.2e}".format(errFlat['hmin'][i]) + " & ")

        tableFile.write("{:.2e}".format(errFlat['q'][i]) + " & ")
        if i > 0: tableFile.write("{:.2f}".format(rates['flat']['q'][i-1]) + " & ")
        else: tableFile.write("     & ")

        tableFile.write("{:.2e}".format(errFlat['p'][i]) + " & ")
        if i > 0:  tableFile.write("{:.2f}".format(rates['flat']['p'][i-1]) + " & ")
        else: tableFile.write("     & ")

        tableFile.write("{:.2e}".format(errFlat['lambda'][i]) + " & ")
        if i > 0: tableFile.write("{:.2f}".format(rates['flat']['lambda'][i-1]) + " & ")
        else: tableFile.write("     & ")

        tableFile.write("{:.2e}".format(errFlat['q_if'][i]) + " & ")
        if i > 0:  tableFile.write("{:.2f}".format(rates['flat']['q_if'][i-1]) + " & ")
        else: tableFile.write("     & ")

        tableFile.write("{:.0f}".format(errFlat['it'][i]))
        tableFile.write(r" \\" + "\n")

    tableFile.write(r"        \toprule" + "\n")
    tableFile.write(r"        $h_{\mathrm{min}}$ & $e_u^\sharp$ & $r_u^\flat$ & $e_p^\sharp$ & $r_p^\sharp$ & $e_\lambda^\sharp$ & $r_\lambda^\sharp$ & $e_{\mathcal{Q}\lambda}^\sharp$ & $r_{\mathcal{Q}\lambda}^\sharp$ & $\#it$ \\" + "\n")
    tableFile.write(r"        \midrule" + "\n")

    # write results for sharp projection
    for i in range(0, numRefinements+1):
        tableFile.write("\t{:.2e}".format(errSharp['hmin'][i]) + " & ")

        tableFile.write("{:.2e}".format(errSharp['q'][i]) + " & ")
        if i > 0: tableFile.write("{:.2f}".format(rates['sharp']['q'][i-1]) + " & ")
        else: tableFile.write("     & ")

        tableFile.write("{:.2e}".format(errSharp['p'][i]) + " & ")
        if i > 0: tableFile.write("{:.2f}".format(rates['sharp']['p'][i-1]) + " & ")
        else: tableFile.write("     & ")

        tableFile.write("{:.2e}".format(errSharp['lambda'][i]) + " & ")
        if i > 0: tableFile.write("{:.2f}".format(rates['sharp']['lambda'][i-1]) + " & ")
        else: tableFile.write("     & ")

        tableFile.write("{:.2e}".format(errSharp['q_if'][i]) + " & ")
        if i > 0: tableFile.write("{:.2f}".format(rates['sharp']['q_if'][i-1]) + " & ")
        else: tableFile.write("     & ")

        tableFile.write("{:.0f}".format(errSharp['it'][i]))
        tableFile.write(r" \\" + "\n")

    tableFile.write(r"        \bottomrule" + "\n")
    tableFile.write(r"    \end{tabular}" + "\n")
    tableFile.write(r"    \end{center}" + "\n")
    tableFile.write(r"    }" + "\n")
    tableFile.write("\end{table}" + "\n")
