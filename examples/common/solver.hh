// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief TODO doc me
 */
#ifndef DUMUX_MORTAR_ITERATIVE_SOLVER_HH
#define DUMUX_MORTAR_ITERATIVE_SOLVER_HH

#include <utility>

#include "types.hh"
#include "preconditioner.hh"
#include "interfaceoperator.hh"
#include "coarsesolver.hh"
#include "vtkoutput.hh"

namespace Dumux {

template<class Interface>
std::size_t numMortarDofs(const ContainerOfPointers<Interface>& interfaces)
{
    return std::accumulate(
        interfaces.begin(), interfaces.end(), std::size_t{0},
        [] (std::size_t current, auto ifPtr) {
            return current + ifPtr->gridGeometry().numDofs();
    });
}

template<class SubDomain>
auto getSubDomainSolutions(const ContainerOfPointers<SubDomain>& subDomains)
{
    std::vector<typename SubDomain::SolutionVector> result;
    result.resize(subDomains.size());
    std::for_each(subDomains.begin(), subDomains.end(), [&result] (auto sdPtr) {
        result[sdPtr->id()] = sdPtr->solution();
    });
    return result;
}

template<typename Operator, typename Preconditioner>
std::shared_ptr<Dune::IterativeSolver<typename Operator::domain_type,
                                      typename Operator::range_type>>
makeInterfaceSolver(Operator& op,
                    Preconditioner& prec,
                    double initResidual,
                    std::string_view solverType = "GMRes")
{
    using namespace Dumux;
    const auto threshold = getParam<double>("InterfaceSolver.Residual");
    const auto maxIt = getParam<std::size_t>("InterfaceSolver.MaxIterations");
    const auto verbosity = getParam<int>("InterfaceSolver.Verbosity");
    const auto reduction = threshold/initResidual;

    using RangeType = typename Operator::range_type;
    if (solverType == "CG")
        return std::make_shared<Dune::CGSolver<RangeType>>(
            op, prec, reduction, maxIt, verbosity
        );

    if (solverType == "GMRes")
        return std::make_shared<Dune::RestartedGMResSolver<RangeType>>(
            op, prec, reduction, maxIt, maxIt, verbosity
        );

    DUNE_THROW(Dune::NotImplemented, "Construction of linear solver with abbreviation: " << solverType);
}

template<class MortarSolution, class Interface, class SubDomainSolver>
void setSubDomainSourcesAfterCoarseSolve(const MortarSolution& mortarSolution,
                                         const ContainerOfPointers<Interface>& interfaces,
                                         const ContainerOfPointers<SubDomainSolver>& solvers)
{

    const auto computeSources = [&] (std::size_t ifId,
                                     const auto& problem,
                                     const auto& boundaryData) {
        if (problem.gridGeometry().discMethod == Dumux::DiscretizationMethod::box)
            DUNE_THROW(Dune::InvalidStateException, "This does not work for box");

        MortarSolution sources;
        sources.resize(problem.gridGeometry().numDofs());
        sources = 0.0;
        for (const auto& element : elements(problem.gridGeometry().gridView()))
        {
            const auto eIdx = problem.gridGeometry().elementMapper().index(element);
            auto fvGeometry = localView(problem.gridGeometry());
            fvGeometry.bindElement(element);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto interfaceIdx = problem.mortarInterfaceIndex(element, scvf);
                if (interfaceIdx && *interfaceIdx == ifId)
                    sources[eIdx] -= scvf.area()*boundaryData[eIdx]/element.geometry().volume();
            }
        }

        return sources;
    };

    std::vector<MortarSolution> ifSolutions;
    extractInterfaceSolutions(interfaces, ifSolutions, mortarSolution);
    for (std::size_t i = 0; i < interfaces.size(); ++i)
    {
        const auto& prob1 = interfaces[i]->positiveNeighborSolver().problemPointer();
        const auto& prob2 = interfaces[i]->negativeNeighborSolver().problemPointer();

        if (isFloating(interfaces[i]->positiveNeighborSolver())
            || isFloating(interfaces[i]->negativeNeighborSolver()))
        {
            MortarSolution p1, p2;
            Dumux::ProjectionDetail::projectToSubDomain(interfaces[i]->positiveProjector(),
                                                        interfaces[i]->negativeProjector(),
                                                        ifSolutions[i], p1, p2);
            p2 *= -1.0;

            prob1->setSourcesFromInterface(i, computeSources(interfaces[i]->id(), *prob1, p1));
            prob2->setSourcesFromInterface(i, computeSources(interfaces[i]->id(), *prob2, p2));
        }
    }
}

template<class SubDomainSolver>
void clearSourcesFromCoarseSolve(ContainerOfPointers<SubDomainSolver>& solvers)
{
    std::for_each(solvers.begin(), solvers.end(), [] (Pointer<SubDomainSolver>& ptr) {
        ptr->problemPointer()->removeSourcesFromInterface();
    });
}

template<class MortarSolutionVector>
class IterativeMortarSolver
{
    using SolutionIterationCountPair = std::pair<MortarSolutionVector, int>;

public:
    template<class Interface, class SubDomainSolver>
    SolutionIterationCountPair
    solve(ContainerOfPointers<Interface>& interfaces,
          ContainerOfPointers<SubDomainSolver>& subDomainSolvers) const
    {
        const auto floatingDomainMap = FloatingDomainMap{subDomainSolvers};
        const auto coarseSolver = CoarseProblemSolver{floatingDomainMap, subDomainSolvers, interfaces};
        const std::size_t numMortarDofs = Dumux::numMortarDofs(interfaces);

        // Eqs. (2.23)
        auto lambdaBar = makeVector<MortarSolutionVector>(numMortarDofs);
        if (!floatingDomainMap.empty())
        {
            std::cout << "\n --- Solving coarse problem --- " << std::endl;
            lambdaBar = coarseSolve_(coarseSolver, floatingDomainMap, subDomainSolvers, interfaces);
            std::cout << "\n --- computing floating subdomain sources --- " << std::endl;
            setSubDomainSourcesAfterCoarseSolve(lambdaBar, interfaces, subDomainSolvers);
        }

        // Eqs. (2.24)
        std::cout << "\n --- Computing initial mortar variable jump --- " << std::endl;
        auto deltap0 = makeVector<MortarSolutionVector>(numMortarDofs);
        setMortarProjectionsInSubDomains(makeVector<MortarSolutionVector>(numMortarDofs), interfaces);
        setHomogeneousSetup_(subDomainSolvers, false);
        solveSubDomains(subDomainSolvers);
        if (!floatingDomainMap.empty())
            setMortarProjectionsInSubDomains(lambdaBar, interfaces);
        computePressureJumpAcrossMortar(deltap0, interfaces);
        clearSourcesFromCoarseSolve(subDomainSolvers);
        const auto pf0 = getSubDomainSolutions(subDomainSolvers);

        // Eq. (2.25) / solve homogeneous system with deltap0 as RHS
        std::cout << "\n --- Solving homogeneous problem --- " << std::endl;
        setHomogeneousSetup_(subDomainSolvers, true);
        auto kernelProjector = makeKernelProjector_(coarseSolver, floatingDomainMap);
        auto preconditioner = makePreconditioner_(interfaces, subDomainSolvers, kernelProjector);
        auto kernelOperator = makeInterfaceOperator_(interfaces, subDomainSolvers, kernelProjector);
        const auto [lambda0, iterationCount] = solveInterfaceProblem_(
            interfaces, subDomainSolvers, kernelOperator, preconditioner, kernelProjector, deltap0
        );
        const auto pLambda0 = getSubDomainSolutions(subDomainSolvers);
        setHomogeneousSetup_(subDomainSolvers, false);

        // (pre-)compute final solutions
        auto lambdaFinal = mergeSolutionContainers_(lambda0, lambdaBar);
        auto pFinal = mergeSolutionContainers_(pf0, pLambda0);

        setMortarProjectionsInSubDomains(lambdaFinal, interfaces);
        setSubDomainSolutions_(subDomainSolvers, pFinal);

        // Eqs. (2.26)
        if (!floatingDomainMap.empty())
        {
            std::cout << "\n --- Solving coarse problem to get pressure offsets--- " << std::endl;
            const auto pLambdaBar = computePressureOffsets_(coarseSolver, interfaces, subDomainSolvers);
            std::for_each(subDomainSolvers.begin(), subDomainSolvers.end(), [&] (Pointer<SubDomainSolver>& ptr) {
                if (floatingDomainMap.isFloating(ptr->id()))
                    std::for_each(pFinal[ptr->id()].begin(),
                                  pFinal[ptr->id()].end(),
                                  [&] (auto& dofs) {
                                      dofs += pLambdaBar[floatingDomainMap.getFloatingDomainIdx(ptr->id())];
                                  });
            });
        }

        setSubDomainSolutions_(subDomainSolvers, pFinal);
        return {lambdaFinal, iterationCount};
    }

private:
    template<class SolutionContainer>
    SolutionContainer mergeSolutionContainers_(const SolutionContainer& c1,
                                               const SolutionContainer& c2) const
    {
        auto result = c1;
        std::transform(
            c1.begin(), c1.end(), c2.begin(), result.begin(),
            [] (auto v1, const auto& v2) {
                v1 += v2;
                return v1;
        });
        return result;
    }

    template<class CoarseSolver>
    Pointer<KernelProjector<MortarSolutionVector>>
    makeKernelProjector_(const CoarseSolver& coarseSolver,
                         const FloatingDomainMap& map) const
    {
        if (!map.empty())
            return makePointer<CoarseMatrixKernelProjector<MortarSolutionVector>>(
                coarseSolver.BBTInverse(), coarseSolver.B()
            );
        return makePointer<Dumux::IdentityKernelProjector<MortarSolutionVector>>();
    }

    template<class Interface, class SubDomainSolver>
    auto makeInterfaceOperator_(ContainerOfPointers<Interface>& interfaces,
                                ContainerOfPointers<SubDomainSolver>& subDomainSolvers,
                                Pointer<KernelProjector<MortarSolutionVector>> p) const
    {
        using Operator = OnePMortarInterfaceOperator<Interface, SubDomainSolver, MortarSolutionVector>;
        return Operator{interfaces, subDomainSolvers, p};
    }

    template<class Interface, class SubDomainSolver>
    auto makePreconditioner_(ContainerOfPointers<Interface>& interfaces,
                             ContainerOfPointers<SubDomainSolver>& subDomainSolvers,
                             Pointer<KernelProjector<MortarSolutionVector>> p) const
    {
        using Preconditioner = OnePMortarPreconditioner<MortarSolutionVector, SubDomainSolver, Interface>;
        Preconditioner prec{interfaces, subDomainSolvers, p};

        for (auto ifPtr : interfaces)
            prec.setMassMatrix(ifPtr->id(), ifPtr->positiveProjector().massMatrix());

        return prec;
    }

    template<class SubDomainSolver>
    void setHomogeneousSetup_(ContainerOfPointers<SubDomainSolver>& solvers, bool value) const
    {
        std::for_each(solvers.begin(), solvers.end(), [value] (Pointer<SubDomainSolver>& ptr) {
            ptr->problemPointer()->setUseHomogeneousSetup(value);
        });
    }



    template<class SubDomain, class SolutionContainer>
    void setSubDomainSolutions_(ContainerOfPointers<SubDomain>& solvers,
                                const SolutionContainer& solutions) const
    {
        std::for_each(solvers.begin(), solvers.end(), [&] (auto solverPtr) {
            solverPtr->setSolution(solutions[solverPtr->id()]);
        });
    }

    template<class CoarseSolver, class SubDomain, class Interface>
    MortarSolutionVector coarseSolve_(const CoarseSolver& coarseSolver,
                                      const FloatingDomainMap& floatingDomainMap,
                                      const ContainerOfPointers<SubDomain>& subDomains,
                                      const ContainerOfPointers<Interface>& interfaces) const
    {
        auto lambdaBar = makeVector<MortarSolutionVector>(numMortarDofs(interfaces));
        auto coarseSolution = makeVector<MortarSolutionVector>(floatingDomainMap.numFloatingDomains());
        auto rhs = makeVector<MortarSolutionVector>(floatingDomainMap.numFloatingDomains());
        assembleCoarseSolveRHS(rhs, subDomains, floatingDomainMap);
        coarseSolver.solve(coarseSolution, rhs);
        coarseSolver.B().mtv(coarseSolution, lambdaBar);
        return lambdaBar;
    }

    template<class Interface, class SubDomain, class Operator, class Preconditioner>
    SolutionIterationCountPair solveInterfaceProblem_(ContainerOfPointers<Interface>& interfaces,
                                                      ContainerOfPointers<SubDomain>& subDomains,
                                                      Operator& op,
                                                      Preconditioner& preconditioner,
                                                      Pointer<KernelProjector<MortarSolutionVector>>& kernelProjector,
                                                      const MortarSolutionVector& deltap) const
    {
        auto rhs = deltap;
        rhs *= -1.0;
        rhs = kernelProjector->fromKernel(rhs);

        auto lambda = makeVector<MortarSolutionVector>(numMortarDofs(interfaces));
        auto initResidual = lambda;
        op.apply(lambda, initResidual);
        initResidual -= rhs;
        const auto initResidualNorm = initResidual.two_norm();
        std::cout << "Initial residual norm: " << initResidualNorm << std::endl;

        auto lsType = getParam<std::string>("InterfaceSolver.LinearSolverType");
        auto isSolver = makeInterfaceSolver(op, preconditioner, initResidualNorm, lsType);

        Dune::InverseOperatorResult result;
        // auto lambda = makeVector<MortarSolutionVector>(numMortarDofs(interfaces));
        isSolver->apply(lambda, rhs, result);
        if (!result.converged)
            DUNE_THROW(Dune::InvalidStateException, "Iterative solver did not converge");

        // solve with final lambda to ensure subdomains hold the correct solutions
        setMortarProjectionsInSubDomains(lambda, interfaces);
        solveSubDomains(subDomains);
        return {lambda, result.iterations};
    }

    template<class CoarseSolver, class Interface, class SubDomain>
    MortarSolutionVector computePressureOffsets_(const CoarseSolver& coarseSolver,
                                                 ContainerOfPointers<Interface>& interfaces,
                                                 ContainerOfPointers<SubDomain>& subDomains) const
    {
        auto pLambdaBar = makeVector<MortarSolutionVector>(numMortarDofs(interfaces));
        auto dp = makeVector<MortarSolutionVector>(numMortarDofs(interfaces));
        auto rhs = makeVector<MortarSolutionVector>(coarseSolver.B().M());
        computePressureJumpAcrossMortar(dp, interfaces);
        coarseSolver.B().mv(dp, rhs);
        rhs *= -1.0;
        coarseSolver.solve(pLambdaBar, rhs);
        return pLambdaBar;
    }
};

}  // end namespace Dumux

#endif
