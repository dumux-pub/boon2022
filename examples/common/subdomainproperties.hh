// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief Properties for a stationary, single-phase problem.
 */
#ifndef DUMUX_MORTAR_SUBDOMAIN_SOLVER_PROPERTIES_HH
#define DUMUX_MORTAR_SUBDOMAIN_SOLVER_PROPERTIES_HH

// define type tags & basic properties for the subdomain solver
#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <examples/common/mpfa/localassembler.hh>
#include <examples/common/mpfa/ivdatahandle.hh>
#include <examples/common/mpfa/darcyslaw.hh>
#include <examples/common/mpfa/ivtraits.hh>

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct SubDomainSolverOneP { using InheritsFrom = std::tuple<OneP>; };
struct SubDomainSolverOnePTpfa { using InheritsFrom = std::tuple<SubDomainSolverOneP, CCTpfaModel>; };
struct SubDomainSolverOnePMpfa { using InheritsFrom = std::tuple<SubDomainSolverOneP, CCMpfaModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::SubDomainSolverOneP>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::Constant<0, Scalar> > ;
};

// Some optimization flags
template<class TypeTag> struct EnableGridGeometryCache<TypeTag, TTag::SubDomainSolverOneP> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridVolumeVariablesCache<TypeTag, TTag::SubDomainSolverOneP> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridFluxVariablesCache<TypeTag, TTag::SubDomainSolverOneP> { static constexpr bool value = true; };
template<class TypeTag> struct SolutionDependentAdvection<TypeTag, TTag::SubDomainSolverOneP> { static constexpr bool value = false; };

// for mpfa, we use a custom interaction volume (or rather custom traits)
template<class TypeTag>
struct PrimaryInteractionVolume<TypeTag, TTag::SubDomainSolverOnePMpfa>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;
    using Traits = MortarIVTraits< NodalIndexSet, Scalar >;
public:
    using type = CCMpfaOInteractionVolume< Traits >;
};

// for mpfa, we use a custom interaction volume (or rather custom traits)
template<class TypeTag>
struct SecondaryInteractionVolume<TypeTag, TTag::SubDomainSolverOnePMpfa>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;
    using Traits = MortarIVTraits< NodalIndexSet, Scalar >;
public:
    using type = CCMpfaOInteractionVolume< Traits >;
};

// for mpfa, we use custom Darcys Law
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::SubDomainSolverOnePMpfa>
{ using type = MortarMpfaDarcysLaw< TypeTag >; };

// for mpfa, we use a custom data handle for the flux vars cache (to fit our local assembler)
template<class TypeTag>
struct GridFluxVariablesCache<TypeTag, TTag::SubDomainSolverOnePMpfa>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridFluxVariablesCache>();
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluxVariablesCache = GetPropType<TypeTag, Properties::FluxVariablesCache>;
    using FluxVariablesCacheFiller = GetPropType<TypeTag, Properties::FluxVariablesCacheFiller>;

    using PrimaryInteractionVolume = GetPropType<TypeTag, Properties::PrimaryInteractionVolume>;
    using SecondaryInteractionVolume = GetPropType<TypeTag, Properties::SecondaryInteractionVolume>;

    using PhysicsTraits = IvDataHandlePhysicsTraits<GetPropType<TypeTag, Properties::ModelTraits>>;
    using PrimaryMatVecTraits = typename PrimaryInteractionVolume::Traits::MatVecTraits;
    using SecondaryMatVecTraits = typename SecondaryInteractionVolume::Traits::MatVecTraits;

    using PrimaryIvDataHandle = MortarIVDataHandle<PrimaryMatVecTraits, PhysicsTraits>;
    using SecondaryIvDataHandle = MortarIVDataHandle<SecondaryMatVecTraits, PhysicsTraits>;

    using Traits = CCMpfaDefaultGridFluxVariablesCacheTraits<Problem,
                                                             FluxVariablesCache, FluxVariablesCacheFiller,
                                                             PrimaryInteractionVolume, SecondaryInteractionVolume,
                                                             PrimaryIvDataHandle, SecondaryIvDataHandle>;
public:
    using type = CCMpfaGridFluxVariablesCache<Traits, enableCache>;
};

//! Custom grid geometry traits to increase stencil size
template<class GV, class NI, class PIV, class SIV>
struct GGTraitsMpfa : public CCMpfaFVGridGeometryTraits<GV, NI, PIV, SIV>
{
    static constexpr int maxElementStencilSize = int(GV::dimension) == 3 ? 200 :
                                                 (int(GV::dimension)<int(GV::dimensionworld) ? 45 : 20);
};

//! Use custom grid geometry traits
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::SubDomainSolverOnePMpfa>
{
private:
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using PrimaryIV = GetPropType<TypeTag, Properties::PrimaryInteractionVolume>;
    using SecondaryIV = GetPropType<TypeTag, Properties::SecondaryInteractionVolume>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;
    using Traits = GGTraitsMpfa<GridView, NodalIndexSet, PrimaryIV, SecondaryIV>;
public:
    using type = CCMpfaFVGridGeometry<GridView, Traits, getPropValue<TypeTag, Properties::EnableGridGeometryCache>()>;
};

} // end namespace Properties

#endif
