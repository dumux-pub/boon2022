// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_INTERFACE_PRECONDITIONER_HH
#define DUMUX_MORTAR_INTERFACE_PRECONDITIONER_HH

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/preconditioner.hh>

#include "projectorfactory.hh"
#include "mortarvariabletype.hh"
#include "interfaceoperator.hh"
#include "types.hh"

namespace Dumux {

template<class Interface, class MortarSolution, class SubDomainSolution>
void projectToSubDomains(const Interface& interface,
                         const FlatProjector<MortarSolution>& projector1,
                         const FlatProjector<MortarSolution>& projector2,
                         const MortarSolution& x,
                         SubDomainSolution& subDomainSolution1,
                         SubDomainSolution& subDomainSolution2)
{
    ProjectionDetail::projectToSubDomain(
        projector1, projector2, x, subDomainSolution1, subDomainSolution2
    );
}

template<class Interface, class MortarSolution, class SubDomainSolution>
void projectToSubDomains(const Interface& interface,
                         const SharpProjector<MortarSolution>&,
                         const SharpProjector<MortarSolution>&,
                         const MortarSolution& x,
                         SubDomainSolution& subDomainSolution1,
                         SubDomainSolution& subDomainSolution2)
{
    // do a standard L2-projection into the subdomains
    ProjectorFactory<FlatProjector<MortarSolution>> factory;
    const auto [projector1, projector2] = factory.makeProjectors(
        interface.positiveNeighborSolver(),
        interface.negativeNeighborSolver(),
        interface.gridGeometry(),
        interface.glue1(),
        interface.glue2()
    );

    ProjectionDetail::projectToSubDomain(
        *projector1, *projector2, x, subDomainSolution1, subDomainSolution2
    );
}

template<class MortarSolution, class Interface>
void setMortarPressuresInSubDomains(const MortarSolution& mortarSolution,
                                    const std::vector<std::shared_ptr<Interface>>& interfaces)
{
    // extract interface sub-solutions
    std::vector<MortarSolution> ifSolutions;
    extractInterfaceSolutions(interfaces, ifSolutions, mortarSolution);

    std::for_each(interfaces.begin(), interfaces.end(), [&] (auto ifPtr) {
        MortarSolution p1, p2;
        projectToSubDomains(*ifPtr,
                            ifPtr->positiveProjector(),
                            ifPtr->negativeProjector(),
                            ifSolutions[ifPtr->id()], p1, p2);

        ifPtr->positiveNeighborSolver().problemPointer()->setMortarPressure(ifPtr->id(), p1);
        ifPtr->negativeNeighborSolver().problemPointer()->setMortarPressure(ifPtr->id(), p2);
    });
    //
    // std::for_each(interfaces.begin(), interfaces.end(), [&] (auto ifPtr) {
    //     MortarSolution p1, p2;
    //     ProjectionDetail::projectToSubDomain(ifPtr->positiveProjector(),
    //                                          ifPtr->negativeProjector(),
    //                                          ifSolutions[ifPtr->id()], p1, p2);
    //
    //     ifPtr->positiveNeighborSolver().problemPointer()->setMortarPressure(ifPtr->id(), p1);
    //     ifPtr->negativeNeighborSolver().problemPointer()->setMortarPressure(ifPtr->id(), p2);
    // });
}

template<class MortarSolution, class Interface>
void computeFluxJumpAcrossMortar(MortarSolution& result,
                                const std::vector<std::shared_ptr<Interface>>& interfaces)
{
    std::vector<MortarSolution> deltaFluxes(interfaces.size());
    for (const auto ifPtr : interfaces)
    {
        const auto& posSolver = ifPtr->positiveNeighborSolver();
        const auto& posTraceOp = ifPtr->positiveTraceOperator();
        const auto& posProjector = ifPtr->positiveProjector();

        const auto& negSolver = ifPtr->negativeNeighborSolver();
        const auto& negTraceOp = ifPtr->negativeTraceOperator();
        const auto& negProjector = ifPtr->negativeProjector();

        MortarSolution t1, t2;
        t1 = posTraceOp.recoverTraceFlux(posSolver.solution());
        t2 = negTraceOp.recoverTraceFlux(negSolver.solution());
        // t2 *= -1.0;

        // use standard L2-projection for back-projection into mortar
        const auto [posToMortar, posToSubDomain] = makeProjectorPair(
            getFunctionSpaceBasis(posSolver.gridGeometry()),
            getFunctionSpaceBasis(ifPtr->gridGeometry()),
            ifPtr->glue1()
        );

        const auto [negToMortar, negToSubDomain] = makeProjectorPair(
            getFunctionSpaceBasis(negSolver.gridGeometry()),
            getFunctionSpaceBasis(ifPtr->gridGeometry()),
            ifPtr->glue2()
        );

        const auto flux1 = posToMortar.project(t1);
        const auto flux2 = negToMortar.project(t2);

        if (flux1.size() != flux2.size())
            DUNE_THROW(Dune::InvalidStateException, "Trace projections do not have the same size");
        if (flux1.size() != ifPtr->gridGeometry().numDofs())
            DUNE_THROW(Dune::InvalidStateException, "Trace projections do not have size of interface space");

        deltaFluxes[ifPtr->id()] = flux1;
        deltaFluxes[ifPtr->id()] += flux2;
    }

    mergeMortarSolutions(result, deltaFluxes);
}

template<typename SubDomain>
void setMortarVariableType(ContainerOfPointers<SubDomain>& sdPtrs,
                           MortarVariableType mv)
{
    std::for_each(sdPtrs.begin(), sdPtrs.end(), [mv] (auto& sdPtr) {
        sdPtr->problemPointer()->setMortarVariableType(mv);
    });
}

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class MortarSolutionVector,
         class SubDomainSolver,
         class Interface>
class OnePMortarPreconditioner
: public Dune::Preconditioner<MortarSolutionVector, MortarSolutionVector>
{

    using SDSolverPtr = std::shared_ptr<SubDomainSolver>;
    using InterfacePtr = std::shared_ptr<Interface>;
    using KernelProjector = Dumux::KernelProjector<MortarSolutionVector>;

public:
    /*!
     * \ingroup TODO doc me.
     * \brief TODO doc me.
     */
    explicit OnePMortarPreconditioner(std::vector<InterfacePtr> interfacePtrs,
                                      std::vector<SDSolverPtr> sdSolverPtr,
                                      std::shared_ptr<KernelProjector> kp
                                         = std::make_shared<IdentityKernelProjector<MortarSolutionVector>>())
    : interfaces_(interfacePtrs)
    , solvers_(sdSolverPtr)
    , kernelProjector_(kp)
    , isActive_(getParam<bool>("Preconditioner.Active", true))
    , useMassMatrixPrec_(getParam<bool>("Preconditioner.UseMassMatrix", false))
    {}

    /*!
     * \brief Prepare the preconditioner.
     */
    virtual void pre (MortarSolutionVector& x, MortarSolutionVector& b) override
    {}

    /*!
     * \brief Apply one step of the preconditioner to the system A(v)=d.
     */
    virtual void apply (MortarSolutionVector& r, const MortarSolutionVector& x) override
    {
        if (isActive_)
        {
            if (!useMassMatrixPrec_)
            {
                std::vector<bool> usesHomogeneousSetup(solvers_.size());
                std::vector<MortarSolutionVector> subDomainSolutions(solvers_.size());
                for (auto sdSolverPtr : solvers_)
                {
                    usesHomogeneousSetup[sdSolverPtr->id()] = sdSolverPtr->problem().usesHomogeneousSetup();
                    sdSolverPtr->problemPointer()->setUseHomogeneousSetup(true);
                    subDomainSolutions[sdSolverPtr->id()] = sdSolverPtr->solution();
                }

                setMortarPressuresInSubDomains(x, interfaces_);
                setMortarVariableType(solvers_, MortarVariableType::pressure);
                solveSubDomains(solvers_);
                computeFluxJumpAcrossMortar(r, interfaces_);
                setMortarVariableType(solvers_, MortarVariableType::flux);

                for (auto sdSolverPtr : solvers_)
                {
                    sdSolverPtr->problemPointer()->setUseHomogeneousSetup(usesHomogeneousSetup[sdSolverPtr->id()]);
                    sdSolverPtr->setSolution(subDomainSolutions[sdSolverPtr->id()]);
                }

                r = kernelProjector_->toKernel(r);
            }
            else
            {
                std::vector<std::size_t> sizes(interfaces_.size());
                for (auto ifPtr : interfaces_)
                    sizes[ifPtr->id()] = ifPtr->gridGeometry().numDofs();

                auto computeOffset = [&] (std::size_t i) {
                    std::size_t offset = 0;
                    for (int j = 0; j < i; ++j)
                        offset += sizes[j];
                    return offset;
                };

                for (auto ifPtr : interfaces_)
                {
                    const auto offset = computeOffset(ifPtr->id());
                    auto tmp = x;
                    tmp.resize(ifPtr->gridGeometry().numDofs());
                    for (int i = 0; i < ifPtr->gridGeometry().numDofs(); ++i)
                        tmp[i] = x[i+offset];

                    auto result = tmp;
                    massMatrices_[ifPtr->id()].mv(tmp, result);

                    for (int i = 0; i < ifPtr->gridGeometry().numDofs(); ++i)
                        r[i+offset] = result[i];
                }
            }
        }
        else
            r = x;
    }

    /*!
     * \brief Clean up.
     */
    virtual void post (MortarSolutionVector& x) override
    {}

    //! Category of the preconditioner (see SolverCategory::Category)
    virtual Dune::SolverCategory::Category category() const
    { return Dune::SolverCategory::sequential; }

    template<typename Matrix>
    void setMassMatrix(std::size_t i, const Matrix& m)
    {
        massMatrices_[i] = m;
    }

private:
    std::vector<InterfacePtr> interfaces_;
    std::vector<SDSolverPtr> solvers_;
    std::shared_ptr<KernelProjector> kernelProjector_;
    bool isActive_;
    bool useMassMatrixPrec_;

    std::unordered_map<
        std::size_t,
        Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>>
    > massMatrices_;
};

} // end namespace Dumux

#endif
