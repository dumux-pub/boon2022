// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_PROJECTOR_FACTORY_HH
#define DUMUX_MORTAR_PROJECTOR_FACTORY_HH

#include <dumux/discretization/functionspacebasis.hh>
#include <dumux/multidomain/glue.hh>

#include "projector.hh"

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template< class Projector >
class ProjectorFactory;

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template< class MortarSolution >
class ProjectorFactory< FlatProjector<MortarSolution> >
{
    using ProjectorPointer = std::shared_ptr<FlatProjector<MortarSolution>>;

public:
    /*!
     * \ingroup TODO doc me.
     * \brief TODO doc me.
     */
    template<class Solver1, class Solver2, class MortarGG>
    static std::pair< ProjectorPointer, ProjectorPointer >
    makeProjectors(const Solver1& solver1,
                   const Solver2& solver2,
                   const MortarGG& mortarGG)
    {
        const auto glue1 = makeGlue(*solver1.gridGeometryPointer(), mortarGG);
        const auto glue2 = makeGlue(*solver2.gridGeometryPointer(), mortarGG);
        return makeProjectors(solver1, solver2, mortarGG, glue1, glue2);
    }

    /*!
     * \ingroup TODO doc me.
     * \brief TODO doc me.
     */
    template<class Solver1, class Solver2, class MortarGG, class Glue>
    static std::pair< ProjectorPointer, ProjectorPointer >
    makeProjectors(const Solver1& solver1,
                   const Solver2& solver2,
                   const MortarGG& mortarGG,
                   const Glue& glue1,
                   const Glue& glue2)
    {
        const auto sd1Basis = getFunctionSpaceBasis(*solver1.gridGeometryPointer());
        const auto sd2Basis = getFunctionSpaceBasis(*solver2.gridGeometryPointer());
        const auto& mortarBasis = getFunctionSpaceBasis(mortarGG);

        auto baseProjectorMatrices1 = makeProjectionMatricesPair(sd1Basis, mortarBasis, glue1);
        auto baseProjectorMatrices2 = makeProjectionMatricesPair(sd2Basis, mortarBasis, glue2);

        using P = FlatProjector<MortarSolution>;
        auto p1 = std::make_shared<P>(
            std::move(baseProjectorMatrices1.second.first),
            std::move(baseProjectorMatrices1.second.second),
            std::move(baseProjectorMatrices1.first.first)
        );
        auto p2 = std::make_shared<P>(
            std::move(baseProjectorMatrices2.second.first),
            std::move(baseProjectorMatrices2.second.second),
            std::move(baseProjectorMatrices2.first.first)
        );
        return std::make_pair(p1, p2);
    }
};

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template< class MortarSolution >
class ProjectorFactory< SharpProjector<MortarSolution> >
{
    using ProjectorPointer = std::shared_ptr<SharpProjector<MortarSolution>>;

public:
    /*!
     * \ingroup TODO doc me.
     * \brief TODO doc me.
     */
    template<class Solver1, class Solver2, class MortarGG>
    static std::pair< ProjectorPointer, ProjectorPointer >
    makeProjectors(const Solver1& solver1,
                   const Solver2& solver2,
                   const MortarGG& mortarGG)
    {
        const auto glue1 = makeGlue(*solver1.gridGeometryPointer(), mortarGG);
        const auto glue2 = makeGlue(*solver2.gridGeometryPointer(), mortarGG);
        return makeProjectors(solver1, solver2, mortarGG, glue1, glue2);
    }

    /*!
     * \ingroup TODO doc me.
     * \brief TODO doc me.
     */
    template<class Solver1, class Solver2, class MortarGG, class Glue>
    static std::pair< ProjectorPointer, ProjectorPointer >
    makeProjectors(const Solver1& solver1,
                   const Solver2& solver2,
                   const MortarGG& mortarGG,
                   const Glue& glue1,
                   const Glue& glue2)
    {
        const auto sd1Basis = getFunctionSpaceBasis(*solver1.gridGeometryPointer());
        const auto sd2Basis = getFunctionSpaceBasis(*solver2.gridGeometryPointer());
        const auto& mortarBasis = getFunctionSpaceBasis(mortarGG);

        const auto matrices1 = makeProjectionMatricesPair(sd1Basis, mortarBasis, glue1).second;
        const auto matrices2 = makeProjectionMatricesPair(sd2Basis, mortarBasis, glue2).second;

        using Projector = SharpProjector<MortarSolution>;
        auto p = std::make_shared<Projector>(matrices1.first, matrices1.second, matrices2.first, matrices2.second);
        return std::make_pair(p, p);
    }
};

} // end namespace Dumux

#endif
