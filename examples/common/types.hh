// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief TODO doc me
 */
#ifndef DUMUX_MORTAR_TYPES_HH
#define DUMUX_MORTAR_TYPES_HH

#include <algorithm>

namespace Dumux {

template<class T>
using Pointer = std::shared_ptr<T>;

template<class T>
using Container = std::vector<T>;

template<class T>
using ContainerOfPointers = Container<Pointer<T>>;

template<class T, class... Args>
Pointer<T> makePointer(Args&&... t)
{ return std::make_shared<T>(std::forward<Args>(t)...); }

template<class Vector>
Vector makeVector(std::size_t n, typename Vector::value_type init_value = 0.0)
{
    Vector v;
    v.resize(n);
    std::fill(v.begin(), v.end(), init_value);
    return v;
}

}  // end namespace Dumux

#endif
