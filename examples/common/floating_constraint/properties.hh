// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_CONSTRAINT_PROPERTIES_HH
#define DUMUX_CONSTRAINT_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/common/fvproblem.hh>
#include <dumux/common/properties/model.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/flux/fluxvariablescaching.hh>

#include "problem.hh"
#include "localresidual.hh"
#include "volumevariables.hh"

namespace Dumux::Properties {

struct ConstraintModelTraits { static constexpr int numEq() { return 1; } };

namespace TTag {
struct Constraint { using InheritsFrom = std::tuple<CCTpfaModel, ModelProperties>; };
} // end namespace TTag

template<class TypeTag>
struct Problem<TypeTag, TTag::Constraint> {
    using type = Dumux::ConstraintProblem<TypeTag>;
};

template<class TypeTag>
struct LocalResidual<TypeTag, TTag::Constraint> {
    using type = Dumux::ConstraintLocalResidual<TypeTag>;
};

template<class TypeTag>
struct Grid<TypeTag, TTag::Constraint> {
    using type = Dune::YaspGrid<1>;
};

template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::Constraint> {
    using type = Dumux::ConstraintVolumeVariables<GetPropType<TypeTag, PrimaryVariables>>;
};

template<class TypeTag>
struct ModelTraits<TypeTag, TTag::Constraint> {
    using type = ConstraintModelTraits;
};

template<class TypeTag>
struct FluxVariablesCache<TypeTag, TTag::Constraint> {
    using type = Dumux::FluxVariablesCaching::EmptyCache<GetPropType<TypeTag, Properties::Scalar>>;
};

template<class TypeTag>
struct FluxVariablesCacheFiller<TypeTag, TTag::Constraint> {
    using type = Dumux::FluxVariablesCaching::EmptyCacheFiller;
};

} // end namespace Dumux::Properties

#endif
