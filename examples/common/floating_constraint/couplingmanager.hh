// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GLAESER_2021_A_CONSTRAINT_COUPLING_MANAGER_HH
#define DUMUX_GLAESER_2021_A_CONSTRAINT_COUPLING_MANAGER_HH

#include <numeric>
#include <dune/common/exceptions.hh>
#include <dumux/multidomain/couplingmanager.hh>

#include "problem.hh"
#include "lagrangemultiplierevaluator.hh"

namespace Dumux {

template<class Traits>
class ConstraintCouplingManager
: public CouplingManager<Traits>
, public AveragePressureEvaluator<double>
, public LagrangeMultiplierEvaluator<double>
{
    template<std::size_t id> using SubDomainTypeTag = typename Traits::template SubDomain<id>::TypeTag;
    template<std::size_t id> using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using GridView = typename GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>::GridView;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using LocalResidual = GetPropType<SubDomainTypeTag<id>, Properties::LocalResidual>;
    template<std::size_t id> using ElementResidualVector = typename LocalResidual<id>::ElementResidualVector;
    using Scalar = GetPropType<SubDomainTypeTag<0>, Properties::Scalar>;

    static constexpr std::size_t domainId = 0;
    static constexpr std::size_t constraintId = 1;
    static_assert(GridGeometry<domainId>::discMethod != DiscretizationMethod::box);

public:
    ConstraintCouplingManager(std::shared_ptr<const GridGeometry<domainId>> domainGG,
                              std::shared_ptr<const GridGeometry<constraintId>> constraintGG)
    : domainGG_(domainGG)
    , constraintGG_(constraintGG)
    , zeroDof_{{0}}
    {
        if (constraintGG->numDofs() != 1)
            DUNE_THROW(Dune::InvalidStateException, "This expects scalar constraints");

        allDomainDofs_.resize(domainGG->numDofs());
        std::iota(allDomainDofs_.begin(), allDomainDofs_.end(), 0);

        Scalar gridVolume = 0.0;
        cellVolumeFractions_.resize(domainGG->gridView().size(0));
        for (const auto& element : elements(domainGG->gridView())) {
            gridVolume += element.geometry().volume();
            cellVolumeFractions_[domainGG->elementMapper().index(element)] = element.geometry().volume();
        }
        // std::for_each(cellVolumeFractions_.begin(),
        //               cellVolumeFractions_.end(),
        //               [&] (auto& f) { f /= gridVolume; });
    }

    template<std::size_t i, std::size_t j>
    const std::vector<std::size_t>& couplingStencil(Dune::index_constant<i> domainI,
                                                    const Element<i>& elementI,
                                                    Dune::index_constant<j> domainJ) const
    {
        static_assert(i != j, "Domain i cannot be coupled to itself!");
        if constexpr (i == domainId && j == constraintId)
            return zeroDof_;
        else if constexpr (i == constraintId && j == domainId)
            return allDomainDofs_;
        DUNE_THROW(Dune::InvalidStateException, "Unknown domain id combination!");
    }

    template<std::size_t i, std::size_t j, class LocalAssemblerI>
    decltype(auto) evalCouplingResidual(Dune::index_constant<i> domainI,
                                        const LocalAssemblerI& localAssemblerI,
                                        Dune::index_constant<j> domainJ,
                                        std::size_t dofIdxGlobalJ) const
    {
        if (localAssemblerI.fvGeometry().numScv() != 1)
            DUNE_THROW(Dune::InvalidStateException, "This only wors for cc schemes");

        if constexpr (i == domainId && j == constraintId)
            return localAssemblerI.evalLocalFluxAndSourceResidual();
        else if constexpr (i == constraintId && j == domainId)
            return localAssemblerI.evalLocalFluxAndSourceResidual();
        DUNE_THROW(Dune::InvalidStateException, "Unknown domain id combination!");
    }

    Scalar evalAveragePressure() const override {
        return std::inner_product(
            cellVolumeFractions_.begin(), cellVolumeFractions_.end(),
            this->curSol()[Dune::index_constant<domainId>()].begin(),
            Scalar{0.0}
        );
    }

    Scalar getLagrangeMultiplier() const override {
        assert(this->curSol()[Dune::index_constant<constraintId>()].size() == 1);
        return this->curSol()[Dune::index_constant<constraintId>()][0];
    }

private:
    std::shared_ptr<const GridGeometry<domainId>> domainGG_;
    std::shared_ptr<const GridGeometry<constraintId>> constraintGG_;

    std::vector<std::size_t> zeroDof_;
    std::vector<std::size_t> allDomainDofs_;
    std::vector<Scalar> cellVolumeFractions_;
};

} //end namespace Dumux

#endif
