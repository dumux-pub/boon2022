// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_CONSTRAINT_PROBLEM_HH
#define DUMUX_CONSTRAINT_PROBLEM_HH

#include <dumux/common/fvproblem.hh>
#include <dumux/common/boundarytypes.hh>

namespace Dumux {

template<class Scalar>
struct AveragePressureEvaluator {
    virtual Scalar evalAveragePressure() const = 0;
};

template<class TypeTag>
class ConstraintProblem : public FVProblem<TypeTag>
{
    using ParentType = FVProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int numEq = GetPropType<TypeTag, Properties::ModelTraits>::numEq();

public:
    ConstraintProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                      const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, paramGroup)
    {}

    const std::string& name() const
    { return "constraint"; }

    // We "inject" the constraint evaluation into the model as source term
    template<class ElementVolumeVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const {
        static const Scalar avg = getParam<Scalar>("Problem.AverageFloatingDomainPressure", 0.0);
        static_assert(NumEqVector::size() == 1);

        NumEqVector result{0.0};
        result[0] = avg - avgPressureEvalator_->evalAveragePressure();
        // result[0] /= scv.volume();
        return result;
    }

    template<typename... Args>
    BoundaryTypes<numEq> boundaryTypes(Args&&... args) const {
        BoundaryTypes<numEq> values;
        values.setAllNeumann();
        return values;
    }

    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    void setAveragePressureEvaluator(std::shared_ptr<AveragePressureEvaluator<Scalar>> eval) {
        avgPressureEvalator_ = eval;
    }

private:
    std::shared_ptr<AveragePressureEvaluator<Scalar>> avgPressureEvalator_;
};

} // end namespace Dumux

#endif
