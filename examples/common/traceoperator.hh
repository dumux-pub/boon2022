// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_INTERFACE_TRACE_OPERATOR_HH
#define DUMUX_MORTAR_INTERFACE_TRACE_OPERATOR_HH

#include <dune/common/exceptions.hh>
#include <dune/geometry/type.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/cellcentered/tpfa/computetransmissibility.hh>
#include "traceoperatorhelper.hh"

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template< class SubDomainSolver,
          DiscretizationMethod dm = SubDomainSolver::GridGeometry::discMethod >
class InterfaceTraceOperator;

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class SubDomainSolver>
class InterfaceTraceOperator< SubDomainSolver, DiscretizationMethod::cctpfa >
{
    using GridGeometry = typename SubDomainSolver::GridGeometry;
    using GridVariables = typename SubDomainSolver::GridVariables;
    using SolutionVector = typename SubDomainSolver::SolutionVector;

    using ElementScvfIndexMap = typename InterfaceTraceOperatorHelper::ElementScvfIndexMap<GridGeometry>;

public:
    /*!
     * \brief TODO doc me.
     */
    template<class InterfaceGridGeometry, class Glue>
    InterfaceTraceOperator(std::shared_ptr<SubDomainSolver> subDomainSolverPtr,
                           const InterfaceGridGeometry& interfaceGridGeometry,
                           const Glue& glue)
    : subDomainSolverPtr_(subDomainSolverPtr)
    {
        coupledScvfMap_ = InterfaceTraceOperatorHelper::findCoupledScvfs(*subDomainSolverPtr->gridGeometryPointer(),
                                                                         interfaceGridGeometry,
                                                                         glue);

        // This currently expects each element having only one face on the interface
        if (std::any_of(coupledScvfMap_.begin(),
                        coupledScvfMap_.end(),
                        [] (const auto& pair) { return pair.second.size() > 1; }))
            DUNE_THROW(Dune::InvalidStateException, "More than one sub-control volume face found on the interface");
    }


    /*!
     * \brief TODO doc me.
     */
    template<class SolutionVector>
    SolutionVector recoverTraceSolution(const SolutionVector& sol) const
    {
        const auto& gridGeometry = *subDomainSolverPtr_->gridGeometryPointer();

        // set up pressure vector with reconstructed face pressures
        SolutionVector sdPressures;
        sdPressures.resize(gridGeometry.gridView().size(0));
        sdPressures = 0.0;

        for (const auto& entry : coupledScvfMap_)
        {
            const auto sdElemIdx = entry.first;
            const auto scvfIdx = entry.second[0];
            const auto sdElement = gridGeometry.element(sdElemIdx);

            auto fvGeometry = localView(gridGeometry);
            auto elemVolVars = localView(subDomainSolverPtr_->gridVariablesPointer()->curGridVolVars());
            auto elemFluxVarsCache = localView(subDomainSolverPtr_->gridVariablesPointer()->gridFluxVarsCache());

            fvGeometry.bind(sdElement);
            elemVolVars.bind(sdElement, fvGeometry, sol);
            elemFluxVarsCache.bind(sdElement, fvGeometry,  elemVolVars);

            const auto& scvf = fvGeometry.scvf(scvfIdx);
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            const auto& insideVolVars = elemVolVars[insideScv];
            const auto& problem = elemVolVars.gridVolVars().problem();

            // flux = -ti*(pBoundary - pCell)
            auto ti = computeTpfaTransmissibility(scvf,
                                                  insideScv,
                                                  insideVolVars.permeability(),
                                                  insideVolVars.extrusionFactor());
            ti *= insideVolVars.density()*insideVolVars.mobility();

            auto flux = problem.neumann(sdElement, fvGeometry, elemVolVars, elemFluxVarsCache, scvf)[0];
            flux -= ti*insideVolVars.pressure();
            sdPressures[sdElemIdx] = -1.0*flux/ti;
        }

        return sdPressures;
    }

    /*!
     * \brief TODO doc me.
     */
    template<class SolutionVector>
    SolutionVector recoverTraceFlux(const SolutionVector& sol) const
    {
        const auto& gridGeometry = *subDomainSolverPtr_->gridGeometryPointer();

        // set up vector with interface flux per cell
        SolutionVector cellFluxes;
        cellFluxes.resize(gridGeometry.gridView().size(0));
        cellFluxes = 0.0;

        for (const auto& entry : coupledScvfMap_)
        {
            const auto sdElemIdx = entry.first;
            const auto scvfIdx = entry.second[0];
            const auto sdElement = gridGeometry.element(sdElemIdx);

            auto fvGeometry = localView(gridGeometry);
            auto elemVolVars = localView(subDomainSolverPtr_->gridVariablesPointer()->curGridVolVars());
            auto elemFluxVarsCache = localView(subDomainSolverPtr_->gridVariablesPointer()->gridFluxVarsCache());

            fvGeometry.bind(sdElement);
            elemVolVars.bind(sdElement, fvGeometry, sol);
            elemFluxVarsCache.bind(sdElement, fvGeometry, elemVolVars);

            const auto& scvf = fvGeometry.scvf(scvfIdx);
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            const auto& insideVolVars = elemVolVars[insideScv];
            const auto& problem = elemVolVars.gridVolVars().problem();

            typename SubDomainSolver::FluxVariables fluxVars;
            fluxVars.init(problem, sdElement, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

            auto up = [] (const auto& vv) { return vv.mobility(); };
            cellFluxes[sdElemIdx] = fluxVars.advectiveFlux(0, up)
                                    /insideVolVars.extrusionFactor()
                                    /scvf.area();
        }

        return cellFluxes;
    }

    //! Returns the map containing the scvfs lying on the interface
    const ElementScvfIndexMap& coupledScvfMap() const
    { return coupledScvfMap_; }

private:
    std::shared_ptr<SubDomainSolver> subDomainSolverPtr_;
    ElementScvfIndexMap coupledScvfMap_;
};

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class SubDomainSolver>
class InterfaceTraceOperator< SubDomainSolver, DiscretizationMethod::ccmpfa >
{
    using GridGeometry = typename SubDomainSolver::GridGeometry;
    using GridVariables = typename SubDomainSolver::GridVariables;
    using SolutionVector = typename SubDomainSolver::SolutionVector;
    using ElementScvfIndexMap = typename InterfaceTraceOperatorHelper::ElementScvfIndexMap<GridGeometry>;

    static constexpr int dim = GridGeometry::GridView::dimension;
    static constexpr int interfaceDim = dim-1;

    //! checks if the number of interface faces is correct for the given geometry type
    template<class SDElement>
    bool checkNumberOfInterfaceFaces_(const SDElement& element, int numFoundFaces) const
    {
        // in 2d, there should always be 2
        if constexpr (dim == 2) { return numFoundFaces == 2; }
        else if constexpr (dim == 3)
        {
            const auto& eg = element.geometry();
            if (eg.type().isTetrahedron()) return 3;
            else if (eg.type().isCube()) return 4;
            else DUNE_THROW(Dune::InvalidStateException, "Unsupported geometry type");
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Unsupported grid dimension");
    }

public:
    /*!
     * \brief TODO doc me.
     */
    template<class InterfaceGridGeometry, class Glue>
    InterfaceTraceOperator(std::shared_ptr<SubDomainSolver> subDomainSolverPtr,
                           const InterfaceGridGeometry& interfaceGridGeometry,
                           const Glue& glue)
    : subDomainSolverPtr_(subDomainSolverPtr)
    {
        coupledScvfMap_ = InterfaceTraceOperatorHelper::findCoupledScvfs(*subDomainSolverPtr->gridGeometryPointer(),
                                                                         interfaceGridGeometry,
                                                                         glue);
    }


    /*!
     * \brief TODO doc me.
     */
    template<class SolutionVector>
    SolutionVector recoverTraceSolution(const SolutionVector& sol) const
    {
        const auto& gridGeometry = *subDomainSolverPtr_->gridGeometryPointer();
        const auto& gridVariables = *subDomainSolverPtr_->gridVariablesPointer();

        // set up pressure vector with reconstructed face pressures
        std::vector<std::size_t> visitedElements;
        SolutionVector sdPressures, sdElementFaceAreas;
        sdPressures.resize(gridGeometry.gridView().size(0));
        sdElementFaceAreas.resize(gridGeometry.gridView().size(0));
        visitedElements.reserve(gridGeometry.gridView().size(0));
        sdElementFaceAreas = 0.0;
        sdPressures = 0.0;

        for (const auto& entry : coupledScvfMap_)
        {
            const auto sdElemIdx = entry.first;
            const auto scvfIndices = entry.second;
            const auto sdElement = gridGeometry.element(sdElemIdx);

            // check if the number of faces is what is expected for this geometry type
            assert(checkNumberOfInterfaceFaces_(sdElement, scvfIndices.size()));

            auto fvGeometry = localView(gridGeometry);
            auto elemVolVars = localView(gridVariables.curGridVolVars());
            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());

            fvGeometry.bind(sdElement);
            elemVolVars.bind(sdElement, fvGeometry, sol);
            elemFluxVarsCache.bind(sdElement, fvGeometry,  elemVolVars);

            for (const auto& scvfIdx : scvfIndices)
            {
                const auto& scvf = fvGeometry.scvf(scvfIdx);

                // function to add face pressures to grid-wide vector
                auto addFacePressure = [&] (const auto& iv, const auto& handle)
                {
                    using IV = std::decay_t<decltype(iv)>;
                    typename IV::Traits::MatVecTraits::FaceVector facePressures;
                    facePressures.resize(iv.numUnknowns());
                    handle.AB().mv(handle.uj(), facePressures);

                    // add Neumann and (maybe) gravity terms
                    static const bool enableGravity = getParam<bool>("Problem.EnableGravity", false);
                    auto rhs = handle.N();
                    if (enableGravity) rhs += handle.deltaG();
                    handle.A().umv(rhs, facePressures);

                    for (unsigned int i = 0; i < iv.numFaces(); ++i)
                    {
                        if (iv.localScvf(i).gridScvfIndex() == scvf.index())
                        {
                            sdPressures[sdElemIdx] += scvf.area()*facePressures[iv.localScvf(i).localDofIndex()];
                            sdElementFaceAreas[sdElemIdx] += scvf.area();
                            visitedElements.push_back(sdElemIdx);
                            return;
                        }
                    }

                    DUNE_THROW(Dune::InvalidStateException, "Could not find face");
                };

                if (gridGeometry.vertexUsesSecondaryInteractionVolume(scvf.vertexIndex()))
                {
                    const auto& iv = elemFluxVarsCache.secondaryInteractionVolume(scvf);
                    const auto& handle = elemFluxVarsCache.secondaryDataHandle(scvf).advectionHandle();
                    addFacePressure(iv, handle);
                }
                else
                {
                    const auto& iv = elemFluxVarsCache.primaryInteractionVolume(scvf);
                    const auto& handle = elemFluxVarsCache.primaryDataHandle(scvf).advectionHandle();
                    addFacePressure(iv, handle);
                }
            }
        }

        // remove duplicates
        auto& v = visitedElements;
        std::sort(v.begin(), v.end());
        v.erase( std::unique(v.begin(), v.end()), v.end() );

        // scale pressures
        for (auto visitedIdx : visitedElements)
            sdPressures[visitedIdx] /= sdElementFaceAreas[visitedIdx];

        return sdPressures;
    }

    /*!
     * \brief TODO doc me.
     */
    template<class SolutionVector>
    SolutionVector recoverTraceFlux(const SolutionVector& sol) const
    {
        const auto& gridGeometry = *subDomainSolverPtr_->gridGeometryPointer();

        // set up vector with interface flux per cell
        SolutionVector cellFluxes;
        SolutionVector cellAreas;
        std::vector<std::size_t> visitedElements;
        cellFluxes.resize(gridGeometry.gridView().size(0));
        cellAreas.resize(gridGeometry.gridView().size(0));
        visitedElements.reserve(gridGeometry.gridView().size(0));
        cellFluxes = 0.0;
        cellAreas = 0.0;

        for (const auto& entry : coupledScvfMap_)
        {
            const auto sdElemIdx = entry.first;
            const auto scvfIdx = entry.second[0];
            const auto sdElement = gridGeometry.element(sdElemIdx);

            auto fvGeometry = localView(gridGeometry);
            auto elemVolVars = localView(subDomainSolverPtr_->gridVariablesPointer()->curGridVolVars());
            auto elemFluxVarsCache = localView(subDomainSolverPtr_->gridVariablesPointer()->gridFluxVarsCache());

            fvGeometry.bind(sdElement);
            elemVolVars.bind(sdElement, fvGeometry, sol);
            elemFluxVarsCache.bind(sdElement, fvGeometry, elemVolVars);

            const auto& scvf = fvGeometry.scvf(scvfIdx);
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            const auto& insideVolVars = elemVolVars[insideScv];
            const auto& problem = elemVolVars.gridVolVars().problem();

            typename SubDomainSolver::FluxVariables fluxVars;
            fluxVars.init(problem, sdElement, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

            auto up = [] (const auto& vv) { return vv.mobility(); };
            visitedElements.push_back(sdElemIdx);
            cellAreas[sdElemIdx] += scvf.area();
            cellFluxes[sdElemIdx] += fluxVars.advectiveFlux(0, up)
                                     /insideVolVars.extrusionFactor();
        }

        // remove duplicates
        auto& v = visitedElements;
        std::sort(v.begin(), v.end());
        v.erase( std::unique(v.begin(), v.end()), v.end() );

        // scale fluxes
        for (auto visitedIdx : visitedElements)
            cellFluxes[visitedIdx] /= cellAreas[visitedIdx];

        return cellFluxes;
    }

    //! Returns the map containing the scvfs lying on the interface
    const ElementScvfIndexMap& coupledScvfMap() const
    { return coupledScvfMap_; }

private:
    std::shared_ptr<SubDomainSolver> subDomainSolverPtr_;
    ElementScvfIndexMap coupledScvfMap_;
};

} // end namespace Dumux

#endif
