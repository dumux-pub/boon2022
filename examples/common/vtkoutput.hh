// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief TODO doc me
 */
#ifndef DUMUX_MORTAR_COMMON_VTK_OUTPUT_HH
#define DUMUX_MORTAR_COMMON_VTK_OUTPUT_HH

#include <memory>
#include <utility>

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include "types.hh"

namespace Dumux {

template<class SubDomainSolver, class Interface>
class MortarVTKOutput
{
    using InterfaceTraits = typename Interface::Traits;
    using InterfaceSolution = typename InterfaceTraits::SolutionVector;
    using InterfaceGridView = typename InterfaceTraits::GridView;
    using InterfaceVTKWriter = Dune::VTKWriter<InterfaceGridView>;
    using InterfaceVTKSequenceWriter = Dune::VTKSequenceWriter<InterfaceGridView>;
    using BlockType = typename InterfaceSolution::block_type;
    using IFGridFunction = std::decay_t<decltype(
        Dune::Functions::makeDiscreteGlobalBasisFunction<BlockType>(
            std::declval<Interface>().gridGeometry().feBasis(), InterfaceSolution{}
        )
    )>;

public:
    MortarVTKOutput(ContainerOfPointers<SubDomainSolver> sdSolvers,
                    ContainerOfPointers<Interface> interfaces)
    : sdSolvers_(sdSolvers)
    , interfaces_(interfaces)
    {
        interfaceWriters_.resize(interfaces_.size());
        interfaceSequenceWriters_.resize(interfaces_.size());

        for (auto ifPtr : interfaces)
        {
            interfaceWriters_[ifPtr->id()] = makePointer<InterfaceVTKWriter>(
                ifPtr->gridGeometry().gridView(),
                InterfaceTraits::VTKDataMode
            );

            interfaceSequenceWriters_[ifPtr->id()] = makePointer<InterfaceVTKSequenceWriter>(
                interfaceWriters_[ifPtr->id()],
                getParamFromGroup<std::string>(
                    "Interface" + std::to_string(ifPtr->id()),
                    "Problem.Name", "Interface" + std::to_string(ifPtr->id())
                )
            );
        }

        for (auto sdSolverPtr : sdSolvers)
            sdSolverPtr->problem().addOutputFields(sdSolverPtr->outputModule());
    }

    void write()
    {
        std::for_each(sdSolvers_.begin(),
                      sdSolvers_.end(),
                      [&] (auto ptr) {
            ptr->write(static_cast<double>(count_));
        });
        std::for_each(interfaceSequenceWriters_.begin(),
                      interfaceSequenceWriters_.end(),
                      [&] (auto ptr) {
            ptr->write(static_cast<double>(count_));
        });
        count_++;
    }

    void addInterfaceFields(const std::string& name,
                            const Container<InterfaceSolution>& values,
                            Dune::VTK::FieldInfo::Type type = Dune::VTK::FieldInfo::Type::scalar,
                            int ncomps = 1)
    {
        for (auto ifPtr : interfaces_)
        {
            interfaceGridFunctions_.emplace_back(makePointer<IFGridFunction>(
                Dune::Functions::makeDiscreteGlobalBasisFunction<BlockType>(
                    ifPtr->gridGeometry().feBasis(),
                    values[ifPtr->id()]
                )
            ));
            if (InterfaceTraits::basisOrder == 0)
                interfaceWriters_[ifPtr->id()]->addCellData(
                    *interfaceGridFunctions_.back(),
                    Dune::VTK::FieldInfo(name, type, ncomps)
                );
            else
                interfaceWriters_[ifPtr->id()]->addVertexData(
                    *interfaceGridFunctions_.back(),
                    Dune::VTK::FieldInfo(name, type, ncomps)
                );
        }
    }

    template<class SubDomainSolutionContainers>
    void addSubDomainFields(const std::string& name,
                            const SubDomainSolutionContainers& values)
    {
        std::for_each(sdSolvers_.begin(), sdSolvers_.end(), [&] (auto ptr) {
            ptr->outputModule().addField(values[ptr->id()], name);
        });
    }

private:
    ContainerOfPointers<SubDomainSolver> sdSolvers_;
    ContainerOfPointers<Interface> interfaces_;
    ContainerOfPointers<InterfaceVTKWriter> interfaceWriters_;
    ContainerOfPointers<InterfaceVTKSequenceWriter> interfaceSequenceWriters_;
    ContainerOfPointers<IFGridFunction> interfaceGridFunctions_;
    int count_{0};
};

}  // end namespace Dumux

#endif
