// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_ONEP_MORTAR_INTERFACE_TRACE_OPERATOR_HELPER_HH
#define DUMUX_ONEP_MORTAR_INTERFACE_TRACE_OPERATOR_HELPER_HH

#include <unordered_map>

#include <dumux/common/indextraits.hh>
#include <dumux/geometry/geometryintersection.hh>
#include <dumux/discretization/functionspacebasis.hh>
#include <dumux/discretization/projection/projector.hh>
#include <dumux/multidomain/glue.hh>

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
class InterfaceTraceOperatorHelper
{
    //! Extract index type from grid geometry and state index set type
    template<class GG> using IndexType = typename IndexTraits< typename GG::GridView >::GridIndex;
    template<class GG> using IndexVector = std::vector< IndexType<GG> >;

public:
    //! Stores the element indices and scvfs of those elements that lie on an interface
    template<class GG>
    using ElementScvfIndexMap = std::unordered_map< IndexType<GG>, IndexVector<GG> >;

    /*!
     * \brief TODO doc me.
     */
    template<class SubDomainGridGeometry, class InterfaceGridGeometry, class GlueType>
    static void findCoupledElements(const SubDomainGridGeometry& sdGridGeometry,
                                    const InterfaceGridGeometry& interfaceGridGeometry,
                                    const GlueType& glue,
                                    std::vector<std::size_t>& coupledSdElements,
                                    std::vector<std::size_t>& coupledInterfaceElements)
    {
        // store which elements are coupled
        coupledSdElements.clear();
        coupledInterfaceElements.clear();
        coupledSdElements.reserve(glue.size());
        coupledInterfaceElements.reserve(glue.size());
        for (const auto& is : intersections(glue))
        {
            coupledSdElements.push_back( sdGridGeometry.elementMapper().index(is.domainEntity(0)) );
            for (unsigned int nIdx = 0; nIdx < is.numTargetNeighbors(); ++nIdx)
                coupledInterfaceElements.push_back( interfaceGridGeometry.elementMapper().index(is.targetEntity(nIdx)) );
        }

        // lambda to remove duplicates
        auto removeDuplicates = [] (auto& v)
        {
            std::sort(v.begin(), v.end());
            v.erase( std::unique(v.begin(), v.end()), v.end() );
        };

        removeDuplicates(coupledSdElements);
        removeDuplicates(coupledInterfaceElements);
    }

    /*!
     * \brief TODO doc me.
     */
    template<class SDGridGeometry, class InterfaceGridGeometry>
    static ElementScvfIndexMap<SDGridGeometry>
    findCoupledScvfs(const SDGridGeometry& sdGridGeometry,
                     const InterfaceGridGeometry& interfaceGridGeometry,
                     const std::vector<std::size_t>& coupledSdElements,
                     const std::vector<std::size_t>& coupledInterfaceElements)
    {
        using MortarElemGeometry = typename InterfaceGridGeometry::GridView::template Codim<0>::Entity::Geometry;
        using ScvfGeometry = typename SDGridGeometry::SubControlVolumeFace::Traits::Geometry;

        ElementScvfIndexMap<SDGridGeometry> map;
        for (auto eIdxMortar : coupledInterfaceElements)
        {
            const auto& mortarElement = interfaceGridGeometry.element(eIdxMortar);
            const auto& mortarElementGeometry = mortarElement.geometry();

            for (auto eIdxSubDomain : coupledSdElements)
            {
                const auto sdElement = sdGridGeometry.element(eIdxSubDomain);
                auto fvGeometry = localView(sdGridGeometry);
                fvGeometry.bind(sdElement);

                using std::abs;
                for (const auto& scvf : scvfs(fvGeometry))
                {
                    // check if face has an intersection with mortar element
                    using Algo = GeometryIntersection<ScvfGeometry, MortarElemGeometry>;
                    using Result = typename Algo::Intersection;

                    Result result;
                    if (Algo::intersection(scvf.geometry(), mortarElementGeometry, result))
                        map[eIdxSubDomain].push_back( scvf.index() );
                }
            }
        }

        // remove duplicates
        for (auto& entry : map)
        {
            auto& v = entry.second;
            std::sort(v.begin(), v.end());
            v.erase( std::unique(v.begin(), v.end()), v.end() );
        }

        return map;
    }

    /*!
     * \brief TODO doc me.
     */
    template<class SDGridGeometry, class InterfaceGridGeometry, class Glue>
    static ElementScvfIndexMap<SDGridGeometry>
    findCoupledScvfs(const SDGridGeometry& sdGridGeometry,
                     const InterfaceGridGeometry& interfaceGridGeometry,
                     const Glue& glue)
    {
        std::vector<std::size_t> coupledSdElements;
        std::vector<std::size_t> coupledInterfaceElements;
        findCoupledElements(sdGridGeometry, interfaceGridGeometry, glue,
                            coupledSdElements, coupledInterfaceElements);

        return findCoupledScvfs(sdGridGeometry, interfaceGridGeometry,
                                coupledSdElements, coupledInterfaceElements);
    }
};

} // end namespace Dumux

#endif
