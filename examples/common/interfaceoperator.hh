// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_ONEP_INTERFACE_OPERATOR_HH
#define DUMUX_MORTAR_ONEP_INTERFACE_OPERATOR_HH

#include <utility>

// tbb is required here
#include <tbb/parallel_for.h>
#include <tbb/task_arena.h>

#include <dune/istl/operators.hh>

#include "projector.hh"
#include "types.hh"

namespace Dumux {

template<typename SolutionVector>
struct KernelProjector
{
    virtual SolutionVector toKernel(const SolutionVector& x) const = 0;
    virtual SolutionVector fromKernel(const SolutionVector& x) const = 0;
};

template<typename SolutionVector>
struct IdentityKernelProjector : public KernelProjector<SolutionVector>
{
    virtual SolutionVector toKernel(const SolutionVector& x) const override { return x; }
    virtual SolutionVector fromKernel(const SolutionVector& x) const override { return x; }
};

template<typename SolutionVector>
class CoarseMatrixKernelProjector : public KernelProjector<SolutionVector>
{
    using Scalar = typename SolutionVector::field_type;
    using Matrix = Dune::BCRSMatrix<Dune::FieldMatrix<Scalar, 1, 1>>;

public:
    template<class DenseMatrix, class BlockMatix>
    CoarseMatrixKernelProjector(const DenseMatrix& BBTInverse, const BlockMatix& B)
    : numFloatingSubDomains_(B.N())
    {
        if (numFloatingSubDomains_ > 0)
        {
            std::cout << "Constructing kernel projector ..." << std::endl;
            const auto blockBBTInverse = convertToBlockMatrix_(BBTInverse);

            Matrix tmp, tmp2;
            Dune::matMultMat(tmp, blockBBTInverse, B);
            Dune::transposeMatMultMat(tmp2, B, tmp);
            if (tmp2.M() != tmp2.N())
                DUNE_THROW(Dune::InvalidStateException, "Expected a square matrix");

            Dune::MatrixIndexSet pattern(tmp2.N(), tmp2.M());
            pattern.import(tmp2);
            for (std::size_t i = 0; i < tmp2.N(); ++i)
                pattern.add(i, i);
            pattern.exportIdx(P_);

            P_ = 0.0;
            for (std::size_t i = 0; i < P_.N(); ++i)
                P_[i][i] = 1.0;
            for (auto rowIt = tmp2.begin(); rowIt != tmp2.end(); ++rowIt)
                for (auto colIt = rowIt->begin(); colIt != rowIt->end(); ++colIt)
                    P_[rowIt.index()][colIt.index()] -= *colIt;
            std::cout << " ... done." << std::endl;
        }
    }

    virtual SolutionVector toKernel(const SolutionVector& x) const override
    {
        if (numFloatingSubDomains_ == 0)
            return x;

        SolutionVector result(P_.N());
        P_.mv(x, result);
        return result;
    }

    virtual SolutionVector fromKernel(const SolutionVector& x) const override
    {
        if (numFloatingSubDomains_ == 0)
            return x;

        SolutionVector result(P_.N());
        P_.mtv(x, result);
        return result;
    }

private:
    template<typename DenseMatrix>
    Matrix convertToBlockMatrix_(const DenseMatrix& m) const
    {
        Dune::MatrixIndexSet pattern(m.N(), m.M());
        for (std::size_t i = 0; i < m.N(); ++i)
            for (std::size_t j = 0; j < m.M(); ++j)
                pattern.add(i, j);

        Matrix result;
        pattern.exportIdx(result);
        for (std::size_t i = 0; i < m.N(); ++i)
            for (std::size_t j = 0; j < m.M(); ++j)
                result[i][j] = m[i][j];

        return result;
    }

    std::size_t numFloatingSubDomains_;
    Matrix P_;
};

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class Interface, class SolutionVector>
void extractInterfaceSolutions(const ContainerOfPointers<Interface>& interfaces,
                               std::vector<SolutionVector>& solutionVectors,
                               const SolutionVector& solution)
{
    std::vector<std::size_t> numDofs; numDofs.reserve(interfaces.size());
    std::vector<std::size_t> offsets; offsets.reserve(interfaces.size());
    std::for_each(interfaces.begin(), interfaces.end(), [&] (auto ifPtr) {
        numDofs.push_back(ifPtr->gridGeometry().numDofs());
    });
    std::accumulate(numDofs.begin(), numDofs.end(), std::size_t{0},
        [&] (std::size_t cur, const std::size_t n) {
            offsets.push_back(cur);
            return cur + n;
        }
    );

    solutionVectors.resize(interfaces.size());
    std::for_each(interfaces.begin(), interfaces.end(), [&] (auto ifPtr) {
        const auto id = ifPtr->id();
        solutionVectors[id].resize(numDofs[id]);
        assert(solution.size() >= offsets[id] + numDofs[id]);
        std::copy(solution.begin() + offsets[id],
                  solution.begin() + offsets[id] + numDofs[id],
                  solutionVectors[id].begin());
    });
}

template<class MortarSolution>
void mergeMortarSolutions(MortarSolution& result, const std::vector<MortarSolution>& mortarSolutions)
{
    const auto numDofs = std::accumulate(
        mortarSolutions.begin(), mortarSolutions.end(), std::size_t{0},
        [] (std::size_t cur, const auto& msol) {
            return cur + msol.size();
        }
    );

    std::size_t curIdx = 0;
    result.resize(numDofs);
    for (const auto& msol : mortarSolutions)
        for (const auto& entry : msol)
            result[curIdx++] = entry;
}

template<class SubDomainSolver>
void solveSubDomains(const std::vector<std::shared_ptr<SubDomainSolver>>& sdSolverPtrs)
{
    static const int numThreads = getParam<int>("Parallelism.MaxNumThreads", 4);
    tbb::task_arena arena(numThreads);
    arena.execute([&] {
        tbb::parallel_for(tbb::blocked_range<std::size_t>(0, sdSolverPtrs.size()),
        [&](const tbb::blocked_range<size_t>& r)
        {
            for (std::size_t i=r.begin(); i<r.end(); ++i)
                sdSolverPtrs[i]->solve();
        });
    });
}

template<class MortarSolution, class Interface>
void setMortarProjectionsInSubDomains(const MortarSolution& mortarSolution,
                                      const std::vector<std::shared_ptr<Interface>>& interfaces)
{
    // extract interface sub-solutions
    std::vector<MortarSolution> ifSolutions;
    extractInterfaceSolutions(interfaces, ifSolutions, mortarSolution);
    std::for_each(interfaces.begin(), interfaces.end(), [&] (auto ifPtr) {
        MortarSolution p1, p2;
        ProjectionDetail::projectToSubDomain(ifPtr->positiveProjector(),
                                             ifPtr->negativeProjector(),
                                             ifSolutions[ifPtr->id()], p1, p2);

        ifPtr->positiveNeighborSolver().problemPointer()->setMortarProjection(ifPtr->id(), p1);
        ifPtr->negativeNeighborSolver().problemPointer()->setMortarProjection(ifPtr->id(), p2);
    });
}

template<class MortarSolution, class Interface, class SubDomainSolution>
void computeTraceProjection(MortarSolution& result,
                            const Interface& interface,
                            const SubDomainSolution& solution,
                            InterfaceSide side)
{
    const auto trace = side == InterfaceSide::positive
                       ? interface.positiveTraceOperator().recoverTraceSolution(solution)
                       : interface.negativeTraceOperator().recoverTraceSolution(solution);
    MortarSolution unused;
    if (side == InterfaceSide::positive)
        ProjectionDetail::projectFromSubDomain(
            interface.positiveProjector(),
            interface.negativeProjector(),
            interface.positiveTraceOperator().recoverTraceSolution(solution),
            interface.negativeNeighborSolver().solution(),
            result,
            unused
        );
    else
        ProjectionDetail::projectFromSubDomain(
            interface.positiveProjector(),
            interface.negativeProjector(),
            interface.positiveNeighborSolver().solution(),
            interface.negativeTraceOperator().recoverTraceSolution(solution),
            unused,
            result
        );
}

template<class MortarSolution, class Interface>
void computePressureJumpAcrossMortar(MortarSolution& result,
                                     const std::vector<std::shared_ptr<Interface>>& interfaces)
{
    std::vector<MortarSolution> deltaps(interfaces.size());
    for (const auto ifPtr : interfaces)
    {
        const auto& posSolver = ifPtr->positiveNeighborSolver();
        const auto& posTraceOp = ifPtr->positiveTraceOperator();
        const auto& posProjector = ifPtr->positiveProjector();

        const auto& negSolver = ifPtr->negativeNeighborSolver();
        const auto& negTraceOp = ifPtr->negativeTraceOperator();
        const auto& negProjector = ifPtr->negativeProjector();

        MortarSolution t1, t2;
        t1 = posTraceOp.recoverTraceSolution(posSolver.solution());
        t2 = negTraceOp.recoverTraceSolution(negSolver.solution());
        t2 *= -1.0;

        MortarSolution p1, p2;
        ProjectionDetail::projectFromSubDomain(posProjector, negProjector, t1, t2, p1, p2);

        if (p1.size() != p2.size())
            DUNE_THROW(Dune::InvalidStateException, "Trace projections do not have the same size");
        if (p1.size() != ifPtr->gridGeometry().numDofs())
            DUNE_THROW(Dune::InvalidStateException, "Trace projections do not have size of interface space");

        deltaps[ifPtr->id()] = p1;
        deltaps[ifPtr->id()] += p2;
    }

    mergeMortarSolutions(result, deltaps);
}

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class Interface,
         class SubDomainSolver,
         class MortarSolutionVector>
class OnePMortarInterfaceOperator
: public Dune::LinearOperator< MortarSolutionVector, MortarSolutionVector >
{
    using FieldType =  typename MortarSolutionVector::field_type;

    using SDSolverPtr = std::shared_ptr<SubDomainSolver>;
    using InterfacePtr = std::shared_ptr<Interface>;
    using KernelProjector = Dumux::KernelProjector<MortarSolutionVector>;

public:
    explicit OnePMortarInterfaceOperator(std::vector<InterfacePtr> interfacePtrs,
                                         std::vector<SDSolverPtr> sdSolverPtrs,
                                         std::shared_ptr<KernelProjector> kp
                                            = std::make_shared<IdentityKernelProjector<MortarSolutionVector>>())
    : interfacePtrs_(interfacePtrs)
    , sdSolverPtrs_(sdSolverPtrs)
    , kernelProjector_(kp)
    {}


    //! apply operator to x:  \f$ y = A(x) \f$
    virtual void apply(const MortarSolutionVector& mortarFluxes, MortarSolutionVector& r) const
    {
        setMortarProjectionsInSubDomains(mortarFluxes, interfacePtrs_);
        solveSubDomains(sdSolverPtrs_);
        computePressureJumpAcrossMortar(r, interfacePtrs_);
        r = kernelProjector_->fromKernel(r);
    }

    //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
    virtual void applyscaleadd(FieldType alpha, const MortarSolutionVector& x, MortarSolutionVector& y) const
    {
        MortarSolutionVector yTmp;

        apply(x, yTmp);
        yTmp *= alpha;

        y += yTmp;
    }

    //! Category of the solver (see SolverCategory::Category)
    virtual Dune::SolverCategory::Category category() const
    { return Dune::SolverCategory::sequential; }

private:
    std::vector<InterfacePtr> interfacePtrs_;
    std::vector<SDSolverPtr> sdSolverPtrs_;
    std::shared_ptr<KernelProjector> kernelProjector_;
};

} // end namespace Dumux

#endif
