// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \todo TODO: doc me
 */
#ifndef DUMUX_MORTAR_SUBDOMAIN_PROBLEM_HH
#define DUMUX_MORTAR_SUBDOMAIN_PROBLEM_HH

#include <optional>

#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "mortarvariabletype.hh"
#include "traceoperatorhelper.hh"
#include "floating_constraint/lagrangemultiplierevaluator.hh"

namespace Dumux {

template<class TypeTag>
class SubDomainProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Impl = GetPropType<TypeTag, Properties::Problem>;


    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<1>;

    using CoupledScvfMap = typename InterfaceTraceOperatorHelper::ElementScvfIndexMap<GridGeometry>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

public:
    //! Export spatial parameters type
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    //! The constructor
    SubDomainProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                     std::shared_ptr<SpatialParams> spatialParams,
                     const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , mv_(MortarVariableType::flux)
    {
        problemName_ = getParamFromGroup<std::string>(paramGroup, "Problem.Name", this->paramGroup());
        useHomogeneousSetup_ = false;
    }

    const std::string& name() const
    { return problemName_; }

    Scalar temperature() const
    { return 273.15 + 10; }

    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        if (isOnMortarInterface(element, scvf))
        {
            BoundaryTypes values;
            if (useNeumannAtInterface_())
                values.setAllNeumann();
            else
                values.setAllDirichlet();
            return values;
        }
        return this->asImp_().boundaryTypesAtFace(element, scvf);
    }

    BoundaryTypes boundaryTypesAtFace(const Element& element,
                                      const SubControlVolumeFace& scvf) const
    { return this->asImp_().boundaryTypesAtPos(scvf.ipGlobal()); }

    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        NumEqVector result(0.0);

        if (!useHomogeneousSetup_)
            result += this->asImp_().sourceAtScv(element, fvGeometry, elemVolVars, scv);

        result[0] += std::accumulate(
            mortarSourceTerms_.begin(),
            mortarSourceTerms_.end(),
            Scalar{0.0},
            [&scv] (Scalar current, const auto& ifIdSourcesPair) {
                return current + ifIdSourcesPair.second[scv.elementIndex()];
            }
        );

        if (lmEvaluator_)
            result[0] -= lmEvaluator_->getLagrangeMultiplier();

        return result;
    }

    template<class ElementVolumeVariables>
    NumEqVector sourceAtScv(const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume& scv) const
    { return this->asImp_().sourceAtPos(scv.center()); }

    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto mortarId = mortarInterfaceIndex(element, scvf);
        if (mortarId) {
            static_assert(!isBox);
            return mortarPressures_.at(*mortarId)[scvf.insideScvIdx()];
        }
        if (!useHomogeneousSetup_)
            return dirichletAtFace(element, scvf);
        return PrimaryVariables(0.0);
    }

    PrimaryVariables dirichletAtFace(const Element& element,
                                     const SubControlVolumeFace& scvf) const
    { return this->asImp_().dirichletAtPos(scvf.ipGlobal()); }

    template<class ElementVolumeVariables, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    { return neumann(element, fvGeometry, elemVolVars, scvf); }

    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        const auto mortarId = mortarInterfaceIndex(element, scvf);
        if (mortarId)
        {
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            auto flux = mortarProjections_.at(*mortarId)[insideScv.elementIndex()];
            flux *= elemVolVars[insideScv].density();
            return isOnNegativeMortarSide(*mortarId) ? NumEqVector(-1.0*flux)
                                                     : NumEqVector(flux);
        }
        if (!useHomogeneousSetup_)
            return neumannAtFace(element, fvGeometry, elemVolVars, scvf);
        return NumEqVector(0.0);
    }

    template<class ElementVolumeVariables>
    NumEqVector neumannAtFace(const Element& element,
                              const FVElementGeometry& fvGeometry,
                              const ElementVolumeVariables& elemVolVars,
                              const SubControlVolumeFace& scvf) const
    { return this->asImp_().neumannAtPos(scvf.ipGlobal()); }

    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    void setMortarProjection(std::size_t interfaceId, SolutionVector p)
    {
        mortarProjections_[interfaceId] = p;
    }

    void setMortarPressure(std::size_t interfaceId, SolutionVector p)
    {
        mortarPressures_[interfaceId] = p;
    }

    void removeSourcesFromInterface()
    {
        mortarSourceTerms_.clear();
    }

    void setSourcesFromInterface(std::size_t interfaceId, SolutionVector s)
    {
        mortarSourceTerms_[interfaceId] = s;
    }

    bool usesHomogeneousSetup() const
    { return useHomogeneousSetup_; }

    void setUseHomogeneousSetup(bool value)
    {
        useHomogeneousSetup_ = value;
    }

    std::optional<std::size_t> mortarInterfaceIndex(const Element& element,
                                                    const SubControlVolumeFace& scvf) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        for (const auto& pair : coupledScvfMaps_)
        {
            auto it = pair.second.find(eIdx);
            if (it != pair.second.end())
            {
                const auto& scvfList = it->second;
                if ( std::find(scvfList.begin(),
                               scvfList.end(),
                               scvf.index()) != scvfList.end() )
                    return pair.first;
            }
        }

        return std::optional<std::size_t>();
    }

    bool isOnMortarInterface(const Element& element,
                             const SubControlVolumeFace& scvf) const
    { return static_cast<bool>(mortarInterfaceIndex(element, scvf)); }


    bool isOnNegativeMortarSide(std::size_t interfaceId) const
    {
        auto it = isOnNegativeMortarSide_.find(interfaceId);
        if (it == isOnNegativeMortarSide_.end())
            DUNE_THROW(Dune::InvalidStateException, "Interface not registered");
        return it->second;
    }

    void setInterfaceSide(std::size_t interfaceId, InterfaceSide side)
    {
        if (side == InterfaceSide::negative)
            isOnNegativeMortarSide_[interfaceId] = true;
        else if (side == InterfaceSide::positive)
            isOnNegativeMortarSide_[interfaceId] = false;
        else
            DUNE_THROW(Dune::InvalidStateException, "Unsupported interface type");
    }

    void setCouplingScvfMap(std::size_t interfaceId, const CoupledScvfMap& map)
    {
        coupledScvfMaps_[interfaceId] = map;
    }

    template<class OutputModule>
    void addOutputFields(OutputModule& outputModule) const
    {
        for (auto& p : mortarProjections_)
            outputModule.addField(
                p.second,
                "mortar_" + std::to_string(p.first),
                OutputModule::FieldType::element
            );
    }

    void setLagrangeMultiplierEvaluator(std::shared_ptr<LagrangeMultiplierEvaluator<Scalar>> ptr)
    {
        lmEvaluator_ = ptr;
    }

    void unsetLagrangeMultiplierEvaluator()
    {
        lmEvaluator_ = nullptr;
    }

    void setMortarVariableType(MortarVariableType mv)
    {
        mv_ = mv;
    }

private:
    bool useNeumannAtInterface_() const
    { return mv_ == MortarVariableType::flux; }

    std::string problemName_;
    std::unordered_map<std::size_t, CoupledScvfMap> coupledScvfMaps_;
    std::unordered_map<std::size_t, SolutionVector> mortarPressures_;
    std::unordered_map<std::size_t, SolutionVector> mortarProjections_;
    std::unordered_map<std::size_t, SolutionVector> mortarSourceTerms_;
    std::unordered_map<std::size_t, bool> isOnNegativeMortarSide_;
    std::shared_ptr<LagrangeMultiplierEvaluator<Scalar>> lmEvaluator_{nullptr};
    bool useHomogeneousSetup_;
    MortarVariableType mv_;
};

} // end namespace Dumux

#endif
