// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_PROJECTOR_HH
#define DUMUX_MORTAR_PROJECTOR_HH

#include <dune/common/indices.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/matrixmatrix.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/matrixconverter.hh>

namespace Dumux {

//! Forward declarations of the implemented projectors
template<class MortarSolution> class FlatProjector;
template<class MortarSolution> class SharpProjector;

//! Details on how to project from and to sub-domains
//! The functions provided here allow for compatibility
//! among different projectors that do things differently
namespace ProjectionDetail {

/*!
 * \brief Default projection (e.g. flat projection)
 */
template<class Projector, class SolutionVector>
void projectFromSubDomain(const Projector& positiveProjector,
                          const Projector& negativeProjector,
                          const SolutionVector& posSolution,
                          const SolutionVector& negSolution,
                          SolutionVector& posResult,
                          SolutionVector& negResult)
{
    posResult = positiveProjector.projectFromSubDomain(posSolution);
    negResult = negativeProjector.projectFromSubDomain(negSolution);
}

/*!
 * \brief Sharp projection
 */
template<class SolutionVector>
void projectFromSubDomain(const SharpProjector<SolutionVector>& positiveProjector,
                          const SharpProjector<SolutionVector>& negativeProjector,
                          const SolutionVector& posSolution,
                          const SolutionVector& negSolution,
                          SolutionVector& posResult,
                          SolutionVector& negResult)
{
    // the two projectors are the same, we use the first one here
    auto p = positiveProjector.projectToMortar(posSolution, negSolution);

    using namespace Dune::Indices;
    posResult = std::move(p[_0]);
    negResult = std::move(p[_1]);
}

/*!
 * \brief Default projection (e.g. flat projection)
 */
template<class Projector, class SolutionVector>
void projectToSubDomain(const Projector& positiveProjector,
                        const Projector& negativeProjector,
                        const SolutionVector& interfaceSolution,
                        SolutionVector& posResult,
                        SolutionVector& negResult)
{
    posResult = positiveProjector.projectToSubDomain(interfaceSolution);
    negResult = negativeProjector.projectToSubDomain(interfaceSolution);
}

/*!
 * \brief Sharp projection
 */
template<class SolutionVector>
void projectToSubDomain(const SharpProjector<SolutionVector>& positiveProjector,
                        const SharpProjector<SolutionVector>& negativeProjector,
                        const SolutionVector& interfaceSolution,
                        SolutionVector& posResult,
                        SolutionVector& negResult)
{
    const auto x1 = interfaceSolution;
    auto x2 = interfaceSolution;
    x2 *= -1.0;

    // the two projectors are the same, we use the first one here
    auto p = positiveProjector.projectToSubDomain(x1, x2);

    using namespace Dune::Indices;
    posResult = std::move(p[_0]);
    negResult = std::move(p[_1]);
    negResult *= -1.0;
}

} // end namespace ProjectionDetail

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class MortarSolution>
class FlatProjector
{
    using Scalar = typename MortarSolution::field_type;
    using Projector = Dumux::Projector<Scalar>;
    using Matrix = typename Projector::Matrix;

public:
    //! Export the type used for projection matrices
    using ProjectionMatrix = Matrix;

    /*!
     * \brief TODO doc me.
     */
    FlatProjector(Matrix&& massMatrix, Matrix&& projMatrix,
                  Matrix&& mortarMassMatrix)
    : massMatrix_(mortarMassMatrix)
    , B_(projMatrix)
    , mortarToSubDomainProjector_(std::move(massMatrix), std::move(projMatrix))
    {}

    /*!
     * \brief TODO doc me.
     */
    MortarSolution projectToSubDomain(const MortarSolution& x) const
    { return mortarToSubDomainProjector_.project(x); }

    /*!
     * \brief TODO doc me.
     */
    MortarSolution projectFromSubDomain(const MortarSolution& x) const
    {
        MortarSolution result;
        result.resize(B_.M());

        B_.mtv(x, result);
        return result;
    }

    const Matrix& massMatrix() const
    { return massMatrix_; }

private:
    Matrix massMatrix_;
    ProjectionMatrix B_;
    Projector mortarToSubDomainProjector_;
};

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class MortarSolution>
class SharpProjector
{
    using Scalar = typename MortarSolution::field_type;
    using BlockType = typename MortarSolution::block_type;
    using SubMatrix = Dune::BCRSMatrix< Dune::FieldMatrix<Scalar, 1, 1> >;
    static_assert(BlockType::size() == 1, "Currently only block size of 1 is supported");

    // Helper aliases
    template<class... ColTypes> using MatrixRow = Dune::MultiTypeBlockVector<ColTypes...>;
    template<class... RowTypes> using Matrix = Dune::MultiTypeBlockMatrix<RowTypes...>;

    // Aliases for the matrix and vector types
    using Vector2 = Dune::MultiTypeBlockVector<MortarSolution, MortarSolution>;
    using Vector3 = Dune::MultiTypeBlockVector<MortarSolution, MortarSolution, MortarSolution>;

    using MatrixRow2 = MatrixRow<SubMatrix, SubMatrix>;
    using MatrixRow3 = MatrixRow<SubMatrix, SubMatrix, SubMatrix>;

    using Matrix23 = Matrix<MatrixRow3, MatrixRow3>;
    using Matrix33 = Matrix<MatrixRow3, MatrixRow3, MatrixRow3>;
    using Matrix32 = Matrix<MatrixRow2, MatrixRow2, MatrixRow2>;

public:
    /*!
     * \brief TODO doc me.
     */
    template<class ProjectionMatrix>
    SharpProjector(const ProjectionMatrix& M1, const ProjectionMatrix& B1,
                   const ProjectionMatrix& M2, const ProjectionMatrix& B2)
    : mass_(M1)
    {
        // create identity matrices
        SubMatrix I1, I2;

        Dune::MatrixIndexSet pattern1, pattern2;
        pattern1.resize(B1.N(), B1.N());
        pattern2.resize(B2.N(), B2.N());
        for (std::size_t i = 0; i < B1.N(); ++i)
            pattern1.add(i, i);
        for (std::size_t i = 0; i < B2.N(); ++i)
            pattern2.add(i, i);

        pattern1.exportIdx(I1); I1 = 1.0;
        pattern2.exportIdx(I2); I2 = 1.0;

        // set up the transposed matrices using identities
        SubMatrix B1T, B2T;
        Dune::transposeMatMultMat(B1T, B1, I1);
        Dune::transposeMatMultMat(B2T, B2, I2);

        using namespace Dune::Indices;

        // fill the sub matrices
        I_[_0][_0] = I1;          I_[_0][_1] = SubMatrix{}; I_[_0][_2] = SubMatrix{};
        I_[_1][_0] = SubMatrix{}; I_[_1][_1] = I2;          I_[_1][_2] = SubMatrix{};

        A_[_0][_0] = M1;          A_[_0][_1] = SubMatrix{}; A_[_0][_2] = B1;
        A_[_1][_0] = SubMatrix{}; A_[_1][_1] = M2;          A_[_1][_2] = B2;
        A_[_2][_0] = B1T;         A_[_2][_1] = B2T;         A_[_2][_2] = SubMatrix{};

        C_[_0][_0] = B1;          C_[_0][_1] = SubMatrix{};
        C_[_1][_0] = SubMatrix{}; C_[_1][_1] = B2;
        C_[_2][_0] = SubMatrix{}; C_[_2][_1] = SubMatrix{};

        // set up the zero sub-matrix block sizes
        using IndexSet = Dune::MatrixIndexSet;

        IndexSet{I1.N(), I2.M()}.exportIdx(I_[_0][_1]); // I[0][1]
        IndexSet{I1.N(), B1.M()}.exportIdx(I_[_0][_2]); // I[0][2]
        IndexSet{I2.N(), I1.M()}.exportIdx(I_[_1][_0]); // I[1][0]
        IndexSet{I2.N(), B2.M()}.exportIdx(I_[_1][_2]); // I[1][2]

        IndexSet{M1.N(), M2.M()}.exportIdx(A_[_0][_1]); // A[0][1]
        IndexSet{M2.N(), M1.M()}.exportIdx(A_[_1][_0]); // A[1][0]
        IndexSet{B1.M(), B1.M()}.exportIdx(A_[_2][_2]); // A[2][2]

        IndexSet{B1.N(), B2.M()}.exportIdx(C_[_0][_1]); // C[0][1]
        IndexSet{B2.N(), B1.M()}.exportIdx(C_[_1][_0]); // C[1][0]
        IndexSet{B1.M(), B1.M()}.exportIdx(C_[_2][_0]); // C[2][0]
        IndexSet{B2.M(), B2.M()}.exportIdx(C_[_2][_1]); // C[2][1]
    }

    /*!
     * \brief TODO doc me.
     */
    Vector2 projectToSubDomain(const MortarSolution& x1,
                               const MortarSolution& x2) const
    {
        using namespace Dune::Indices;

        Vector2 x(x1, x2);

        // set up right hand side rhs = Cx
        Vector3 rhs;
        rhs[_0].resize(C_[_0][_0].N());
        rhs[_1].resize(C_[_1][_0].N());
        rhs[_2].resize(C_[_2][_0].N());
        C_.mv(x, rhs);

        // solve A*xTmp = rhs
        // We use the direct solver UMFPack here,
        // which doesn't support MultyTypeMatrices.
        // Therefore, we copy the values into a BCRS matrix beforehand
        auto ATmp = MatrixConverter<Matrix33>::multiTypeToBCRSMatrix(A_);
        auto rhsTmp = VectorConverter<Vector3>::multiTypeToBlockVector(rhs);
        auto xTmp = rhsTmp;

        UMFPackBackend solver;
        solver.solve(ATmp, xTmp, rhsTmp);

        // copy values back into MultiTypeVector (reuse rhs)
        VectorConverter<Vector3>::retrieveValues(rhs, xTmp);

        // restrict
        Vector2 xp;
        xp[_0].resize(I_[_0][_0].N());
        xp[_1].resize(I_[_1][_0].N());
        I_.mv(rhs, xp);

        return xp;
    }

    /*!
     * \brief TODO doc me.
     */
    Vector2 projectToMortar(const MortarSolution& x1,
                            const MortarSolution& x2) const
    {
        using namespace Dune::Indices;

        // set up right hand side
        Vector2 x(x1, x2);

        Vector3 rhs;
        rhs[_0].resize(I_[_0][_0].M());
        rhs[_1].resize(I_[_0][_1].M());
        rhs[_2].resize(I_[_0][_2].M());

        // compute rhs = I_^T*x
        I_[_0][_0].mtv(x[_0], rhs[_0]);
        I_[_1][_0].umtv(x[_1], rhs[_0]);
        I_[_0][_1].mtv(x[_0], rhs[_1]);
        I_[_1][_1].umtv(x[_1], rhs[_1]);
        I_[_0][_2].mtv(x[_0], rhs[_2]);
        I_[_1][_2].umtv(x[_1], rhs[_2]);

        // apply C
        Vector2 xp;
        xp[_0].resize(C_[_0][_0].M());
        xp[_1].resize(C_[_0][_1].M());

        C_[_0][_0].mtv(rhs[_0], xp[_0]);
        C_[_1][_0].umtv(rhs[_1], xp[_0]);
        C_[_2][_0].umtv(rhs[_2], xp[_0]);

        C_[_0][_1].mtv(rhs[_0], xp[_1]);
        C_[_1][_1].umtv(rhs[_1], xp[_1]);
        C_[_2][_1].umtv(rhs[_2], xp[_1]);

        return xp;
    }

    const SubMatrix& massMatrix() const
    { return mass_; }

private:
    SubMatrix mass_;
    Matrix23 I_;
    Matrix33 A_;
    Matrix32 C_;
};

} // end namespace Dumux

#endif
