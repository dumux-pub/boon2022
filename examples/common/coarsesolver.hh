// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_ONEP_COARSE_SOLVER_HH
#define DUMUX_MORTAR_ONEP_COARSE_SOLVER_HH

#include <utility>
#include <algorithm>
#include <unordered_map>
#include <vector>
#include <memory>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/matrixmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include "types.hh"
#include "projector.hh"

namespace Dumux {

// forward declaration
template<class SubDomainSolver>
bool isFloating(const SubDomainSolver& solver);

class FloatingDomainMap
{
public:
    template<class SubDomainSolver>
    FloatingDomainMap(const ContainerOfPointers<SubDomainSolver>& sdSolvers)
    {
        std::size_t floatingDomainIdx = 0;
        for (auto& sdSolverPtr : sdSolvers)
            if (Dumux::isFloating(*sdSolverPtr))
                domainToFloatingIdx_[sdSolverPtr->id()] = floatingDomainIdx++;
    }

    std::size_t getFloatingDomainIdx(std::size_t id) const
    {
        assert(this->isFloating(id));
        return domainToFloatingIdx_.at(id);
    }

    bool empty() const
    { return numFloatingDomains() == 0; }

    std::size_t numFloatingDomains() const
    { return domainToFloatingIdx_.size(); }

    bool isFloating(std::size_t id) const
    { return domainToFloatingIdx_.find(id) != domainToFloatingIdx_.end(); }

private:
    std::unordered_map<std::size_t, std::size_t> domainToFloatingIdx_;
};

template<class Scalar = double, class SubDomainSolver>
Scalar integrateSourceTerm(const SubDomainSolver& solver)
{
    const auto& gridGeometry = solver.gridGeometry();
    const auto& problem = solver.problem();

    Scalar source = 0.0;
    Scalar volume = 0.0;
    for (const auto& element : elements(gridGeometry.gridView()))
    {
        auto fvGeometry = localView(gridGeometry);
        auto elemVolVars = localView(solver.gridVariables().curGridVolVars());

        fvGeometry.bindElement(element);
        elemVolVars.bindElement(element, fvGeometry, solver.solution());

        for (const auto& scv : scvs(fvGeometry))
        {
            source += problem.source(element, fvGeometry, elemVolVars, scv)*scv.volume();
            volume += scv.volume();
        }
    }

    return source;
}

template<class Scalar = double, class StorageContainer, class SubDomainSolver>
void assembleCoarseSolveRHS(StorageContainer& storage,
                            const std::vector<std::shared_ptr<SubDomainSolver>>& sdSolverPtrs,
                            const FloatingDomainMap& floatingDomainMap)
{
    assert(storage.size() == floatingDomainMap.numFloatingDomains());
    std::for_each(sdSolverPtrs.begin(), sdSolverPtrs.end(),
        [&] (const auto sdSolverPtr) {
            if (floatingDomainMap.isFloating(sdSolverPtr->id()))
                storage[floatingDomainMap.getFloatingDomainIdx(sdSolverPtr->id())]
                    = integrateSourceTerm<Scalar>(*sdSolverPtr);
        }
    );
}

template<class SubDomainSolver,
         class Interface,
         class Scalar = typename SubDomainSolver::Scalar>
class CoarseProblemSolver
{
    using SDSolverPtr = std::shared_ptr<SubDomainSolver>;
    using InterfacePtr = std::shared_ptr<Interface>;

    using VectorBlockType = Dune::FieldVector<Scalar, 1>;
    using MatrixBlockType = Dune::FieldMatrix<Scalar, 1, 1>;
    using BlockVector = Dune::BlockVector<VectorBlockType>;
    using BlockMatrix = Dune::BCRSMatrix<MatrixBlockType>;
    using DenseMatrix = Dune::Matrix<MatrixBlockType>;

public:
    CoarseProblemSolver(const FloatingDomainMap idMap,
                        std::vector<SDSolverPtr> sdSolverPtrs,
                        std::vector<InterfacePtr> interfacePtrs)
    : floatingIdMap_(idMap)
    , solverPtrs_(sdSolverPtrs)
    , interfacePtrs_(interfacePtrs)
    {
        assembleMatrices_();
    }

    //! Return the matrix of the coarse system
    const BlockMatrix& B() const
    { return B_; }

    //! Return the matrix B*BTransposed
    const BlockMatrix& BBT() const
    { return BBT_; }

    //! Return the inverse of B*BTransposed
    const DenseMatrix& BBTInverse() const
    { return BBTInverse_; }

    //! Solve w.r.t to the given right-hand side
    void solve(BlockVector& result, BlockVector& rhs) const
    {
        result.resize(floatingIdMap_.numFloatingDomains());
        if (floatingIdMap_.numFloatingDomains() > 0)
            BBTInverse_.mv(rhs, result);
    }

private:
    void computeNumMortarDofsAndOffsets_()
    {
        interfaceDofOffsets_.resize(interfacePtrs_.size());
        numMortarDofs_ = std::accumulate(
            interfacePtrs_.begin(), interfacePtrs_.end(), std::size_t{0},
            [&] (std::size_t current, InterfacePtr ifPtr) {
                interfaceDofOffsets_[ifPtr->id()] = current;
                return current + ifPtr->gridGeometry().numDofs();
            }
        );
    }

    Dune::MatrixIndexSet getMatrixPatternB_() const
    {
        Dune::MatrixIndexSet pattern;
        pattern.resize(floatingIdMap_.numFloatingDomains(), numMortarDofs_);

        const auto addInterfaceEntries = [&] (std::size_t rowIdx, const Interface& interface)
        {
            const auto offset = interfaceDofOffsets_[interface.id()];
            for (std::size_t i = 0; i < interface.gridGeometry().numDofs(); ++i)
                pattern.add(rowIdx, offset + i);
        };

        for (auto ifPtr : interfacePtrs_)
        {
            const auto& posSolver = ifPtr->positiveNeighborSolver();
            if (floatingIdMap_.isFloating(posSolver.id()))
                addInterfaceEntries(floatingIdMap_.getFloatingDomainIdx(posSolver.id()), *ifPtr);

            const auto& negSolver = ifPtr->negativeNeighborSolver();
            if (floatingIdMap_.isFloating(negSolver.id()))
                addInterfaceEntries(floatingIdMap_.getFloatingDomainIdx(negSolver.id()), *ifPtr);
        }

        return pattern;
    }

    void assembleMatrices_()
    {
        if (floatingIdMap_.numFloatingDomains() == 0)
        {
            B_.setSize(0, 0);
            BBT_.setSize(0, 0);
            BBTInverse_.setSize(0, 0);
            return;
        }

        computeNumMortarDofsAndOffsets_();
        const auto pattern = getMatrixPatternB_();
        pattern.exportIdx(B_);
        B_ = 0.0;
        for (auto interfacePtr : interfacePtrs_)
            assembleInterfaceContributions_(*interfacePtr);
        constructBBT_();
        invertBBT_();
    }

    void assembleInterfaceContributions_(Interface& interface)
    {
        const auto n = interface.gridGeometry().numDofs();
        BlockVector unitMortar; unitMortar.resize(n); unitMortar = 0.0;
        BlockVector positiveProjection, negativeProjection;

        for (std::size_t i = 0; i < n; ++i)
        {
            unitMortar[i] = 1.0;

            ProjectionDetail::projectToSubDomain(
                interface.positiveProjector(),
                interface.negativeProjector(),
                unitMortar, positiveProjection, negativeProjection
            );

            negativeProjection *= -1.0;
            if (floatingIdMap_.isFloating(interface.positiveNeighborSolver().id()))
                assembleAndAddEntry_(interface.positiveNeighborSolver(),
                                     interface, positiveProjection, i);
            if (floatingIdMap_.isFloating(interface.negativeNeighborSolver().id()))
                assembleAndAddEntry_(interface.negativeNeighborSolver(),
                                     interface, negativeProjection, i);

            unitMortar[i] = 0.0;
        }
    }

    // TODO: substitute by scalar product!
    void assembleAndAddEntry_(const SubDomainSolver& sdSolver,
                              const Interface& interface,
                              const BlockVector& unitProjection,
                              const std::size_t dofIndexOnInterface)
    {
        Scalar fluxes = 0.0;
        for (const auto& element : elements(sdSolver.gridGeometry().gridView()))
        {
            auto fvGeometry = localView(sdSolver.gridGeometry());
            fvGeometry.bindElement(element);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto interfaceId = sdSolver.problem().mortarInterfaceIndex(element, scvf);
                if (interfaceId && *interfaceId == interface.id())
                    fluxes += scvf.area()*unitProjection[
                        fvGeometry.scv(scvf.insideScvIdx()).dofIndex()
                    ];
            }
        }

        const auto rowIdx = floatingIdMap_.getFloatingDomainIdx(sdSolver.id());
        const auto colIdx = interfaceDofOffsets_[interface.id()] + dofIndexOnInterface;
        B_[rowIdx][colIdx] = fluxes;
    }

    void constructBBT_()
    { Dune::matMultTransposeMat(BBT_, B_, B_); }

    void invertBBT_()
    {
        Dune::DynamicMatrix<Scalar> inverse(BBT_.N(), BBT_.M());
        inverse = 0.0;
        for (auto rowIt = BBT_.begin(); rowIt != BBT_.end(); ++rowIt)
            for (auto colIt = rowIt->begin(); colIt != rowIt->end(); ++colIt)
                inverse[rowIt.index()][colIt.index()] = *colIt;

        std::cout << "Inverting coarse problem matrix ..." << std::endl;
        inverse.invert();
        std::cout << " ... done." << std::endl;

        BBTInverse_.setSize(inverse.N(), inverse.M());
        for (auto rowIt = BBTInverse_.begin(); rowIt != BBTInverse_.end(); ++rowIt)
            for (auto colIt = rowIt->begin(); colIt != rowIt->end(); ++colIt)
                (*colIt) = inverse[rowIt.index()][colIt.index()];
    }

    FloatingDomainMap floatingIdMap_;

    std::vector<SDSolverPtr> solverPtrs_;
    std::vector<InterfacePtr> interfacePtrs_;

    std::vector<std::size_t> interfaceDofOffsets_;
    std::size_t numMortarDofs_;

    BlockMatrix B_;
    BlockMatrix BBT_;
    DenseMatrix BBTInverse_;
};

} // end namespace Dumux

#endif
