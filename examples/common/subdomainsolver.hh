// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief Darcy solver class to solve a stationary, single-phase problem.
 */
#ifndef DUMUX_MORTAR_SUBDOMAIN_SOLVER_HH
#define DUMUX_MORTAR_SUBDOMAIN_SOLVER_HH

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/porousmediumflow/velocityoutput.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <examples/common/floating_constraint/properties.hh>
#include <examples/common/floating_constraint/couplingmanager.hh>
#include <examples/common/floating_constraint/lagrangemultiplierevaluator.hh>

namespace Dumux {
namespace Detail {

template<class TypeTag, class LS, class P, class GG, class GV, class X>
void solveWithoutConstraint(std::shared_ptr<P> problem,
                            std::shared_ptr<GG> gridGeometry,
                            std::shared_ptr<GV> gridVariables,
                            X& solution)
{
    using Assembler = Dumux::FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);
    auto linearSolver = std::make_shared<LS>();

    Dumux::NewtonSolver<Assembler, LS> newton(assembler, linearSolver);
    newton.solve(solution);
}

template<class TypeTag, class LS, class P, class GG, class GV, class X>
void solveWithConstraint(std::shared_ptr<P> problem,
                         std::shared_ptr<GG> gridGeometry,
                         std::shared_ptr<GV> gridVariables,
                         X& solution)
{
    using ConstraintGrid = GetPropType<Properties::TTag::Constraint, Properties::Grid>;
    using ConstraintGG = GetPropType<Properties::TTag::Constraint, Properties::GridGeometry>;
    Dumux::GridManager<ConstraintGrid> gridManager;
    gridManager.init(Dune::FieldVector<double, 1>{1.0}, std::array<int, 1>{1});

    const auto& constraintGV = gridManager.grid().leafGridView();
    auto constraintGG = std::make_shared<ConstraintGG>(constraintGV);
    constraintGG->update();

    using ConstraintProblem = GetPropType<Properties::TTag::Constraint, Properties::Problem>;
    auto constraintProblem = std::make_shared<ConstraintProblem>(constraintGG, "Constraint");

    using ConstraintGridVariables = GetPropType<Properties::TTag::Constraint, Properties::GridVariables>;
    auto constraintGridVars = std::make_shared<ConstraintGridVariables>(constraintProblem, constraintGG);

    using MDTraits = Dumux::MultiDomainTraits<TypeTag, Properties::TTag::Constraint>;
    using CouplingManager = ConstraintCouplingManager<MDTraits>;
    auto couplingManager = std::make_shared<CouplingManager>(gridGeometry, constraintGG);
    constraintProblem->setAveragePressureEvaluator(couplingManager);

    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, true>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(problem, constraintProblem),
                                                 std::make_tuple(gridGeometry, constraintGG),
                                                 std::make_tuple(gridVariables, constraintGridVars),
                                                 couplingManager);
    auto linearSolver = std::make_shared<LS>();

    Dumux::MultiDomainNewtonSolver<Assembler, LS, CouplingManager> newton(assembler, linearSolver, couplingManager);

    using ConstraintSolution = Dune::BlockVector<Dune::FieldVector<double, 1>>;
    ConstraintSolution constraintSolution(1);
    constraintSolution = 0.0;
    Dune::MultiTypeBlockVector<X, ConstraintSolution> x;
    x[Dune::index_constant<0>()] = solution;
    x[Dune::index_constant<1>()] = constraintSolution;

    problem->setLagrangeMultiplierEvaluator(couplingManager);
    newton.solve(x);
    solution = x[Dune::index_constant<0>()];
    problem->unsetLagrangeMultiplierEvaluator();
}

} // end namespace Detail

template<class SubDomainSolver>
bool isFloating(const SubDomainSolver& solver)
{
    for (const auto& e : elements(solver.gridGeometry().gridView()))
    {
        auto fvGeometry = localView(solver.gridGeometry());
        fvGeometry.bindElement(e);
        for (const auto& scvf : scvfs(fvGeometry))
            if (scvf.boundary() && solver.problem().boundaryTypes(e, scvf).hasDirichlet())
                return false;
    }
    return true;
}

template<class TypeTag,
         class LinearSolver = ILU0BiCGSTABBackend,
         bool useDomainMarker = false>
class SubDomainSolver
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    using SolVec = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVars = GetPropType<TypeTag, Properties::GridVariables>;
    using OutputModule = VtkOutputModule<GridVars, SolVec>;

public:
    //! export some data types
    using SolutionVector = SolVec;
    using GridVariables = GridVars;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    SubDomainSolver(std::size_t id, const std::string& paramGroup)
    : id_(id)
    {
        init_(paramGroup);
    }

    void solve() {
        if (isFloating(*this))
            Detail::solveWithConstraint<TypeTag, LinearSolver>(
                problem_, fvGridGeometry_, gridVariables_, *x_
            );
        else
            Detail::solveWithoutConstraint<TypeTag, LinearSolver>(
                problem_, fvGridGeometry_, gridVariables_, *x_
            );
    }

    std::size_t id() const
    { return id_; }

    void write(Scalar t)
    { vtkWriter_->write(t); }

    OutputModule& outputModule()
    { return *vtkWriter_; }

    std::shared_ptr<const GridGeometry> gridGeometryPointer() const { return fvGridGeometry_; }
    const GridGeometry& gridGeometry() const { return *fvGridGeometry_; }

    std::shared_ptr<GridVariables> gridVariablesPointer() { return gridVariables_; }
    const GridVariables& gridVariables() const { return *gridVariables_; }

    std::shared_ptr<Problem> problemPointer() { return problem_; }
    const Problem& problem() const { return *problem_; }

    std::shared_ptr<SolutionVector> solutionPointer() { return x_; }
    const SolutionVector& solution() const { return *x_; }

    void setSolution(const SolutionVector& x)
    {
        *x_ = x;
        gridVariables_->update(*x_);
    }

private:
    void init_(const std::string& paramGroup)
    {
        // try to create a grid (from the given grid file or the input file)
        gridManager_.init(paramGroup);

        // we compute on the leaf grid view
        const auto& leafGridView = gridManager_.grid().leafGridView();

        // create the finite volume grid geometry
        fvGridGeometry_ = std::make_shared<GridGeometry>(leafGridView);
        fvGridGeometry_->update();

        // the problem (initial and boundary conditions)
        if constexpr (useDomainMarker) {
            auto gridData = gridManager_.getGridData();
            auto params = std::make_shared<SpatialParams>(fvGridGeometry_, paramGroup, gridData);
            problem_ = std::make_shared<Problem>(fvGridGeometry_, params, paramGroup);
        }
        else {
            auto params = std::make_shared<SpatialParams>(fvGridGeometry_, paramGroup);
            problem_ = std::make_shared<Problem>(fvGridGeometry_, params, paramGroup);
        }

        // resize and initialize the given solution vector
        x_ = std::make_shared<SolutionVector>();
        x_->resize(fvGridGeometry_->numDofs());
        problem_->applyInitialSolution(*x_);

        // the grid variables
        gridVariables_ = std::make_shared<GridVariables>(problem_, fvGridGeometry_);
        gridVariables_->init(*x_);

        // initialize the vtk output module
        using IOFields = GetPropType<TypeTag, Properties::IOFields>;
        using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
        using VelocityOutput = Dumux::PorousMediumFlowVelocityOutput<GridVariables, FluxVariables>;
        vtkWriter_ = std::make_unique<OutputModule>(*gridVariables_, *x_, problem_->name());
        IOFields::initOutputModule(*vtkWriter_);
        vtkWriter_->addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables_));
    }

    std::size_t id_;

    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager_;
    std::shared_ptr<GridGeometry> fvGridGeometry_;
    std::shared_ptr<Problem> problem_;
    std::shared_ptr<GridVariables> gridVariables_;
    std::unique_ptr<OutputModule> vtkWriter_;
    std::shared_ptr<SolutionVector> x_;
};

} // end namespace Dumux

#endif
