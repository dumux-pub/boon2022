// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \todo TODO: Doc me
 */
#ifndef DUMUX_ONEP_MORTAR_MPFA_DARCYS_LAW_HH
#define DUMUX_ONEP_MORTAR_MPFA_DARCYS_LAW_HH

#include <dune/common/dynvector.hh>
#include <dune/common/dynmatrix.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/discretization/method.hh>
#include <dumux/flux/ccmpfa/darcyslaw.hh>

namespace Dumux {

/*!
 * \ingroup CCMpfaFlux
 * \brief Darcy's law for cell-centered finite volume schemes
 *        with multi-point flux approximation.
 */
template<class TypeTag>
class MortarMpfaDarcysLaw
: public DarcysLawImplementation<TypeTag, DiscretizationMethod::ccmpfa>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ParentType = DarcysLawImplementation<TypeTag, DiscretizationMethod::ccmpfa>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using Problem = GetPropType<TypeTag, Properties::Problem>;

public:

    //! Compute the advective flux across an scvf
    template<class ElementVolumeVariables, class ElementFluxVariablesCache>
    static Scalar flux(const Problem& problem,
                       const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolumeFace& scvf,
                       const unsigned int phaseIdx,
                       const ElementFluxVariablesCache& elemFluxVarsCache)
    {
        const auto& fluxVarsCache = elemFluxVarsCache[scvf];

        // forward to the private function taking the iv data handle
        if (fluxVarsCache.usesSecondaryIv())
            return flux_(problem, fluxVarsCache, fluxVarsCache.advectionSecondaryDataHandle(), phaseIdx);
        else
            return flux_(problem, fluxVarsCache, fluxVarsCache.advectionPrimaryDataHandle(), phaseIdx);
    }

private:
    template< class Problem, class FluxVarsCache, class DataHandle >
    static Scalar flux_(const Problem& problem,
                        const FluxVarsCache& cache,
                        const DataHandle& dataHandle,
                        int phaseIdx)
    {
        dataHandle.setPhaseIndex(phaseIdx);

        const bool switchSign = cache.advectionSwitchFluxSign();

        const auto localFaceIdx = cache.ivLocalFaceIndex();
        const auto idxInOutside = cache.indexInOutsideFaces();
        const auto& pj = dataHandle.uj();
        const auto& tij = dim == dimWorld ? dataHandle.T()[localFaceIdx]
                                          : (!switchSign ? dataHandle.T()[localFaceIdx]
                                                         : dataHandle.tijOutside()[localFaceIdx][idxInOutside]);
        Scalar scvfFlux = tij*pj + dataHandle.neumannBCs()[localFaceIdx];

        // maybe add gravitational acceleration
        static const bool enableGravity = getParamFromGroup<bool>(problem.paramGroup(), "Problem.EnableGravity");
        if (enableGravity)
            scvfFlux += dim == dimWorld ? dataHandle.g()[localFaceIdx]
                                        : (!switchSign ? dataHandle.g()[localFaceIdx]
                                                       : dataHandle.gOutside()[localFaceIdx][idxInOutside]);

        // switch the sign if necessary
        if (switchSign)
            scvfFlux *= -1.0;

        return scvfFlux;
    }
};

} // end namespace

#endif
