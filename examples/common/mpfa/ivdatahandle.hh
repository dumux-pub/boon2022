// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \todo TODO: Doc me
 */
#ifndef DUMUX_ONEP_MORTAR_MPFA_IV_DATA_HANDLE_HH
#define DUMUX_ONEP_MORTAR_MPFA_IV_DATA_HANDLE_HH

#include <dumux/discretization/cellcentered/mpfa/interactionvolumedatahandle.hh>

namespace Dumux {

//! Data handle for quantities related to advection (adding Neumann BC  handle)
template<class MatVecTraits, class PhysicsTraits, bool EnableAdvection>
class OnePMortarAdvectionDataHandle
: public AdvectionDataHandle< MatVecTraits, PhysicsTraits, EnableAdvection >
{
    static_assert(PhysicsTraits::numPhases, "This expects a single phase");
    using UnknownVector = typename MatVecTraits::AMatrix::row_type;

public:

    //! The flux contributions of neumann boundary conditions
    const UnknownVector& neumannBCs() const { return neumannBCs_; }
    UnknownVector& neumannBCs() { return neumannBCs_; }

    //! The neumann boundary conditions vector for the iv-local eq-system
    const UnknownVector& N() const { return n_; }
    UnknownVector& N() { return n_; }

private:
    UnknownVector neumannBCs_; //!< The flux contributions of Neumann boundary conditions
    UnknownVector n_; //!< The Neumann boundary conditions
};

//! Process-dependent data handles when related process is disabled
template<class MatVecTraits, class PhysicsTraits>
class OnePMortarAdvectionDataHandle<MatVecTraits, PhysicsTraits, false> : public EmptyDataHandle {};

/*!
 * \ingroup CCMpfaDiscretization
 * \brief Class for the interaction volume data handle.
 *
 * \tparam MVT The matrix/vector traits collecting type information used by the iv
 * \tparam PT The physics traits collecting data on the physical processes to be considered
 */
template<class MVT, class PT>
class MortarIVDataHandle
{

public:
    //! export the underlying process-specific handle types
    using AdvectionHandle = OnePMortarAdvectionDataHandle<MVT, PT, PT::enableAdvection>;
    using DiffusionHandle = DiffusionDataHandle<MVT, PT, PT::enableMolecularDiffusion>;
    using HeatConductionHandle = HeatConductionDataHandle<MVT, PT, PT::enableHeatConduction>;

    //! return references to the handle containing data related to advection
    const AdvectionHandle& advectionHandle() const { return advectionHandle_; }
    AdvectionHandle& advectionHandle() { return advectionHandle_; }

    //! return references to the handle containing data related to diffusion
    const DiffusionHandle& diffusionHandle() const { return diffusionHandle_; }
    DiffusionHandle& diffusionHandle() { return diffusionHandle_; }

    //! return references to the handle containing data related to heat conduction
    const HeatConductionHandle& heatConductionHandle() const { return heatConductionHandle_; }
    HeatConductionHandle& heatConductionHandle() { return heatConductionHandle_; }

private:
    AdvectionHandle advectionHandle_;
    DiffusionHandle diffusionHandle_;
    HeatConductionHandle heatConductionHandle_;
};

} // end namespace Dumux

#endif
