// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_INTERFACE_HH
#define DUMUX_MORTAR_INTERFACE_HH

#include <iostream>
#include <iomanip>

#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dumux/discretization/fem/fegridgeometry.hh>
#include <dumux/multidomain/glue.hh>
#include "traceoperator.hh"
#include "projectorfactory.hh"
#include "projector.hh"
#include "types.hh"

#ifndef MORTARBASISORDER
#define MORTARBASISORDER 1
#endif

namespace Dumux {

template<int spaceDimension, int order = 1>
struct DefaultInterfaceTraits
{
    static constexpr int basisOrder = order;

    using Scalar = double;
    using Grid = Dune::FoamGrid<spaceDimension-1, spaceDimension>;
    using GridView = typename Grid::LeafGridView;
    using BlockType = Dune::FieldVector<Scalar, 1>;
    using SolutionVector = Dune::BlockVector<BlockType>;
    using FEBasis = Dune::Functions::LagrangeBasis<GridView, basisOrder>;
    using GridGeometry = Dumux::FEGridGeometry<FEBasis>;
    static constexpr Dune::VTK::DataMode VTKDataMode = Dune::VTK::conforming;
};

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
enum class InterfaceSide { positive, negative };

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class TheTraits, class SubDomainSolver, class Projector>
class MortarInterface
{
    using SDGridGeometry = typename SubDomainSolver::GridGeometry;
    using TraceOperator = InterfaceTraceOperator<SubDomainSolver>;

public:
    //! Export underlying traits class
    using Traits = TheTraits;
    //! Export grid geometry
    using GridGeometry = typename Traits::GridGeometry;
    //! Export the glue type containing grid intersections
    using Glue = MultiDomainGlue<typename SDGridGeometry::GridView,
                                 typename GridGeometry::GridView,
                                 typename SDGridGeometry::ElementMapper,
                                 typename GridGeometry::ElementMapper>;

    /*!
     * \brief TODO doc me.
     */
    MortarInterface(std::shared_ptr<GridGeometry> gg,
                    std::shared_ptr<SubDomainSolver> solver1,
                    std::shared_ptr<SubDomainSolver> solver2,
                    std::size_t interfaceId)
    : id_(interfaceId)
    , gridGeometry_(gg)
    , solver1_(solver1)
    , solver2_(solver2)
    , glue1_(makeGlue(*solver1->gridGeometryPointer(), *gg))
    , glue2_(makeGlue(*solver2->gridGeometryPointer(), *gg))
    {
        using ProjectorFactory = ProjectorFactory<Projector>;
        auto projectors = ProjectorFactory::makeProjectors(*solver1, *solver2, *gg, glue1_, glue2_);

        projector1_ = projectors.first;
        projector2_ = projectors.second;
        solver1_->problemPointer()->setInterfaceSide(interfaceId, InterfaceSide::positive);
        solver2_->problemPointer()->setInterfaceSide(interfaceId, InterfaceSide::negative);

        traceOperator1_ = std::make_shared<TraceOperator>(solver1_, *gg, glue1_);
        traceOperator2_ = std::make_shared<TraceOperator>(solver2_, *gg, glue2_);

        solver1_->problemPointer()->setCouplingScvfMap(interfaceId, traceOperator1_->coupledScvfMap());
        solver2_->problemPointer()->setCouplingScvfMap(interfaceId, traceOperator2_->coupledScvfMap());
    }

    /*!
     * \brief TODO doc me.
     */
    MortarInterface(std::shared_ptr<GridGeometry> gg,
                    std::shared_ptr<SubDomainSolver> solver1,
                    std::shared_ptr<SubDomainSolver> solver2,
                    const Glue& glue1,
                    const Glue& glue2,
                    std::size_t interfaceId)
    : id_(interfaceId)
    , gridGeometry_(gg)
    , solver1_(solver1)
    , solver2_(solver2)
    , glue1_(glue1)
    , glue2_(glue2)
    {
        using ProjectorFactory = ProjectorFactory<Projector>;
        auto projectors = ProjectorFactory::makeProjectors(*solver1, *solver2, *gg, glue1_, glue2_);

        projector1_ = projectors.first;
        projector2_ = projectors.second;

        solver1_->problemPointer()->setInterfaceSide(interfaceId, InterfaceSide::positive);
        solver2_->problemPointer()->setInterfaceSide(interfaceId, InterfaceSide::negative);

        traceOperator1_ = std::make_shared<TraceOperator>(solver1_, *gg, glue1);
        traceOperator2_ = std::make_shared<TraceOperator>(solver2_, *gg, glue2);

        solver1_->problemPointer()->setCouplingScvfMap(interfaceId, traceOperator1_->coupledScvfMap());
        solver2_->problemPointer()->setCouplingScvfMap(interfaceId, traceOperator2_->coupledScvfMap());
    }

   //! Return the grid geometry of this interface
   GridGeometry& gridGeometry() { return *gridGeometry_; }
   const GridGeometry& gridGeometry() const { return *gridGeometry_; }

   //! The identifier of this interface
   std::size_t id() const { return id_; }

   //! Return references to sub-domain solvers
   SubDomainSolver& positiveNeighborSolver() { return *solver1_; }
   SubDomainSolver& negativeNeighborSolver() { return *solver2_; }
   const SubDomainSolver& positiveNeighborSolver() const { return *solver1_; }
   const SubDomainSolver& negativeNeighborSolver() const { return *solver2_; }

   //! Return references to sub-domain projectors
   const Projector& positiveProjector() const { return *projector1_; }
   const Projector& negativeProjector() const { return *projector2_; }

   //! Return references to the sub-domain trace operators
   const TraceOperator& positiveTraceOperator() const { return *traceOperator1_; }
   const TraceOperator& negativeTraceOperator() const { return *traceOperator2_; }

   //! Return glue objects holgind grid intersections
   const Glue& glue1() const { return glue1_; }
   const Glue& glue2() const { return glue2_; }

private:
   std::size_t id_;                                //!< Identifier of this interface
   std::shared_ptr<GridGeometry> gridGeometry_;    //!< Grid geometry decribing this interface
   std::shared_ptr<SubDomainSolver> solver1_;      //!< "Positive" neighboring sub-domain solver
   std::shared_ptr<SubDomainSolver> solver2_;      //!< "Negative" neighboring sub-domain solver
   std::shared_ptr<Projector> projector1_;         //!< Projector from & to sub-domain 1
   std::shared_ptr<Projector> projector2_;         //!< Projector from & to sub-domain 2
   std::shared_ptr<TraceOperator> traceOperator1_; //!< Interface trace operator of sub-domain 1
   std::shared_ptr<TraceOperator> traceOperator2_; //!< Interface trace operator of sub-domain 2
   Glue glue1_;                                    //!< Glue object between grid of sub-domain 1 and interface
   Glue glue2_;                                    //!< Glue object between grid of sub-domain 2 and interface
};

} // end namespace Dumux

#endif
