// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief TODO doc me
 */
#ifndef DUMUX_MORTAR_INITIALIZATION_HH
#define DUMUX_MORTAR_INITIALIZATION_HH

#include <utility>

#include <dumux/common/parameters.hh>
#include <dumux/io/grid/gridmanager.hh>

#include "types.hh"
#include "subdomainsolver.hh"
#include "interface.hh"

namespace Dumux {

template<class SubDomainSolver>
auto makeSubDomainSolvers()
{
    // create sub-domain solvers
    std::size_t solverIdx = 0;
    ContainerOfPointers<SubDomainSolver> solvers;
    while (hasParamInGroup("SubDomain" + std::to_string(solverIdx), "Grid.File")
           || hasParamInGroup("SubDomain" + std::to_string(solverIdx), "Grid.Cells"))
    {
        std::cout << "Initialing subdomain solver " << solverIdx << std::endl;
        solvers.emplace_back(makePointer<SubDomainSolver>(
            solverIdx,
            "SubDomain" + std::to_string(solverIdx)
        ));
        solverIdx++;
        std::cout << "Finished initialization" << std::endl;
    }
    return solvers;
}

template<class InterfaceTraits>
auto makeInterfaceGridGeometries()
{
    // create interface grid geometries
    using InterfaceGrid = typename InterfaceTraits::Grid;
    using InterfaceGridGeometry = typename InterfaceTraits::GridGeometry;

    std::size_t interfaceIdx = 0;
    static Container<GridManager<InterfaceGrid>> ifGridManagers;
    ContainerOfPointers<InterfaceGridGeometry> ifGridGeometries;
    while (hasParamInGroup("Interface" + std::to_string(interfaceIdx), "Grid.File")
           || hasParamInGroup("Interface" + std::to_string(interfaceIdx), "Grid.Cells"))
    {
        std::cout << "Initialing interface " << interfaceIdx << std::endl;
        ifGridManagers.emplace_back();
        ifGridManagers.back().init("Interface" + std::to_string(interfaceIdx));

        const auto& gridView = ifGridManagers.back().grid().leafGridView();
        auto feBasis = std::make_shared<typename InterfaceTraits::FEBasis>(gridView);
        ifGridGeometries.emplace_back( std::make_shared<InterfaceGridGeometry>(feBasis) );
        interfaceIdx++;

        std::cout << "Finished initialization" << std::endl;
    }

    return ifGridGeometries;
}

/*!
 * \brief TODO doc me.
 */
template< class InterfaceTraits, class Projector, class SubDomainSolver >
std::vector< Pointer<Dumux::MortarInterface<InterfaceTraits, SubDomainSolver, Projector>> >
makeInterfaces(const ContainerOfPointers<SubDomainSolver>& subDomainSolvers,
               const ContainerOfPointers<typename InterfaceTraits::GridGeometry>& interfaceGridGeometries)
{
    using InterfaceType = MortarInterface<InterfaceTraits, SubDomainSolver, Projector>;
    using SDGridGeometry = typename SubDomainSolver::GridGeometry;
    using IFGridGeometry = typename InterfaceTraits::GridGeometry;
    using Glue = Dumux::MultiDomainGlue< typename SDGridGeometry::GridView, typename IFGridGeometry::GridView,
                                         typename SDGridGeometry::ElementMapper, typename IFGridGeometry::ElementMapper>;

    Dune::Timer watch;
    std::cout << "Setting up the interfaces...\n";

    std::size_t ifId = 0;
    std::vector<std::shared_ptr<InterfaceType>> interfaces;
    interfaces.reserve(interfaceGridGeometries.size());
    for (auto ifGGPtr : interfaceGridGeometries)
    {
        std::cout << "Interface" << ifId << "..." << std::endl;
        int neighborCount = 0;
        std::array<Glue, 2> glues;
        std::array<std::size_t, 2> neighborIds;

        std::size_t solverId = 0;
        for (auto solver : subDomainSolvers)
        {
            auto glue = Dumux::makeGlue(*solver->gridGeometryPointer(), *ifGGPtr);
            if (glue.size() > 0)
            {
                if (neighborCount >= 2)
                    DUNE_THROW(Dune::InvalidStateException, "Each interface is expected to be between only two subdomains");

                glues[neighborCount] = std::move(glue);
                neighborIds[neighborCount] = solverId;
                neighborCount++;

                static const bool checkNeighbors = Dumux::getParam<bool>("InterfaceSolver.CheckIntersectionNeighbors", false);
                static const bool checkAreas = Dumux::getParam<bool>("InterfaceSolver.CheckInterfaceAreas", false);

                if (checkNeighbors)
                {
                    for (const auto& is : intersections(glues[neighborCount-1]))
                        if (is.numTargetNeighbors() != 1)
                            DUNE_THROW(Dune::InvalidStateException, "Intersection does not have a single target neighbor");
                }

                if (checkAreas)
                {
                    // interface grid area
                    double ifArea = 0.0;
                    for (const auto& e : elements(ifGGPtr->gridView()))
                        ifArea += e.geometry().volume();

                    // intersection grid area
                    double isArea = 0.0;
                    for (const auto& is : intersections(glues[neighborCount-1]))
                        isArea += is.geometry().volume();

                    using std::abs;
                    const auto origPrecCout = std::cout.precision();
                    std::cout << "\nInfo on interface " << ifId << " with solver " << solverId << ":\n";
                    std::cout << "\t- computed interface grid area:    " << std::setprecision(30) << ifArea << std::endl;
                    std::cout << "\t- computed intersection grid area: " << std::setprecision(30) << isArea << std::endl;
                    std::cout << "\t- computed grid area mismatch:     " << std::setprecision(30) << abs(isArea-ifArea) << std::endl;
                    std::cout.precision(origPrecCout);
                }
            }

            solverId++;
        }

        if (neighborCount != 2)
        {
            std::cout << "Interface " << ifId << " has " << neighborCount << " neighbors\n";
            DUNE_THROW(Dune::InvalidStateException, "Each interface is expected to have two neighbors");
        }

        interfaces.emplace_back(makePointer<InterfaceType>(ifGGPtr,
                                                           subDomainSolvers[neighborIds[0]],
                                                           subDomainSolvers[neighborIds[1]],
                                                           glues[0], glues[1], ifId));

        ifId++;
    }

    std::cout << " ... done. Took " << watch.elapsed() << " seconds." << std::endl;

    return interfaces;
}

template<class Projector, class InterfaceTraits, class SubDomainSolver>
auto makeSubDomainSolversAndInterfaces()
{
    auto solvers = makeSubDomainSolvers<SubDomainSolver>();
    auto ifGridGeoms = makeInterfaceGridGeometries<InterfaceTraits>();
    auto interfaces = makeInterfaces<InterfaceTraits, Projector>(solvers, ifGridGeoms);
    return std::make_pair(solvers, interfaces);
}

}  // end namespace Dumux

#endif
