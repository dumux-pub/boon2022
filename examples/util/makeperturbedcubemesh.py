import sys


# compute the center of a set of points
def center(*args):
    c = [0.0 for i in range(len(args[0]))]
    for arg in args:
        for i in range(len(arg)):
            c[i] += arg[i]
    for i in range(len(c)):
        c[i] /= len(args)
    return c


# get the coordinates of a randomly perturbed cube mesh
def getPerturbedCubeMeshCoords(extrema, cellSize):
    diag = [extrema[1][i] - extrema[0][i] for i in range(0, 3)]
    if any(diag[i] <= 0.0 for i in range(0, 3)):
        sys.exit("Corners must be given in ascending order.")

    dx = min(cellSize[0], diag[0])
    dy = min(cellSize[1], diag[1])
    dz = min(cellSize[2], diag[2])
    numCellX = max(1, int(diag[0]/dx))
    numCellY = max(1, int(diag[1]/dy))
    numCellZ = max(1, int(diag[2]/dz))

    # compute a coordinate shift
    def deltaCoordinate(dcoord):
        import random
        return random.uniform(-dcoord/4.0, dcoord/4.0)

    coords = []
    for k in range(0, numCellZ+1):
        coords.append([])
        for j in range(0, numCellY+1):
            coords[-1].append([])
            for i in range(0, numCellX+1):
                x = extrema[0][0] + i*dx
                y = extrema[0][1] + j*dy
                z = extrema[0][2] + k*dz

                x = x + deltaCoordinate(dx) if i > 0 else extrema[0][0]
                y = y + deltaCoordinate(dy) if j > 0 else extrema[0][1]
                z = z + deltaCoordinate(dz) if k > 0 else extrema[0][2]

                x = extrema[1][0] if i >= numCellX else x
                y = extrema[1][1] if j >= numCellY else y
                z = extrema[1][2] if k >= numCellZ else z
                coords[-1][-1].append([x, y, z])
    return coords


# refine a perturbed cube mesh and return the new vertex coordinates
def refinePerturbedCubeMeshCoords(coords):
    coordsNew = []
    for zIdx, xyLayer in enumerate(coords):
        coordsNew.append([])
        for yIdx, row in enumerate(xyLayer):
            coordsNew[-1].append([])
            for xIdx, col in enumerate(row):
                coordsNew[-1][-1].append(col)
                if xIdx < len(row) - 1:
                    coordsNew[-1][-1].append(center(col, row[xIdx+1]))

            # add additional row
            if yIdx < len(xyLayer) - 1:
                coordsNew[-1].append([])
                for xIdx, col in enumerate(row):
                    coordsNew[-1][-1].append(
                        center(coords[zIdx][yIdx][xIdx],
                               coords[zIdx][yIdx+1][xIdx]))
                    if xIdx < len(row) - 1:
                        coordsNew[-1][-1].append(
                            center(col, row[xIdx+1],
                                   coords[zIdx][yIdx+1][xIdx],
                                   coords[zIdx][yIdx+1][xIdx+1]))

        # add additional x-y layer
        if zIdx < len(coords) - 1:
            coordsNew.append([])
            for yIdx, row in enumerate(xyLayer):
                coordsNew[-1].append([])
                for xIdx, col in enumerate(row):
                    coordsNew[-1][-1].append(
                        center(coords[zIdx][yIdx][xIdx],
                               coords[zIdx+1][yIdx][xIdx]))

                    if xIdx < len(row) - 1:
                        coordsNew[-1][-1].append(
                            center(coords[zIdx][yIdx][xIdx],
                                   coords[zIdx][yIdx][xIdx+1],
                                   coords[zIdx+1][yIdx][xIdx+1],
                                   coords[zIdx+1][yIdx][xIdx]))

                if yIdx < len(xyLayer) - 1:
                    coordsNew[-1].append([])
                    for xIdx, col in enumerate(row):
                        coordsNew[-1][-1].append(
                            center(coords[zIdx][yIdx][xIdx],
                                   coords[zIdx][yIdx+1][xIdx],
                                   coords[zIdx+1][yIdx+1][xIdx],
                                   coords[zIdx+1][yIdx][xIdx]))

                        if xIdx < len(row) - 1:
                            coordsNew[-1][-1].append(
                                center(coords[zIdx][yIdx][xIdx],
                                       coords[zIdx][yIdx][xIdx+1],
                                       coords[zIdx][yIdx+1][xIdx+1],
                                       coords[zIdx][yIdx+1][xIdx],
                                       coords[zIdx+1][yIdx][xIdx],
                                       coords[zIdx+1][yIdx][xIdx+1],
                                       coords[zIdx+1][yIdx+1][xIdx+1],
                                       coords[zIdx+1][yIdx+1][xIdx]))
    return coordsNew


# write the coordinates of a perturbed cube mesh to a .msh file
def writePerturbedCubeMesh(mshFileName, coords):
    numCellZ = len(coords) - 1
    numCellY = len(coords[0]) - 1
    numCellX = len(coords[0][0]) - 1

    with open(mshFileName, 'w') as mshFile:

        mshFile.write('$MeshFormat\n')
        mshFile.write('2.2 0 8\n')
        mshFile.write('$EndMeshFormat\n')
        mshFile.write('$Nodes\n')

        numCells = (numCellX)*(numCellY)*(numCellZ)
        numNodes = (numCellX+1)*(numCellY+1)*(numCellZ+1)
        mshFile.write('{:d}\n'.format(numNodes))

        count = 1
        for k in range(0, numCellZ+1):
            for j in range(0, numCellY+1):
                for i in range(0, numCellX+1):
                    x, y, z = coords[k][j][i]
                    mshFile.write('{:d} {:.30f} {:.30f} {:.30f}\n'
                                  .format(count, x, y, z))
                    count += 1

        mshFile.write('$EndNodes\n')
        mshFile.write('$Elements\n')
        mshFile.write('{}\n'.format(numCells))

        count = 1
        for k in range(0, numCellZ):
            for j in range(0, numCellY):
                for i in range(0, numCellX):
                    gmshStartIdx = 1
                    p0 = gmshStartIdx
                    p0 += k*(numCellX+1)*(numCellY+1)
                    p0 += j*(numCellX+1)
                    p0 += i

                    p1 = p0 + 1
                    p2 = p1 + numCellX + 1
                    p3 = p2 - 1
                    p4 = p0 + (numCellX+1)*(numCellY+1)
                    p5 = p1 + (numCellX+1)*(numCellY+1)
                    p6 = p2 + (numCellX+1)*(numCellY+1)
                    p7 = p3 + (numCellX+1)*(numCellY+1)

                    mshFile.write('{} 5 0 {} {} {} {} {} {} {} {}\n'
                                  .format(count,
                                          p0, p1, p2, p3,
                                          p4, p5, p6, p7))
                    count += 1
        mshFile.write('$EndElements\n')


# get the coordinates of a perturbed structured mortar mesh
def getPerturbedMortarMeshCoords(extrema, initCellSize):

    diag = [extrema[2][i] - extrema[0][i] for i in range(0, 3)]
    deltax = diag[0]
    deltay = diag[1]
    deltaz = diag[2]

    minx = min([extrema[i][0] for i in range(0, 4)])
    miny = min([extrema[i][1] for i in range(0, 4)])
    minz = min([extrema[i][2] for i in range(0, 4)])
    maxx = max([extrema[i][0] for i in range(0, 4)])
    maxy = max([extrema[i][1] for i in range(0, 4)])
    maxz = max([extrema[i][2] for i in range(0, 4)])

    if (maxx - minx != deltax):
        sys.exit("deltax wrong")
    if (maxy - miny != deltay):
        sys.exit("deltay wrong")
    if (maxz - minz != deltaz):
        sys.exit("deltaz wrong")

    if (maxx - minx != 1.0 and maxx - minx != 0.0):
        sys.exit("deltax wrong")
    if (maxy - miny != 1.0 and maxy - miny != 0.0):
        sys.exit("deltay wrong")
    if (maxz - minz != 1.0 and maxz - minz != 0.0):
        sys.exit("deltaz wrong")

    def getXYCoords(xmin, xmax, dx,
                    ymin, ymax, dy):

        dx = min(dx, xmax - xmin)
        dy = min(dy, ymax - ymin)
        numCellX = max(1, int((xmax-xmin)/dx))
        numCellY = max(1, int((ymax-ymin)/dy))

        def deltaCoordinate(dcoord):
            import random
            return random.uniform(-dcoord/4.0, dcoord/4.0)

        coords = []
        for j in range(0, numCellY+1):
            coords.append([])
            for i in range(0, numCellX+1):
                x = xmin + i*dx
                y = ymin + j*dy

                x = x + deltaCoordinate(dx) if i > 0 else xmin
                y = y + deltaCoordinate(dy) if j > 0 else ymin

                x = xmax if i >= numCellX else x
                y = ymax if j >= numCellY else y

                coords[-1].append([x, y])
        return coords

    if abs(deltaz) < 1e-4:
        coords = getXYCoords(minx, maxx, initCellSize[0],
                             miny, maxy, initCellSize[1])
        coords = [[[x, y, minz] for x, y in rowCoords] for rowCoords in coords]
    elif abs(deltay) < 1e-4:
        coords = getXYCoords(minx, maxx, initCellSize[0],
                             minz, maxz, initCellSize[1])
        coords = [[[x, miny, z] for x, z in rowCoords] for rowCoords in coords]
    elif abs(deltax) < 1e-4:
        coords = getXYCoords(miny, maxy, initCellSize[0],
                             minz, maxz, initCellSize[1])
        coords = [[[minx, y, z] for y, z in rowCoords] for rowCoords in coords]
    else:
        sys.exit("Something went wrong upon coordinate construction!")

    return coords


# refine the coordinates of a perturbed structured mortar mesh
def refinePerturbedMortarMeshCoords(coords):

    coordsNew = []
    numRows = len(coords)
    for rowIdx, row in enumerate(coords):
        numCols = len(row)

        # add rows with new mid points
        coordsNew.append([])
        for colIdx, col in enumerate(row):
            coordsNew[-1].append(col)
            if colIdx < numCols - 1:
                coordsNew[-1].append(center(col, row[colIdx+1]))

        # add new row
        if rowIdx < numRows - 1:
            coordsNew.append([])
            for colIdx, col in enumerate(row):
                coordsNew[-1].append(center(coords[rowIdx][colIdx],
                                            coords[rowIdx+1][colIdx]))
                if colIdx < numCols - 1:
                    coordsNew[-1].append(center(col, row[colIdx+1],
                                                coords[rowIdx+1][colIdx],
                                                coords[rowIdx+1][colIdx+1]))

    return coordsNew


# write the coordinates of a perturbed mortar mesh into gmsh format
def writePerturbedMortarMesh(mshFileName, coords):
    with open(mshFileName, 'w') as mshFile:

        mshFile.write('$MeshFormat\n')
        mshFile.write('2.2 0 8\n')
        mshFile.write('$EndMeshFormat\n')
        mshFile.write('$Nodes\n')

        numCellX = len(coords[0]) - 1
        numCellY = len(coords) - 1
        numCells = numCellX*numCellY
        numNodes = (numCellX+1)*(numCellY+1)
        mshFile.write('{:d}\n'.format(numNodes))

        count = 1
        for row in coords:
            for x, y, z in row:
                mshFile.write('{:d} {:.30f} {:.30f} {:.30f}\n'
                              .format(count, x, y, z))
                count += 1

        mshFile.write('$EndNodes\n')
        mshFile.write('$Elements\n')
        mshFile.write('{}\n'.format(numCells))

        count = 1
        for j in range(0, numCellY):
            for i in range(0, numCellX):
                gmshStartIdx = 1
                p0 = gmshStartIdx + j*(numCellX+1) + i
                p1 = p0 + 1
                p2 = p1 + numCellX + 1
                p3 = p2 - 1
                mshFile.write('{} 3 0 {} {} {} {}\n'
                              .format(count, p0, p1, p2, p3))
                count += 1
        mshFile.write('$EndElements\n')
