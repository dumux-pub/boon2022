class DomainDecomposition:
    def __init__(self,
                 lowerLeft: list,
                 upperRight: list,
                 numDomainsPerAxis: list) -> None:
        self._dim = len(lowerLeft)
        assert self._dim == len(upperRight)
        assert self._dim == len(numDomainsPerAxis)
        assert self._dim == 2 or self._dim == 3
        self._cornerCoords = [
            [
                lowerLeft[dir]
                + i*(upperRight[dir] - lowerLeft[dir])/numDomainsPerAxis[dir]
                for i in range(numDomainsPerAxis[dir] + 1)
            ] for dir in range(self._dim)
        ]

    @property
    def numSubDomains(self) -> int:
        result = 1
        for dir in range(self._dim):
            result *= self._numSubDomainsInDirection(dir)
        return result

    @property
    def numInterfaces(self) -> int:
        ifaces0 = self._numInterfacesInDirection(0)
        ifaces1 = self._numInterfacesInDirection(1)
        if self._dim == 2:
            return ifaces0*(ifaces1 + 1) + ifaces1*(ifaces0 + 1)
        else:
            ifaces2 = self._numInterfacesInDirection(2)
            return (
                (ifaces0*(ifaces1 + 1) + ifaces1*(ifaces0 + 1))*(ifaces2 + 1)
                + ifaces2*(ifaces1 + 1)*(ifaces0 + 1)
            )

    def getSubDomainCorners(self, idx: int) -> list:
        assert idx < self.numSubDomains
        if self._dim == 2:
            yIdx = int(idx/self._numSubDomainsInDirection(0))
            xIdx = int(idx%self._numSubDomainsInDirection(0))
            x0 = self._cornerCoords[0][xIdx]
            x1 = self._cornerCoords[0][xIdx + 1]
            y0 = self._cornerCoords[1][yIdx]
            y1 = self._cornerCoords[1][yIdx + 1]
            return [[x0, y0], [x1, y0], [x0, y1], [x1, y1]]
        if self._dim == 3:
            sdPerPlane = self._numSubDomainsInDirection(0)*self._numSubDomainsInDirection(1)
            zIdx = int(idx/sdPerPlane)
            yIdx = int(int(idx%sdPerPlane)/self._numSubDomainsInDirection(0))
            xIdx = int(int(idx%sdPerPlane)%self._numSubDomainsInDirection(0))
            x0 = self._cornerCoords[0][xIdx]
            x1 = self._cornerCoords[0][xIdx + 1]
            y0 = self._cornerCoords[1][yIdx]
            y1 = self._cornerCoords[1][yIdx + 1]
            z0 = self._cornerCoords[2][zIdx]
            z1 = self._cornerCoords[2][zIdx + 1]
            return [
                [x0, y0, z0], [x1, y0, z0], [x0, y1, z0], [x1, y1, z0],
                [x0, y0, z1], [x1, y0, z1], [x0, y1, z1], [x1, y1, z1]
            ]

    def getInterfaceCorners(self, idx: int) -> list:
        assert idx < self.numInterfaces
        if self._dim == 2:
            return self._makeXYInterface(idx)
        else:
            numHorizontalInterfaceLayers = self._numInterfacesInDirection(2)
            numHorizontalInterfacesPerLayer = self._numSubDomainsInDirection(0)*self._numSubDomainsInDirection(1)

            ifaces0 = self._numInterfacesInDirection(0)
            ifaces1 = self._numInterfacesInDirection(1)
            numVerticalInterfaceLayers = numHorizontalInterfaceLayers + 1
            numVerticalInterfacesPerLayer = ifaces0*(ifaces1 + 1) + ifaces1*(ifaces0 + 1)
            numVerticalInterfaces = numVerticalInterfaceLayers*numVerticalInterfacesPerLayer
            isVerticalInterface = idx < numVerticalInterfaces
            if isVerticalInterface:
                layerIdx = int(idx/numVerticalInterfacesPerLayer)
                idxInLayer = int(idx%numVerticalInterfacesPerLayer)
                zCoordLow = self._cornerCoords[2][layerIdx]
                zCoordHigh = self._cornerCoords[2][layerIdx+1]

                cornerCoords = self._makeXYInterface(idxInLayer)
                cornerCoords = [c + [zCoordLow] for c in cornerCoords]
                cornerCoords.extend([
                    [c[0], c[1], zCoordHigh] for c in cornerCoords
                ])
                return cornerCoords

            idx -= numVerticalInterfaces  # turn into index within horizontal interfaces
            layerIdx = int(idx/numHorizontalInterfacesPerLayer)
            idxInLayer = int(idx%numHorizontalInterfacesPerLayer)
            rowIdx = int(idxInLayer/self._numSubDomainsInDirection(0))
            colIdx = int(idxInLayer%self._numSubDomainsInDirection(0))
            x0 = self._cornerCoords[0][colIdx]
            x1 = self._cornerCoords[0][colIdx+1]
            y0 = self._cornerCoords[1][rowIdx]
            y1 = self._cornerCoords[1][rowIdx+1]
            z = self._cornerCoords[2][layerIdx+1]
            return [[x0, y0, z], [x1, y0, z], [x0, y1, z], [x1, y1, z]]

    def _makeXYInterface(self, idx: int) -> list:
        numRows = self._numInterfacesInDirection(1) + 1
        numInterfacesXPerRow = self._numInterfacesInDirection(0)
        numInterfacesX = numInterfacesXPerRow*numRows
        isXInterface = idx < numInterfacesX
        if isXInterface:
            rowIdx = int(idx/numInterfacesXPerRow)
            colIdx = int(idx%numInterfacesXPerRow)
            return [
                [self._cornerCoords[0][colIdx+1], self._cornerCoords[1][rowIdx]],
                [self._cornerCoords[0][colIdx+1], self._cornerCoords[1][rowIdx+1]]
            ]
        idx -= numInterfacesX  # turn index into index within y-interfaces
        numInterfacesYPerRow = self._numInterfacesInDirection(0) + 1
        rowIdx = int(idx/numInterfacesYPerRow)
        colIdx = int(idx%numInterfacesYPerRow)
        return [
            [self._cornerCoords[0][colIdx], self._cornerCoords[1][rowIdx+1]],
            [self._cornerCoords[0][colIdx+1], self._cornerCoords[1][rowIdx+1]],
        ]

    def _numSubDomainsInDirection(self, dir: int) -> int:
        return len(self._cornerCoords[dir]) - 1

    def _numInterfacesInDirection(self, dir: int) -> int:
        return max(self._numSubDomainsInDirection(dir) - 1, 0)
