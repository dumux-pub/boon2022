import subprocess
import random
from math import sqrt
from os import remove
from os.path import splitext
from enum import Enum, auto
from typing import Optional

from makeperturbedcubemesh import (
    getPerturbedCubeMeshCoords,
    writePerturbedCubeMesh,
    refinePerturbedCubeMeshCoords
)

randomInstance = random.Random(1e8)

class CellType(Enum):
    segment = auto()
    triangle = auto()
    rectangle = auto()
    quadrilateral = auto()
    recombinedTriangle = auto()
    tetrahedron = auto()
    hexahedron = auto()
    cube = auto()
    default = auto()

def makeMesh(geomCorners: list,
             filename: str,
             dx: float,
             cellType: CellType = CellType.default,
             numRefinements: Optional[int] = None) -> None:
    geoFileName = "{}.geo".format(splitext(filename)[0])
    if len(geomCorners) == 2:
        _makeSegment(geomCorners, geoFileName, dx, cellType, numRefinements)
    elif len(geomCorners) == 4:
        _makeQuad(geomCorners, geoFileName, dx, cellType, numRefinements)
    elif len(geomCorners) == 8:
        _makeHex(geomCorners, geoFileName, dx, cellType, numRefinements)
    else:
        raise ValueError("Unknown geometry type")

def refineMesh(meshFile: str, targetMeshFile: str) -> None:
    _run(["gmsh", "-format", "msh2", "-refine", meshFile, "-o", targetMeshFile])

def _change_extension(inFileName: str, newExt: str) -> str:
    return splitext(inFileName)[0] + newExt

def _add_msh_extension(inFileName: str) -> str:
    return _change_extension(inFileName, ".msh")

def _add_refinement_index(inFileName: str, refIdx: int) -> str:
    _baseName = splitext(inFileName)[0]
    _ext = splitext(inFileName)[1]
    return f"{_baseName}_ref_{refIdx}{_ext}"

def _add_mesh_save_commands(geoFile, filename, numRefinements, meshDim) -> None:
    if numRefinements is None:
        geoFile.write("Mesh.MshFileVersion = 2.0;\n")
        geoFile.write(f"Mesh {meshDim};\n")
        geoFile.write(f"Save \"{_add_msh_extension(filename)}\";\n")
    else:
        geoFile.write(f"Mesh {meshDim};\n")
        geoFile.write(f"Save \"{_add_msh_extension(_add_refinement_index(filename, 0))}\";\n")
        for refIdx in range(1, numRefinements+1):
            geoFile.write("RefineMesh;\n")
            geoFile.write(f"Save \"{_add_msh_extension(_add_refinement_index(filename, refIdx))}\";\n")

def _run_gmsh(geoFileName):
    subprocess.run([
        "gmsh", "-tol", "1e-16", "-match", "-format", "msh2", "-parse_and_exit", geoFileName
    ], check=True)

def _makeSegment(corners, filename, dx, cellType, numRefinements) -> None:
    if cellType == CellType.default:
        cellType = CellType.segment
    assert len(corners) == 2
    assert len(corners[0]) == len(corners[1])
    assert cellType == CellType.segment

    dim = len(corners[0])
    a = corners[0]
    b = corners[1]
    if dim < 3:
        a.extend([0]*(3-dim))
        b.extend([0]*(3-dim))
    d = [b[i] - a[i] for i in range(len(a))]
    layers = int(round(_norm(d)/dx))
    with open(filename, "w") as geoFile:
        geoFile.write(f"Point(1) = {{{a[0]}, {a[1]}, {a[2]}}};\n")
        geoFile.write(f"Extrude{{{d[0]}, {d[1]}, {d[2]}}}")
        geoFile.write(f"{{Point{{1}}; Layers{{{layers}}};}}\n")
        geoFile.write("Physical Line(1) = {Line{:}};\n")
        _add_mesh_save_commands(geoFile, filename, numRefinements, 1)
    _run_gmsh(filename)


def _makeQuad(corners, filename, dx, cellType, numRefinements) -> None:
    if cellType == CellType.default:
        cellType = CellType.quadrilateral
    assert len(corners) == 4
    assert (
        cellType == CellType.quadrilateral
        or cellType == CellType.rectangle
        or cellType == CellType.triangle
        or cellType == CellType.recombinedTriangle
    )

    dim = len(corners[0])
    assert all(len(c) == dim for c in corners)

    a = corners[0] if dim == 3 else corners[0] + [0]*(3-dim)
    b = corners[1] if dim == 3 else corners[1] + [0]*(3-dim)
    c = corners[3] if dim == 3 else corners[3] + [0]*(3-dim)
    d = corners[2] if dim == 3 else corners[2] + [0]*(3-dim)

    with open(filename, "w") as geoFile:
        if cellType == CellType.rectangle:
            d1 = [b[i] - a[i] for i in range(3)]
            d2 = [d[i] - a[i] for i in range(3)]
            layers1 = int(round(_norm(d1)/dx))
            layers2 = int(round(_norm(d2)/dx))
            geoFile.write(f"Point(1) = {{{a[0]}, {a[1]}, {a[2]}}};\n")
            geoFile.write(f"out[] = Extrude{{{d1[0]}, {d1[1]}, {d1[2]}}}")
            geoFile.write(f"{{Point{{1}}; Layers{{{layers1}}};}};\n")
            geoFile.write(f"resultLine = out[1];\n")
            geoFile.write(f"Extrude{{{d2[0]}, {d2[1]}, {d2[2]}}}")
            geoFile.write(f"{{Line{{resultLine}}; Layers{{{layers2}}}; Recombine;}}\n")
            geoFile.write("Physical Surface(1) = {Surface{:}};")
        elif cellType == CellType.triangle or cellType == CellType.recombinedTriangle:
            for i, corner in zip(range(1, 5), [a, b, c, d]):
                geoFile.write(f"Point({i}) = {{{corner[0]}, {corner[1]}, {corner[2]}}};\n")
            geoFile.write("Line(1) = {1, 2};\n")
            geoFile.write("Line(2) = {2, 3};\n")
            geoFile.write("Line(3) = {3, 4};\n")
            geoFile.write("Line(4) = {4, 1};\n")
            geoFile.write("Line Loop(1) = {1, 2, 3, 4};\n")
            geoFile.write("Plane Surface(1) = {1};\n")
            geoFile.write("Physical Surface(1) = {1};\n")
            geoFile.write(f"Characteristic Length{{:}} = {dx};\n")
            if cellType == CellType.recombinedTriangle:
                geoFile.write("Recombine Surface{1};\n")
        else:
            distortion = 0.15
            e1 = [b[i] - a[i] for i in range(3)]
            e2 = [d[i] - a[i] for i in range(3)]
            layers1 = int(round(_norm(e1)/dx))
            layers2 = int(round(_norm(e2)/dx))
            _dx = [e1[_dir]/float(layers1) for _dir in range(3)]
            _dy = [e2[_dir]/float(layers2) for _dir in range(3)]
            for i in range(layers2+1):
                for j in range(layers1+1):
                    doDistortionX = j > 0 and j < layers1
                    doDistortionY = i > 0 and i < layers2
                    actualDistortionX = randomInstance.uniform(-distortion, distortion) if doDistortionX else 0.0
                    actualDistortionY = randomInstance.uniform(-distortion, distortion) if doDistortionY else 0.0
                    cur_point = [
                        a[_dir] + float(j)*_dx[_dir] + randomInstance.uniform(-actualDistortionX, actualDistortionX)*_dx[_dir] + float(i)*_dy[_dir] + randomInstance.uniform(-actualDistortionY, actualDistortionY)*_dy[_dir]
                        for _dir in range(3)
                    ]
                    id = int(i*(layers1+1) + j)
                    geoFile.write(f"Point({id}) = {{{cur_point[0]}, {cur_point[1]}, {cur_point[2]}}};\n")
            lineId = 0
            for i in range(layers2+1):
                for j in range(layers1):
                    idP0 = int(i*(layers1+1) + j)
                    numPointsPerLayer = layers1+1
                    geoFile.write(f"Line({lineId}) = {{{idP0}, {idP0+1}}};\n")
                    geoFile.write(f"Transfinite Line{{{lineId}}} = 2;\n")
                    lineId += 1
                    if i < layers2:
                        geoFile.write(f"Line({lineId}) = {{{idP0}, {idP0+numPointsPerLayer}}};\n")
                        geoFile.write(f"Transfinite Line{{{lineId}}} = 2;\n")
                        lineId += 1
                        if j == layers1 - 1:
                            geoFile.write(f"Line({lineId}) = {{{idP0+1}, {idP0+1+numPointsPerLayer}}};\n")
                            geoFile.write(f"Transfinite Line{{{lineId}}} = 2;\n")
                            lineId += 1
            surfId = 1
            for i in range(layers2):
                for j in range(layers1):
                    linesPerLayer = layers1*2 + 1
                    idLine0 = int(i*linesPerLayer + 2*j)
                    leftVerticalLineId = idLine0+3 if j < layers1 - 1 else idLine0 + 2
                    upperVerticalLineId = idLine0+linesPerLayer if i < layers2 - 1 else int((i+1)*linesPerLayer + j)
                    geoFile.write(f"Line Loop({surfId}) = {{{idLine0}, {leftVerticalLineId}, -{upperVerticalLineId}, -{idLine0+1}}};\n")
                    geoFile.write(f"Surface({surfId}) = {{{surfId}}};\n")
                    surfId += 1
            geoFile.write("Physical Surface(1) = {Surface{:}};\n")
            geoFile.write("Recombine Surface{Surface{:}};\n")
        _add_mesh_save_commands(geoFile, filename, numRefinements, 2)
    _run_gmsh(filename)

def _makeHex(corners, filename, cellSize, cellType, numRefinements) -> None:
    if cellType == CellType.default:
        cellType = CellType.hexahedron
    assert len(corners) == 8
    assert (
        cellType == CellType.hexahedron
        or cellType == CellType.tetrahedron
        or cellType == CellType.cube
    )

    dim = len(corners[0])
    assert all(len(c) == 3 for c in corners)

    if cellType == CellType.hexahedron:
        coords = getPerturbedCubeMeshCoords(
            (corners[0], corners[-1]),
            [cellSize for _ in range(dim)]
        )
        writePerturbedCubeMesh(_add_msh_extension(_add_refinement_index(filename, 0)), coords)
        for refIdx in range(1, numRefinements+1):
            coords = refinePerturbedCubeMeshCoords(coords)
            writePerturbedCubeMesh(_add_msh_extension(_add_refinement_index(filename, refIdx)), coords)
        return

    gmshReorderedCorners = [
        corners[0], corners[1], corners[3], corners[2],
        corners[0+4], corners[1+4], corners[3+4], corners[2+4]
    ]

    with open(filename, "w") as geoFile:
        dx = [corners[1][i] - corners[0][i] for i in range(3)]
        dy = [corners[2][i] - corners[0][i] for i in range(3)]
        dz = [corners[4][i] - corners[0][i] for i in range(3)]
        if cellType == CellType.cube:
            layers1 = int(round(_norm(dx)/cellSize))
            layers2 = int(round(_norm(dy)/cellSize))
            layers3 = int(round(_norm(dz)/cellSize))
            geoFile.write(f"Point(1) = {{{corners[0][0]}, {corners[0][1]}, {corners[0][2]}}};\n")
            geoFile.write(f"out[] = Extrude{{{dx[0]}, {dx[1]}, {dx[2]}}}")
            geoFile.write(f"{{Point{{1}}; Layers{{{layers1}}};}};\n")
            geoFile.write(f"resultLine = out[1];\n")
            geoFile.write(f"out[] = Extrude{{{dy[0]}, {dy[1]}, {dy[2]}}}")
            geoFile.write(f"{{Line{{resultLine}}; Layers{{{layers2}}}; Recombine;}};\n")
            geoFile.write(f"resultSurface = out[1];\n")
            geoFile.write(f"Extrude{{{dz[0]}, {dz[1]}, {dz[2]}}}")
            geoFile.write(f"{{Surface{{resultSurface}}; Layers{{{layers3}}}; Recombine;}}\n")
            geoFile.write("Physical Volume(1) = {Volume{:}};")
        else:
            geoFile.write(f"Point(1) = {{{corners[0][0]}, {corners[0][1]}, {corners[0][2]}}};\n")
            geoFile.write(f"out[] = Extrude{{{dx[0]}, {dx[1]}, {dx[2]}}}")
            geoFile.write(f"{{Point{{1}};}};\n")
            geoFile.write(f"resultLine = out[1];\n")
            geoFile.write(f"out[] = Extrude{{{dy[0]}, {dy[1]}, {dy[2]}}}")
            geoFile.write(f"{{Line{{resultLine}};}};\n")
            geoFile.write(f"resultSurface = out[1];\n")
            geoFile.write(f"Extrude{{{dz[0]}, {dz[1]}, {dz[2]}}}")
            geoFile.write(f"{{Surface{{resultSurface}};}}\n")
            geoFile.write("Physical Volume(1) = {Volume{:}};")
            if cellType == CellType.hexahedron:
                geoFile.write(f"Characteristic Length{{:}} = {cellSize*2.0};\n")
                geoFile.write("Mesh.Algorithm = 8;\n")
                geoFile.write("Mesh.SubdivisionAlgorithm = 2;\n")
            else:
                geoFile.write(f"Characteristic Length{{:}} = {cellSize};\n")
        _add_mesh_save_commands(geoFile, filename, numRefinements, 3)
    _run_gmsh(filename)

def _norm(vector: list) -> float:
    length = 0.0
    for dir in range(len(vector)):
        length += vector[dir]*vector[dir]
    return sqrt(length)

def _run(cmd):
    subprocess.run(cmd, check=True)
