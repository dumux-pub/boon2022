# define some plot parameters
import matplotlib.pyplot as plt
plt.rcParams['font.size'] = 13
plt.rcParams['font.weight'] = 'light'
plt.rcParams['axes.labelsize'] = 20
plt.rcParams['axes.labelweight'] = 'light'
plt.rcParams['xtick.labelsize'] = 13
plt.rcParams['ytick.labelsize'] = 13
plt.rcParams['legend.fontsize'] = 16
plt.rcParams['text.usetex'] = False
plt.rcParams['figure.titlesize'] = 15
