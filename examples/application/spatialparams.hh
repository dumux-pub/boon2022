// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief The spatial parameters class for the test problem using the 1p model.
 */
#ifndef DUMUX_MORTAR_DARCY_SPATIALPARAMS_HH
#define DUMUX_MORTAR_DARCY_SPATIALPARAMS_HH

#include <dune/common/exceptions.hh>

#include <dumux/io/grid/griddata.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \ingroup BoundaryTests
 * \brief The spatial parameters class for the test problem using the 1p model.
 */
template<class GridGeometry, class Scalar>
class OnePDarcySpatialParams
: public FVSpatialParamsOneP<GridGeometry, Scalar,
                             OnePDarcySpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using ParentType = FVSpatialParamsOneP<GridGeometry, Scalar,
                                           OnePDarcySpatialParams<GridGeometry, Scalar>>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridData = Dumux::GridData<typename GridView::Grid>;

public:
    //! Export permeability type
    using PermeabilityType = Scalar;

    //! Markers of the different domains
    static constexpr int faultMarker = 1;
    static constexpr int barrierMarker = 2;
    static constexpr int layerMarker = 3;

    //! The constructor
    OnePDarcySpatialParams(std::shared_ptr<const GridGeometry> fvGridGeometry,
                           const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , gridData_(nullptr)
    {
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        useMarkers_ = false;
    }

    //! The constructor with grid data
    OnePDarcySpatialParams(std::shared_ptr<const GridGeometry> fvGridGeometry,
                           const std::string& paramGroup,
                           std::shared_ptr<const GridData> gridData)
    : ParentType(fvGridGeometry)
    , gridData_(gridData)
    {
        if (paramGroup != "")
        {
            permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
            useMarkers_ = false;
        }
        else
            useMarkers_ = true;
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     * \param globalPos The global position
     * \return the intrinsic permeability
     */
    template<class SubControlVolume, class ElemSol>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElemSol& elemSol) const
    {
        if (!useMarkers_)
            return permeability_;
        else
        {
            static const Scalar faultK_ = getParam<Scalar>("SpatialParams.FaultPermeability");
            static const Scalar domainK_ = getParam<Scalar>("SpatialParams.DomainPermeability");
            static const Scalar barrierK_ = getParam<Scalar>("SpatialParams.BarrierPermeability");

            const auto m = getElementDomainMarker(element);
            if (m == faultMarker) return faultK_;
            else if (m == layerMarker) return domainK_;
            else if (m == barrierMarker) return barrierK_;

            DUNE_THROW(Dune::InvalidStateException, "Invalid domain marker");
        }
    }

    /*!
     * \brief Defines the porosity in [-].
     * \param globalPos The global position
     * \returns the porosity
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

    /*!
     * \brief Return the domain marker of an element.
     */
    int getElementDomainMarker(const Element& element) const
    {
        if (!gridData_)
            DUNE_THROW(Dune::InvalidStateException, "Grid data not available");

        return gridData_->getElementDomainMarker(element);
    }

private:
    Scalar permeability_;
    std::shared_ptr<const GridData> gridData_;
    bool useMarkers_;
};

} // end namespace Dumux

#endif
