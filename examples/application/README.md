Example 2: faulted geology
=========================

To reproduce the results, go to the build directory for this example and
execute the script `runscript.sh`. This will produce subfolders with the
results in 2d and 3d. For visualization, you can use the provided ParaView
state files (files with extension `.pvsm`) by opening ParaView from within
a folder with results and then loading the state (the state files assume all
results files to be present in the folder from which ParaView was launched).
