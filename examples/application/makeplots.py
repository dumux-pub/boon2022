import os
import sys
import subprocess
from numpy import genfromtxt, loadtxt, isnan
from argparse import ArgumentParser


def exitWithError(message):
    sys.stderr.write(message)
    sys.exit(1)


# we require Python3
if sys.version_info[0] < 3:
    exitWithError("Python 3 required\n")

# import plot config
try:
    utilFolder = os.getcwd().split('examples')
    utilFolder = os.path.join(utilFolder[0], 'examples/util')
    sys.path.append(utilFolder)
    from mplconfig import plt
except Exception:
    exitWithError("Could not load plot config module\n")

# find plot over line script
scriptFolder = os.getcwd().split("examples")
scriptFolder = os.path.dirname(scriptFolder[0].split("build-cmake")[0])
scriptFolder = os.path.join(scriptFolder, "bin")
polScript = os.path.join(scriptFolder, "extractlinedata.py")
if not os.path.exists(polScript):
    exitWithError("Could not find plot over line script\n")


def getStartPoint(id):
    return [0, 0, 0]


def getEndPoint(id):
    if id == '2d':
        return [1, 1, 0]
    elif id == '3d':
        return [1, 1, 1]
    else:
        raise Exception("Unknown identifier")


def getVTKFileName(basename, domainId=None, ext='.vtu'):
    domainSuffix = f"_{domainId}" if domainId is not None else ""
    return f"{basename}{domainSuffix}-00000{ext}"


def getDomainVTKFileNames(prefix):
    nDomains = 3
    return [getVTKFileName(f"{prefix}domain", i) for i in range(0, nDomains)]


def getFaultVTKFileNames(prefix):
    nFaults = 1
    return [getVTKFileName(f"{prefix}fault", i) for i in range(0, nFaults)]


def getBarrierVTKFileNames(prefix):
    nBarriers = 2
    return [getVTKFileName(f"{prefix}barrier", i) for i in range(0, nBarriers)]


def getConformingVTKFileName(prefix):
    return getVTKFileName(f"{prefix}conforming" if prefix else "conforming")


def getPlotOverLineData(start, end, file, resolution=2000):
    print("Reading pol data from {}".format(file))
    tmpFileName = "_tmp193081_"
    subprocess.run([
        "pvbatch",
        polScript,
        "-f", file,
        "--point1", str(start[0]), str(start[1]), str(start[2]),
        "--point2", str(end[0]), str(end[1]), str(end[2]),
        "-r", str(resolution),
        "-outFile", tmpFileName,
        "-o", "."
    ], check=True)
    data = genfromtxt(f"{tmpFileName}.csv", delimiter=',', names=True)
    os.remove(f"{tmpFileName}.csv")
    return data


if __name__ == "__main__":
    parser = ArgumentParser(description="Plot pressure across the domain")
    parser.add_argument('-t', '--title', required=False, default="")
    parser.add_argument('-p', '--fileprefixes', required=True, nargs='*')
    parser.add_argument('-l', '--labels', required=True, nargs='*')
    parser.add_argument(
        '-i', '--identifier', required=True, choices=['2d', '3d']
    )
    parser.add_argument(
        '-x', '--xfieldname', required=False, default='Points0'
    )
    parser.add_argument(
        '-y', '--yfieldname', required=False, default='p'
    )
    parser.add_argument(
        '-o', '--outfilename', required=False, default='plot.pdf'
    )

    args = vars(parser.parse_args())
    if len(args['labels']) != len(args['fileprefixes']):
        exitWithError("Number of labels and prefixes do not match!\n")

    numSubDomains = 3
    numBarriers = 2
    numFaults = 1

    xAxisName = args['xfieldname']
    yAxisName = args['yfieldname']

    p0 = getStartPoint(args['identifier'])
    p1 = getEndPoint(args['identifier'])

    # load color table
    colors = loadtxt(os.path.join(utilFolder, 'colortable.csv'),
                     delimiter=',', skiprows=1)

    plt.figure()
    for idx, [prefix, label] in enumerate(zip(args['fileprefixes'],
                                              args['labels'])):
        confLabel = f"{label} (conforming)" if label else "monolithic"
        label = label if label else "iterative"
        for subIdx, file in enumerate(getDomainVTKFileNames(prefix)
                                      + getBarrierVTKFileNames(prefix)
                                      + getFaultVTKFileNames(prefix)):
            d = getPlotOverLineData(p0, p1, file)
            plt.plot(
                d[xAxisName], d[yAxisName],
                label=(label if subIdx == 0 else ""),
                color=colors[2*idx]
            )

        d = getPlotOverLineData(p0, p1, getConformingVTKFileName(prefix))
        plt.plot(
            d[xAxisName], d[yAxisName],
            label=confLabel,
            color=colors[2*idx + 1],
            linestyle='--'
        )
    plt.legend()
    plt.xlabel("x")
    plt.ylabel(yAxisName)
    if args['title']:
        plt.title(args['title'])
    plt.savefig(args['outfilename'], bbox_inches='tight')

    # make interface plot
    plt.clf()
    plt.figure()

    if_p0 = [0.468012108211, 4.58655984258e-17, 0.413120583959]
    if_p1 = [0.725, 1, 0.85]
    if_extension = ".vtu"

    if args["identifier"] == "2d":
        if_p0 = [if_p0[0], if_p0[2], 0.0]
        if_p1 = [if_p1[0], if_p1[2], 0.0]
        if_extension = ".vtp"

    # move point 1 a little bit to avoid nan values
    delta = [if_p1[i]-if_p0[i] for i in range(3)]
    delta = [d*1e-4 for d in delta]
    if_p0 = [if_p0[i] + delta[i] for i in range(3)]

    for idx, [prefix, label] in enumerate(zip(args['fileprefixes'],
                                              args['labels'])):

        confLabel = f"{label} (conforming)" if label else "monolithic"
        label = label if label else "iterative"

        seq_ifvtk = getVTKFileName(f"{prefix}interface", 10, if_extension)
        d = getPlotOverLineData(if_p0, if_p1, seq_ifvtk)
        plt.plot(
            d["arc_length"], d["mortar_variable"],
            label=label,
            color=colors[2*idx]
        )

        d = getPlotOverLineData(
            if_p0, if_p1, f"{prefix}conforming_interface" + if_extension
        )
        plt.plot(
            d["arc_length"], d["flux"],
            label=confLabel,
            color=colors[2*idx + 1],
            linestyle='--'
        )

    plt.xlabel("x")
    plt.ylabel(r"$\lambda$")
    plt.legend()
    if args['title']:
        plt.title(args['title'] + " (interface)")
    plt.savefig(os.path.splitext(args['outfilename'])[0] + "_interface.pdf",
                bbox_inches="tight")
