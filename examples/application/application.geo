SetFactory("OpenCASCADE");

// use quadrilaterals?
DefineConstant[ useQuads = 0 ];
If (useQuads)
    // Mesh.Algorithm = 8;
    Mesh.SubdivisionAlgorithm = 1;
EndIf

// domain dimensions
minx = 0.0; maxx = 1.0;
miny = 0.0; maxy = 1.0;

// barrier dimensions
barrierMiny = 0.4*maxy;
barrierMaxThickness = 0.2*(maxy-miny);
barrierAngle = Pi/10.0;
barrierDeltaY = 0.5*barrierMaxThickness;

// mesh size (modifiable via command-line)
DefineConstant[ meshSizeFactor = 1.0 ];
rawMeshSize = 0.1*0.5*(maxx-minx+maxy-miny);
meshSize = rawMeshSize*meshSizeFactor;

// define domain
domain = news; Rectangle(domain) = {minx, miny, 0.0, (maxx-minx), (maxy-miny)};

// define raw barrier layer as triangle
p1 = newp; Point(p1) = {-0.75*(maxx-minx) - minx, barrierMiny, 0.0};
p2 = newp; Point(p2) = {maxx + 0.25*(maxx-minx), barrierMiny, 0.0};
p3 = newp; Point(p3) = {maxx + 0.25*(maxx-minx), barrierMiny + barrierMaxThickness, 0.0};
l1 = newl; Line(l1) = {p1, p2};
l2 = newl; Line(l2) = {p2, p3};
l3 = newl; Line(l3) = {p3, p1};
ll = newll; Line Loop(ll) = {l1, l2, l3};
barrier1 = news; Plane Surface(barrier1) = {ll};

// rotate barrier
Rotate {{0, 0, 1}, {minx + 0.5*(maxx-minx), miny + 0.5*(maxy-miny), 0}, barrierAngle} { Surface{barrier1}; }

// move barrier downwards
Translate {0, -barrierDeltaY, 0} { Surface{barrier1}; }

// move copy upwards
faultCut[] = Translate {0, 2.0*barrierDeltaY, 0} { Duplicata { Surface{barrier1}; } };
barrier2 = faultCut[0];

// fault center line
p1_fault = newp; Point(p1_fault) = {0.25*maxx, miny, 0.0};
p2_fault = newp; Point(p2_fault) = {0.75*maxx, 0.85*maxy, 0.0};
fault = newl; Line(fault) = {p1_fault, p2_fault};

// fault area
faultThickness = (maxx - minx)*0.05;
Translate {faultThickness/2.0, 0, 0} {  Curve{fault}; }
faultSurfaceExtrusion[] = Extrude {-faultThickness, 0, 0} { Curve{fault}; };
faultSurface = faultSurfaceExtrusion[1];

// cut barrier by fault
barrierCut1[] = BooleanDifference{ Surface{barrier1}; Recursive Delete; }{ Surface{faultSurface}; };
barrierCut2[] = BooleanDifference{ Surface{barrier2}; Recursive Delete; }{ Surface{faultSurface}; };
barrier1 = barrierCut1[0];
barrier2 = barrierCut2[1];
Recursive Delete { Surface{barrierCut1[1]}; }
Recursive Delete { Surface{barrierCut2[0]}; }

// confine barrier layers by domain
barrierConfine1[] = BooleanIntersection{ Surface{barrier1}; Delete; }{ Surface{domain}; };
barrierConfine2[] = BooleanIntersection{ Surface{barrier2}; Delete; }{ Surface{domain}; };
barrier1 = barrierConfine1[0];
barrier2 = barrierConfine2[0];

// cut out the barrier layers of the domain
domainCut[] = BooleanDifference{ Surface{domain}; Recursive Delete; }{ Surface{barrier1, barrier2}; };
domain = domainCut[0];

// cut away the fault surface from domain
domainFaultCut[] = BooleanDifference{ Surface{domain}; Recursive Delete; }{ Surface{faultSurface}; };

// fragment fault with all other domains (this fragments the boundary, otherwise boundary intersection algo later failed)
faultSurface = BooleanFragments{
    Surface{faultSurface}; Recursive Delete;
}{
    Surface{barrier1, barrier2, domainFaultCut[0], domainFaultCut[1], domainFaultCut[2]};
};
faultSurface = faultSurface[0];

// mesh size to be used on all points
Characteristic Length{:} = meshSize;
Printf("Mesh size = ", meshSize);

DefineConstant[ make3d = 0 ];
DefineConstant[ makeConforming = 0 ];

DefineConstant[ meshFault1 = 0 ];
DefineConstant[ meshBarrier1 = 0 ];
DefineConstant[ meshBarrier2 = 0 ];
DefineConstant[ meshSubDomain1 = 0 ];
DefineConstant[ meshSubDomain2 = 0 ];
DefineConstant[ meshSubDomain3 = 0 ];

DefineConstant[ meshMortar1 = 0];
DefineConstant[ meshMortar2 = 0];
DefineConstant[ meshMortar3 = 0];
DefineConstant[ meshMortar4 = 0];
DefineConstant[ meshMortar5 = 0];
DefineConstant[ meshMortar6 = 0];
DefineConstant[ meshMortar7 = 0];
DefineConstant[ meshMortar8 = 0];
DefineConstant[ meshMortar9 = 0];
DefineConstant[ meshMortar10 = 0];
DefineConstant[ meshMortar11 = 0];

If (make3d == 0)

    // define the physical indices and exit
    If (makeConforming == 0)

        subDomain1 = domainFaultCut[0];
        subDomain2 = domainFaultCut[2];
        subDomain3 = domainFaultCut[1];

        // make the different layers & mortars physical
        If (meshFault1) Physical Surface(1) = {faultSurface}; EndIf
        If (meshBarrier1) Physical Surface(2) = {barrier1}; EndIf
        If (meshBarrier2) Physical Surface(2) = {barrier2}; EndIf
        If (meshSubDomain1) Physical Surface(3) = {subDomain1}; EndIf
        If (meshSubDomain2) Physical Surface(3) = {subDomain2}; EndIf
        If (meshSubDomain3) Physical Surface(3) = {subDomain3}; EndIf

        // identify mortars
        bDomain1 = Abs(CombinedBoundary{ Surface{subDomain1}; });
        bDomain2 = Abs(CombinedBoundary{ Surface{subDomain2}; });
        bDomain3 = Abs(CombinedBoundary{ Surface{subDomain3}; });
        bBarrier1 = Abs(CombinedBoundary{ Surface{barrier1}; });
        bBarrier2 = Abs(CombinedBoundary{ Surface{barrier2}; });
        bFault = Abs(CombinedBoundary{ Surface{fault}; });
        mortarD1B1 = BooleanIntersection{ Curve{bDomain1[]}; }{ Curve{bBarrier1[]}; };
        mortarD2B2 = BooleanIntersection{ Curve{bDomain2[]}; }{ Curve{bBarrier2[]}; };
        mortarD3B1 = BooleanIntersection{ Curve{bDomain3[]}; }{ Curve{bBarrier1[]}; };
        mortarD3B2 = BooleanIntersection{ Curve{bDomain3[]}; }{ Curve{bBarrier2[]}; };
        mortarB1F = BooleanIntersection{ Curve{bBarrier1[]}; }{ Curve{bFault[]}; };
        mortarB2F = BooleanIntersection{ Curve{bBarrier2[]}; }{ Curve{bFault[]}; };
        mortarD1F = BooleanIntersection{ Curve{bDomain1[]}; }{ Curve{bFault[]}; };
        mortarD2F = BooleanIntersection{ Curve{bDomain2[]}; }{ Curve{bFault[]}; };
        mortarD3F[] = BooleanIntersection{ Curve{bDomain3[]}; }{ Curve{bFault[]}; };
        mortarD3F_1 = mortarD3F[0];
        mortarD3F_2 = mortarD3F[1];
        mortarD3F_3 = mortarD3F[2];

        If (meshMortar1) Physical Line(1) = {mortarD1B1}; EndIf
        If (meshMortar2) Physical Line(2) = {mortarD2B2}; EndIf
        If (meshMortar3) Physical Line(3) = {mortarD3B1}; EndIf
        If (meshMortar4) Physical Line(4) = {mortarD3B2}; EndIf
        If (meshMortar5) Physical Line(5) = {mortarB1F}; EndIf
        If (meshMortar6) Physical Line(6) = {mortarB2F}; EndIf
        If (meshMortar7) Physical Line(7) = {mortarD1F}; EndIf
        If (meshMortar8) Physical Line(8) = {mortarD2F}; EndIf
        If (meshMortar9) Physical Line(9) = {mortarD3F_1}; EndIf
        If (meshMortar10) Physical Line(10) = {mortarD3F_2}; EndIf
        If (meshMortar11) Physical Line(11) = {mortarD3F_3}; EndIf

    // remove duplicate geometries
    Else

        // mesh sizes
        DefineConstant[ meshFactorFault = 1.0 ];
        DefineConstant[ meshFactorBarrier = 1.0 ];
        DefineConstant[ meshFactorLayer = 1.0 ];

        meshSizeBarrier = rawMeshSize*meshFactorBarrier;
        meshSizeFault = rawMeshSize*meshFactorFault;
        meshSizeLayer = rawMeshSize*meshFactorLayer;

        BooleanFragments {Surface{:}; Delete;}{Surface{:}; Delete;}
        Physical Surface(1) = {11};
        Physical Surface(2) = {12, 15};
        Physical Surface(3) = {16, 17, 18};
        Characteristic Length{PointsOf{Surface{16, 17, 18};}} = meshSizeLayer;
        Characteristic Length{PointsOf{Surface{12, 15};}} = meshSizeBarrier;
        Characteristic Length{PointsOf{Surface{11};}} = meshSizeFault;

    EndIf

// in 3d, extrude the grid and define physical indices
Else

    // rotate the whole thing such that y-axis becomes z-axis
    Rotate {{1, 0, 0}, {0, 0, 0}, Pi/2.0} {Surface{:};}

    // extrude subdomains
    out[] = Extrude {0, 1, 0}{Surface{faultSurface};};      fault = out[1];
    out[] = Extrude {0, 1, 0}{Surface{barrier1};};          barrier1 = out[1];
    out[] = Extrude {0, 1, 0}{Surface{barrier2};};          barrier2 = out[1];
    out[] = Extrude {0, 1, 0}{Surface{domainFaultCut[0]};}; subDomain1 = out[1];
    out[] = Extrude {0, 1, 0}{Surface{domainFaultCut[2]};}; subDomain2 = out[1];
    out[] = Extrude {0, 1, 0}{Surface{domainFaultCut[1]};}; subDomain3 = out[1];

    // identify mortars
    bDomain1 = Abs(CombinedBoundary{ Volume{subDomain1}; });
    bDomain2 = Abs(CombinedBoundary{ Volume{subDomain2}; });
    bDomain3 = Abs(CombinedBoundary{ Volume{subDomain3}; });
    bBarrier1 = Abs(CombinedBoundary{ Volume{barrier1}; });
    bBarrier2 = Abs(CombinedBoundary{ Volume{barrier2}; });
    bFault = Abs(CombinedBoundary{ Volume{fault}; });
    mortarD1B1 = BooleanIntersection{ Surface{bDomain1[]}; }{ Surface{bBarrier1[]}; };
    mortarD2B2 = BooleanIntersection{ Surface{bDomain2[]}; }{ Surface{bBarrier2[]}; };
    mortarD3B1 = BooleanIntersection{ Surface{bDomain3[]}; }{ Surface{bBarrier1[]}; };
    mortarD3B2 = BooleanIntersection{ Surface{bDomain3[]}; }{ Surface{bBarrier2[]}; };
    mortarB1F = BooleanIntersection{ Surface{bBarrier1[]}; }{ Surface{bFault[]}; };
    mortarB2F = BooleanIntersection{ Surface{bBarrier2[]}; }{ Surface{bFault[]}; };
    mortarD1F = BooleanIntersection{ Surface{bDomain1[]}; }{ Surface{bFault[]}; };
    mortarD2F = BooleanIntersection{ Surface{bDomain2[]}; }{ Surface{bFault[]}; };
    mortarD3F[] = BooleanIntersection{ Surface{bDomain3[]}; }{ Surface{bFault[]}; };
    mortarD3F_1 = mortarD3F[0];
    mortarD3F_2 = mortarD3F[1];
    mortarD3F_3 = mortarD3F[2];

    // make the different layers & mortars physical
    If (makeConforming)
        out[] = BooleanFragments {Volume{:}; Delete;}{Volume{:}; Delete;};
        Physical Volume(1) = {fault};
        Physical Volume(2) = {barrier1, barrier2};
        Physical Volume(3) = {subDomain1, subDomain2, subDomain3};

        // mesh sizes
        DefineConstant[ meshFactorFault = 1.0 ];
        DefineConstant[ meshFactorBarrier = 1.0 ];
        DefineConstant[ meshFactorLayer = 1.0 ];

        meshSizeBarrier = rawMeshSize*meshFactorBarrier;
        meshSizeFault = rawMeshSize*meshFactorFault;
        meshSizeLayer = rawMeshSize*meshFactorLayer;

        Printf("%g", meshSizeBarrier);
        Printf("%g", meshSizeFault);
        Printf("%g", meshSizeLayer);

        Characteristic Length{PointsOf{Volume{4, 5, 6};}} = meshSizeLayer;
        Characteristic Length{PointsOf{Volume{2, 3};}} = meshSizeBarrier;
        Characteristic Length{PointsOf{Volume{1};}} = meshSizeFault;
    Else
        If (meshFault1) Physical Volume(1) = {fault}; EndIf
        If (meshBarrier1) Physical Volume(2) = {barrier1}; EndIf
        If (meshBarrier2) Physical Volume(2) = {barrier2}; EndIf
        If (meshSubDomain1) Physical Volume(3) = {subDomain1}; EndIf
        If (meshSubDomain2) Physical Volume(3) = {subDomain2}; EndIf
        If (meshSubDomain3) Physical Volume(3) = {subDomain3}; EndIf
        If (meshMortar1)   Physical Surface(1)  = {mortarD1B1}; EndIf
        If (meshMortar2)   Physical Surface(2)  = {mortarD2B2}; EndIf
        If (meshMortar3)   Physical Surface(3)  = {mortarD3B1}; EndIf
        If (meshMortar4)   Physical Surface(4)  = {mortarD3B2}; EndIf
        If (meshMortar5)   Physical Surface(5)  = {mortarB1F}; EndIf
        If (meshMortar6)   Physical Surface(6)  = {mortarB2F}; EndIf
        If (meshMortar7)   Physical Surface(7)  = {mortarD1F}; EndIf
        If (meshMortar8)   Physical Surface(8)  = {mortarD2F}; EndIf
        If (meshMortar9)   Physical Surface(9)  = {mortarD3F_1}; EndIf
        If (meshMortar10) Physical Surface(10) = {mortarD3F_2}; EndIf
        If (meshMortar11) Physical Surface(11) = {mortarD3F_3}; EndIf
    EndIf
EndIf
