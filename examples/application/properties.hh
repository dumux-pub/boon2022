// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief Properties for the Darcy test problem.
 */
#ifndef DUMUX_MORTAR_DARCY_PROPERTIES_HH
#define DUMUX_MORTAR_DARCY_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <examples/common/subdomainproperties.hh>

#include "problem.hh"
#include "spatialparams.hh"

// grid type might be set by CMAKELISTS.TXT
#ifndef GRIDTYPE
#define GRIDTYPE Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<Scalar, 2>>
#endif

namespace Dumux::Properties {

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::SubDomainSolverOneP>
{ using type = ApplicationSubProblem<TypeTag>; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::SubDomainSolverOneP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = GRIDTYPE;
};

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::SubDomainSolverOneP>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = OnePDarcySpatialParams<GridGeometry, Scalar>;
};

} // end namespace Dumux::Properties

#endif
