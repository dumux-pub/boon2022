import os
import sys
import subprocess


# class to prepare the mesh files
class Mesher:
    def __init__(self, dimension,
                 outFolder='',
                 numSubDomains=3,
                 numBarriers=2,
                 numFaults=1,
                 numInterfaces=11,
                 verbose=False):
        self.dimension_ = dimension
        self.outFolder_ = outFolder
        self.numSubDomains_ = numSubDomains
        self.numBarriers_ = numBarriers
        self.numFaults_ = numFaults
        self.numInterfaces_ = numInterfaces
        self.verbose_ = verbose

    def getMeshFileNames(self):
        subDomainMeshes = []
        for i in [str(j) for j in range(1, self.numSubDomains_+1)]:
            meshFile = os.path.join(self.outFolder_, 'domain_' + i + '.msh')
            subDomainMeshes.append(meshFile)

        barrierMeshes = []
        for i in [str(j) for j in range(1, self.numBarriers_+1)]:
            meshFile = os.path.join(self.outFolder_, 'barrier_' + i + '.msh')
            barrierMeshes.append(meshFile)

        faultMeshes = []
        for i in [str(j) for j in range(1, self.numFaults_+1)]:
            meshFile = os.path.join(self.outFolder_, 'fault_' + i + '.msh')
            faultMeshes.append(meshFile)

        interfaceMeshes = []
        for i in [str(j) for j in range(1, self.numInterfaces_+1)]:
            meshFile = os.path.join(self.outFolder_, 'interface_' + i + '.msh')
            interfaceMeshes.append(meshFile)

        return subDomainMeshes, barrierMeshes, faultMeshes, interfaceMeshes

    def prepareMeshes(self, meshSize, useQuads=False):

        useQuadString = '1' if useQuads else '0'
        verbosityLevel = '10' if self.verbose_ else '0'
        make3d = '1' if self.dimension_ == 3 else '0'

        if not os.path.exists('application.geo'):
            sys.exit('Could not find application.geo file in current folder.')
        if self.outFolder_ and not os.path.exists(self.outFolder_):
            os.makedirs(self.outFolder_, exist_ok=True)

        # mesh sub domains
        subDomainMeshes = []
        for i in range(1, self.numSubDomains_+1):
            print("Making mesh for subdomain {}".format(i))
            meshFile = os.path.join(self.outFolder_, 'domain_{}.msh'.format(i))
            ms = meshSize.get('subdomain', i-1)
            subDomainMeshes.append(meshFile)
            subprocess.run(['gmsh',
                            '-format', 'msh2',
                            '-v', verbosityLevel,
                            '-setnumber', 'meshSubDomain{}'.format(i), '1',
                            '-setnumber', 'meshSizeFactor', str(ms),
                            '-setnumber', 'useQuads', useQuadString,
                            '-setnumber', 'make3d', make3d,
                            '-' + str(self.dimension_), 'application.geo',
                            '-o', meshFile], check=True)

        # mesh barriers
        barrierMeshes = []
        for i in range(1, self.numBarriers_+1):
            print("Making mesh for barrier {}".format(i))
            meshFile = os.path.join(self.outFolder_, 'barrier_{}.msh'.format(i))
            ms = meshSize.get('barrier', i-1)
            barrierMeshes.append(meshFile)
            subprocess.run(['gmsh',
                            '-format', 'msh2',
                            '-v', verbosityLevel,
                            '-setnumber', 'meshBarrier{}'.format(i), '1',
                            '-setnumber', 'meshSizeFactor', str(ms),
                            '-setnumber', 'useQuads', useQuadString,
                            '-setnumber', 'make3d', make3d,
                            '-' + str(self.dimension_), 'application.geo',
                            '-o', meshFile], check=True)

        # mesh faults
        faultMeshes = []
        for i in range(1, self.numFaults_+1):
            print("Making mesh for fault {}".format(i))
            meshFile = os.path.join(self.outFolder_, 'fault_{}.msh'.format(i))
            ms = meshSize.get('fault', i-1)
            faultMeshes.append(meshFile)
            subprocess.run(['gmsh',
                            '-format', 'msh2',
                            '-v', verbosityLevel,
                            '-setnumber', 'meshFault{}'.format(i), '1',
                            '-setnumber', 'meshSizeFactor', str(ms),
                            '-setnumber', 'useQuads', useQuadString,
                            '-setnumber', 'make3d', make3d,
                            '-' + str(self.dimension_), 'application.geo',
                            '-o', meshFile], check=True)

        # mesh interfaces
        interfaceMeshes = []
        for i in range(1, self.numInterfaces_+1):
            print("Making mesh for interface {}".format(i))
            meshFile = os.path.join(
                self.outFolder_, 'interface_{}.msh'.format(i)
            )
            ms = meshSize.get('interface', i-1)
            interfaceMeshes.append(meshFile)
            subprocess.run(['gmsh',
                            '-format', 'msh2',
                            '-v', verbosityLevel,
                            '-setnumber', 'meshMortar{}'.format(i), '1',
                            '-setnumber', 'meshSizeFactor', str(ms),
                            '-setnumber', 'make3d', make3d,
                            '-' + str(self.dimension_-1), 'application.geo',
                            '-o', meshFile], check=True)

        # return created mesh files
        return subDomainMeshes, barrierMeshes, faultMeshes, interfaceMeshes

    def prepareConformingMesh(self,
                              refDomain, refBarriers, refFault,
                              useQuads=False):

        print("Making single-domain mesh")
        make3d = '1' if self.dimension_ == 3 else '0'
        verbosityLevel = '10' if self.verbose_ else '0'
        meshFile = os.path.join(self.outFolder_, 'conforming.msh')
        subprocess.run(['gmsh',
                        '-format', 'msh2',
                        '-v', verbosityLevel,
                        '-setnumber', 'makeConforming', '1',
                        '-setnumber', 'meshFactorFault', str(refFault),
                        '-setnumber', 'meshFactorBarrier', str(refBarriers),
                        '-setnumber', 'meshFactorLayer', str(refDomain),
                        '-setnumber', 'useQuads', ('1' if useQuads else '0'),
                        '-setnumber', 'make3d', make3d,
                        '-' + str(self.dimension_), 'application.geo',
                        '-o', meshFile], check=True)
        return meshFile

    def getConformingMeshFileName(self):
        return 'conforming.msh'
