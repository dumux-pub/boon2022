// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_MORTAR_DARCY_SUBPROBLEM_HH
#define DUMUX_MORTAR_DARCY_SUBPROBLEM_HH

#include <limits>
#include <dune/common/fvector.hh>

#include <dumux/common/numeqvector.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <examples/common/interface.hh>
#include <examples/common/subdomainproblem.hh>

namespace Dumux {

template <class TypeTag>
class ApplicationSubProblem : public SubDomainProblem<TypeTag>
{
    using ParentType = SubDomainProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimension;
    static_assert(dim == 2 || dim == 3);

public:
    template<typename SpatialParams>
    ApplicationSubProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                          std::shared_ptr<SpatialParams> spatialParams,
                          const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    {}

    //! Specifies the type of boundary condition as function of the position.
    BoundaryTypes<1> boundaryTypesAtFace(const Element& element,
                                         const SubControlVolumeFace& scvf) const
    {
        const auto& globalPos = scvf.ipGlobal();

        BoundaryTypes<1> values;
        values.setAllDirichlet();

        static const GlobalPosition upperRight = getParam<GlobalPosition>("Problem.UpperRight");
        static const GlobalPosition lowerLeft = getParam<GlobalPosition>("Problem.LowerLeft");
        const auto onTop = [&] (const GlobalPosition& p) { return p[dim-1] > upperRight[dim-1] - 1e-6; };
        const auto onBottom = [&] (const GlobalPosition& p) { return p[dim-1] < lowerLeft[dim-1] + 1e-6; };
        const auto marker = this->spatialParams().getElementDomainMarker(element);
        const auto isBarrier = marker == this->spatialParams().barrierMarker;

        if (isBarrier)
            values.setAllNeumann();

        if (onTop(globalPos) || onBottom(globalPos))
            values.setAllNeumann();

        if constexpr (dim == 3)
            if (globalPos[dim-2] < lowerLeft[dim-2] + 1e-6
                || globalPos[dim-2] > upperRight[dim-2] - 1e-6)
                values.setAllNeumann();

        return values;
    }

    //! Evaluate the boundary solution at a given position.
    NumEqVector boundarySolution(const GlobalPosition& globalPos) const
    {
        const auto x = globalPos[0];
        const auto height = globalPos[dimWorld-1];

        static const Scalar lowerInletPressure = getParam<Scalar>("Problem.LowerInletPressure");
        static const Scalar upperInletPressure = getParam<Scalar>("Problem.UpperInletPressure");
        static const Scalar outletPressure = getParam<Scalar>("Problem.OutletPressure");
        static const GlobalPosition upperRight = getParam<GlobalPosition>("Problem.UpperRight");
        static const GlobalPosition lowerLeft = getParam<GlobalPosition>("Problem.LowerLeft");
        static const auto xMin = lowerLeft[0];
        static const auto heightMin = lowerLeft[dimWorld-1];
        static const auto heightMax = upperRight[dimWorld-1];

        double yGradFactor = 0.0;
        if constexpr (dim == 3)
        {
            static const Scalar deltaPY = getParam<Scalar>("Problem.DeltaPY");
            static const auto yMin = lowerLeft[1];
            static const auto yMax = upperRight[1];
            yGradFactor = deltaPY*(globalPos[1] - yMin)/(yMax - yMin);
        }

        if (x < xMin + 1e-5)
        {
            if (height > heightMin + 0.15*(heightMax - heightMin))
                return NumEqVector(upperInletPressure*(1.0 - yGradFactor));
            return NumEqVector(lowerInletPressure*(1.0 - yGradFactor));
        }
        else
            return NumEqVector(outletPressure*(1.0 - yGradFactor));

        DUNE_THROW(Dune::InvalidStateException, "Unexpected boundary position");
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return boundarySolution(globalPos); }
};

} // end namespace Dumux

#endif
