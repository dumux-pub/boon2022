#!/bin/bash

moveResults() {
    DIRNAME=$1
    mkdir -p $DIRNAME
    mv *vtu *vtp *pvd $DIRNAME
}

run() {
    if ! $1; then
        echo "Error upon running $1"
        exit 1
    fi
}

runAndPlot() {
   MESHTYPE=$1
   IDENTIFIER=$2
   TITLE=$3
   PROJ_TYPE=$4

   TPFA_PRE=${MESHTYPE}_tpfa
   MPFA_PRE=${MESHTYPE}_mpfa

   run "python3 run.py -s mpfa -m $MESHTYPE -p ${PROJ_TYPE}"
   moveResults $MPFA_PRE

   python3 makeplots.py \
       -p ${MPFA_PRE}/ \
       -l "" -i $IDENTIFIER \
       -o ${MESHTYPE}.pdf \
       -t ""
}

PROJ="flat"
runAndPlot tria 2d "triangular grid" $PROJ
runAndPlot tets 3d "tetrahedral grid" $PROJ
