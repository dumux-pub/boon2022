// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief TODO doc me
 */
#include <config.h>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/alugrid/grid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <examples/common/subdomainsolver.hh>
#include "properties.hh"

template<typename Normal>
int getSign(int insideDomainIdx, int outsideDomainIdx, const Normal& normal)
{
    return normal*Normal(1.0) > 0.0 ? 1 : -1;
}


// write out the interface fluxes to VTK
template<typename FluxVariables, typename GridGeometry, typename GridVariables, typename SolutionVector, typename Problem>
void writeInterfaceFluxVTU(const GridGeometry& gg,
                           const GridVariables& gv,
                           const Problem& p,
                           const SolutionVector& x)
{
    using InterfaceGrid = MONOLITHICINTERFACEGRID;
    Dune::GridFactory<InterfaceGrid> factory;

    std::vector<double> fluxes;
    fluxes.reserve(gg.gridView().size(0)*2);

    std::size_t curVertexIndex = 0;
    std::vector<unsigned int> cornerIndices;

    for (const auto& element : elements(gg.gridView()))
    {
        auto fvGeometry = localView(gg);
        auto elemVolVars = localView(gv.curGridVolVars());
        auto elemFluxVarsCache = localView(gv.gridFluxVarsCache());

        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, x);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

        for (const auto& scvf : scvfs(fvGeometry))
        {
            if (scvf.boundary() || scvf.insideScvIdx() > scvf.outsideScvIdx())
                continue;

            const auto outsideElement = gg.element(scvf.outsideScvIdx());
            const auto outsideMarker = p.spatialParams().getElementDomainMarker(outsideElement);
            const auto insideMarker = p.spatialParams().getElementDomainMarker(element);
            if (outsideMarker == insideMarker)
                continue;

            const auto& scvfGeom = scvf.geometry();

            FluxVariables fluxVars;
            fluxVars.init(p, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
            const auto flux = fluxVars.advectiveFlux(
                0, [] (const auto& vv) { return vv.mobility()*vv.density(); }
            );

            cornerIndices.clear();
            for (int i = 0; i < scvfGeom.corners(); ++i)
            {
                cornerIndices.push_back(curVertexIndex);
                factory.insertVertex(scvfGeom.corner(i));
                curVertexIndex++;
            }

            const auto sign = getSign(insideMarker, outsideMarker, scvf.unitOuterNormal());
            factory.insertElement(scvfGeom.type(), cornerIndices);
            fluxes.push_back(flux*sign
                                 /scvf.area()
                                 /elemVolVars[scvf.insideScvIdx()].extrusionFactor());
        }
    }

    auto grid = factory.createGrid();

    std::vector<double> fluxes_indexed(fluxes.size());
    for (const auto& e : elements(grid->leafGridView()))
        fluxes_indexed[grid->leafGridView().indexSet().index(e)] = fluxes[factory.insertionIndex(e)];

    Dune::VTKWriter<typename InterfaceGrid::LeafGridView> writer(grid->leafGridView());
    writer.addCellData(fluxes_indexed, "flux");
    writer.write("conforming_interface");
}

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // TODO: Runtime support for box ?
    using Solver = Dumux::SubDomainSolver<Properties::TTag::TYPETAG,
                                          Dumux::UMFPackBackend,
                                          true>;

    // instantiate solver
    Solver solver(0, "");
    solver.solve();
    solver.write(1.0);

    using FluxVariables = GetPropType<Properties::TTag::TYPETAG, Properties::FluxVariables>;
    writeInterfaceFluxVTU<FluxVariables>(solver.problem().gridGeometry(),
                                         solver.gridVariables(),
                                         solver.problem(),
                                         solver.solution());

    // finalize, print dumux message to say goodbye
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
}
