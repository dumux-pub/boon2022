import argparse
import subprocess

from preparemeshes import Mesher

permeabilityDomain = 1.0
permeabilityBarrier = 1e-4
permeabilityFault = 1e3

################################
# parse command-line arguments #
################################
parser = argparse.ArgumentParser(description='Domain decomposition test.')
parser.add_argument('-s', '--scheme', required=True,
                    choices=['tpfa', 'mpfa'],
                    help="Specify the discretization scheme to be used")
parser.add_argument('-m', '--meshtype', required=True,
                    choices=['tria', 'quad', 'tets', 'hex'],
                    help="Specify the type of mesh to be used\n"
                         "\t- tria: unstructured triangular grid\n"
                         "\t- quad: general quadrilateral grid\n"
                         "\t- tets: 3d case with tetrahedra\n")
parser.add_argument('-f', '--meshfolder', required=False,
                    default='',
                    help="Folder in which to store the mesh files")
parser.add_argument('-p', '--projectiontype', required=False,
                    choices=['flat', 'sharp'],
                    default='flat',
                    help='Choose between flat and sharp projection')
parser.add_argument('-n', '--nomeshgen',
                    required=False,
                    action='store_true',
                    help='Use this flag to skip mesh generation')
parser.add_argument('-v', '--verbosity',
                    required=False, action='store_true',
                    help='Use this flag for verbose output upon meshing')
args = vars(parser.parse_args())

# prepare meshes
dimension = 2 if args['meshtype'] in ['quad', 'tria'] else 3
mesher = Mesher(dimension, args['meshfolder'], verbose=args['verbosity'])

overallRefinement = 0.25 if dimension == 2 else 1.0
overallRefinement /= 2.5

refFacDomain = 1.0*overallRefinement  # 1.0/3.5*overallRefinement
refFacBarrier = 1.5*overallRefinement  # 1.0*overallRefinement
refFacFault = 0.6*overallRefinement  # 1.0/5.0*overallRefinement
refFacMortar = 1.25*overallRefinement  # 0.75*overallRefinement


class MeshSize:
    def get(self, typeString, id):
        if typeString == 'subdomain':
            return refFacDomain
        if typeString == 'barrier':
            return refFacBarrier
        if typeString == 'fault':
            return refFacFault
        if typeString == 'interface':
            if id < 6:
                return refFacBarrier*2.1
            return refFacDomain*2.1
        raise Exception("Unknown type string")


if args["nomeshgen"]:
    sdMeshes, bMeshes, fMeshes, iMeshes = mesher.getMeshFileNames()
    confMesh = mesher.getConformingMeshFileName()
else:
    print('Preparing meshes')
    useQuads = args['meshtype'] == 'quad'
    size = MeshSize()
    sdMeshes, bMeshes, fMeshes, iMeshes = mesher.prepareMeshes(size, useQuads)
    confMesh = mesher.prepareConformingMesh(refFacDomain,
                                            refFacBarrier,
                                            refFacFault,
                                            useQuads)

# make sure executable is up to date
print('Compiling executable')
exe = 'application_' + args['scheme'] + '_' + args['meshtype']
confExe = 'application_conforming_' + args['scheme'] + '_' + args['meshtype']
subprocess.run(['make', exe], check=True)
subprocess.run(['make', confExe], check=True)

# prepare call to dumux
dumuxCallArgs = ['./' + exe, 'params.input']

# add arguments for subdomains
for i, meshFile in enumerate(sdMeshes):
    paramGroup = 'SubDomain' + str(i)
    probName = 'domain_' + str(i)
    print("Subdomain {} (matrix) -> grid {}".format(i, meshFile))
    dumuxCallArgs.extend(['-' + paramGroup + '.Grid.File', meshFile,
                          '-' + paramGroup + '.Problem.Name', probName,
                          '-' + paramGroup + '.SpatialParams.Permeability',
                          str(permeabilityDomain)])

# add arguments for barriers
for i, meshFile in enumerate(bMeshes):
    sdIdx = i + len(sdMeshes)
    paramGroup = 'SubDomain' + str(sdIdx)
    probName = 'barrier_' + str(i)
    print("Subdomain {} (barrier) -> grid {}".format(sdIdx, meshFile))
    dumuxCallArgs.extend(['-' + paramGroup + '.Grid.File', meshFile,
                          '-' + paramGroup + '.Problem.Name', probName,
                          '-' + paramGroup + '.SpatialParams.Permeability',
                          str(permeabilityBarrier)])

# add arguments for faults
for i, meshFile in enumerate(fMeshes):
    sdIdx = i + len(sdMeshes) + len(bMeshes)
    paramGroup = 'SubDomain' + str(sdIdx)
    probName = 'fault_' + str(i)
    print("Subdomain {} (fault) -> grid {}".format(sdIdx, meshFile))
    dumuxCallArgs.extend(['-' + paramGroup + '.Grid.File', meshFile,
                          '-' + paramGroup + '.Problem.Name', probName,
                          '-' + paramGroup + '.SpatialParams.Permeability',
                          str(permeabilityFault)])

# add arguments for interfaces
for i, meshFile in enumerate(iMeshes):
    paramGroup = 'Interface' + str(i)
    probName = 'interface_' + str(i)
    print("Interface {} -> grid {}".format(i, meshFile))
    dumuxCallArgs.extend(['-' + paramGroup + '.Grid.File', meshFile,
                          '-' + paramGroup + '.Problem.Name', probName])

# set projector type
dumuxCallArgs.extend(['-Mortar.ProjectorType',
                      args['projectiontype'].capitalize()])


def setBoundingBox(callArgs):
    callArgs.extend(['-Problem.LowerLeft',
                     ' '.join("0" for i in range(dimension))])
    callArgs.extend(['-Problem.UpperRight',
                     ' '.join("1" for i in range(dimension))])


setBoundingBox(dumuxCallArgs)

# run simulation
print('Calling Dumux to run decomposed simulation')
subprocess.run(dumuxCallArgs, check=True)

print('Calling Dumux to run conforming simulation')
dumuxConfCallArgs = ['./' + confExe, 'params.input']
dumuxConfCallArgs.extend(['-Grid.File', confMesh])
dumuxConfCallArgs.extend(['-SpatialParams.DomainPermeability',
                          str(permeabilityDomain)])
dumuxConfCallArgs.extend(['-SpatialParams.BarrierPermeability',
                          str(permeabilityBarrier)])
dumuxConfCallArgs.extend(['-SpatialParams.FaultPermeability',
                          str(permeabilityFault)])
dumuxConfCallArgs.extend(['-Problem.Name', 'conforming'])

setBoundingBox(dumuxConfCallArgs)
subprocess.run(dumuxConfCallArgs, check=True)
