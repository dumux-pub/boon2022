Example 4: locally adapted grids
=========================

To reproduce the results for this example, go to
the build directory for this example and execute `python3 run.py --decomposition 4 4`.
To visualize the results, you may use the provided ParaView state files (files with
extension `.pvsm`).
