import sys
from numpy import zeros, array, real, mean, log, digitize, linspace
from numpy import abs as np_abs
from numpy.fft import fft2

if len(sys.argv) < 3:
    sys.stderr.write("Need two arguments: number of subdomains in x- and y-direction\n")
    sys.exit(1)

base_mesh_file_name = "mesh" if len(sys.argv) < 4 else sys.argv[3]
num_subdomains_x = int(sys.argv[1])
num_subdomains_y = int(sys.argv[2])

perm_file = "spe_perm.dat"  # taken from https://www.spe.org/web/csp/datasets/set02.htm#download
num_cells_x = 60
num_cells_y = 60
num_cells_z = 85
layer_idx = 55
perm_dir = 0

offset = perm_dir*(num_cells_x*num_cells_y*num_cells_z)
perm_values = [float(v) for v in open(perm_file).read().split()]
perm_values = array([
    array([v for v in perm_values[offset + (layer_idx+i)*num_cells_x:offset + (layer_idx+i+1)*num_cells_x]])
    for i in range(num_cells_y)
])

if num_cells_x%num_subdomains_x != 0:
    sys.stderr.write("Number of subdomains in x-direction not supported")
    sys.exit(1)
if num_cells_y%num_subdomains_y != 0:
    sys.stderr.write("Number of subdomains in y-direction not supported")
    sys.exit(1)

num_total_subdomains = num_subdomains_x*num_subdomains_y
n_cells_x_per_subdomain = int(num_cells_x/num_subdomains_x)
n_cells_y_per_subdomain = int(num_cells_y/num_subdomains_y)

def _get_sd_perms(sd_id_x, sd_id_y):
    cell_offset_x = n_cells_x_per_subdomain*sd_id_x
    cell_offset_y = n_cells_y_per_subdomain*sd_id_y
    return perm_values[
        cell_offset_y:cell_offset_y+n_cells_y_per_subdomain,
        cell_offset_x:cell_offset_x+n_cells_x_per_subdomain,
    ]

domain_to_spatial_freq = {
    (_x, _y): mean(np_abs(real(fft2(_get_sd_perms(_x, _y)))))
    for _y in range(num_subdomains_y)
    for _x in range(num_subdomains_x)
}
min_freq = min(domain_to_spatial_freq.values())
max_freq = max(domain_to_spatial_freq.values())
num_freq_bins = 3
freq_thresholds = [i*(max_freq-min_freq)/num_freq_bins for i in range(1,num_freq_bins+1)]

def _get_resample_level(sd_x_index, sd_y_index):
    spatial_freq = domain_to_spatial_freq[(sd_x_index, sd_y_index)]
    index = digitize(spatial_freq, freq_thresholds)
    level = index
    print("Subdomain {}/{} is resampled {} level(s)".format(
        sd_x_index, sd_y_index, level
    ))
    return level

def _resample_array(input_array, level=0):
    if level == 0:
        return input_array
    if level < 0:
        # downsampling
        level = abs(level)
        _dx = pow(2, level)
        _nx = int(input_array.shape[1]/_dx)
        _ny = int(input_array.shape[0]/_dx)
        _dx = input_array.shape[1]/_nx
        _dy = input_array.shape[0]/_ny
        new_array = zeros(dtype=float, shape=(_ny, _nx))
        for j in range(_ny):
            for i in range(_nx):
                x0_idx = int(_dx*i + 1e-6)
                y0_idx = int(_dy*j + 1e-6)
                nx = round(_dx)
                ny = round(_dy)
                new_array[j, i] = mean(
                    input_array[
                        y0_idx:y0_idx+ny,
                        x0_idx:x0_idx+nx
                    ]
                )
        return new_array
    else:
        # upsampling
        _dx = 1.0/pow(2, level)
        _nx = int(input_array.shape[1]/_dx)
        _ny = int(input_array.shape[0]/_dx)
        _dx = input_array.shape[1]/_nx
        _dy = input_array.shape[0]/_ny
        new_array = zeros(dtype=float, shape=(_ny, _nx))
        for j in range(_ny):
            for i in range(_nx):
                x0_idx = int(i*_dx + 1e-6)
                y0_idx = int(j*_dy + 1e-6)
                nx = round(_dx)
                ny = round(_dy)
                new_array[j, i] = input_array[y0_idx, x0_idx]
        return new_array

permeabilities = []
physical_id = 0
for domain_id_y in range(num_subdomains_y):
    for domain_id_x in range(num_subdomains_x):
        subdomain_id = domain_id_y*num_subdomains_x + domain_id_x
        downsample_level = _get_resample_level(domain_id_x, domain_id_y)
        _orig_perms = _get_sd_perms(domain_id_x, domain_id_y)
        _perm_values = _resample_array(_orig_perms, downsample_level)

        _nx = _perm_values.shape[1]
        _ny = _perm_values.shape[0]
        _dx = _orig_perms.shape[1]/_perm_values.shape[1]
        _dy = _orig_perms.shape[0]/_perm_values.shape[0]
        _x_offset = n_cells_x_per_subdomain*domain_id_x
        _y_offset = n_cells_y_per_subdomain*domain_id_y
        with open(f"{base_mesh_file_name}_{subdomain_id}.msh", "w") as mesh_file:
            mesh_file.write("$MeshFormat\n")
            mesh_file.write("2.2 0 8\n")
            mesh_file.write("$EndMeshFormat\n")

            mesh_file.write("$Nodes\n")
            mesh_file.write("{}\n".format((_nx+1)*(_ny+1)))
            for j in range(_ny+1):
                for i in range(_nx+1):
                    mesh_file.write(
                        f"{j*(_nx+1) + i} "
                        f"{_x_offset + i*_dx} "
                        f"{_y_offset + j*_dy} 0.0\n"
                    )
            mesh_file.write("$EndNodes\n")

            mesh_file.write("$Elements\n")
            mesh_file.write("{}\n".format(_nx*_ny))
            for j in range(_ny):
                for i in range(_nx):
                    mesh_file.write(
                        f"{j*_nx + i} 3 1 "
                        f"{physical_id} "
                        f"{j*(_nx+1) + i} {j*(_nx+1) + i + 1} "
                        f"{(j+1)*(_nx+1) + i + 1} {(j+1)*(_nx+1) + i}\n"
                    )
                    physical_id += 1
                    permeabilities.append(_perm_values[j, i])
            mesh_file.write("$EndElements\n")

with open("physical_id_to_permeability_map.txt", "w") as phys_id_map:
    for phys_id, k in enumerate(permeabilities):
        phys_id_map.write(f"{phys_id} {k}\n")
