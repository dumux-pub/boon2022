// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \todo TODO: doc me
 */
#include <config.h>

#include <examples/common/initialization.hh>
#include <examples/common/interface.hh>
#include <examples/common/subdomainsolver.hh>
#include <examples/common/solver.hh>
#include <examples/common/types.hh>

#include "properties.hh"

#ifndef MORTARBASISORDER
#define MORTARBASISORDER 1
#endif

namespace Dumux {

template<class SubDomainSolver, class Interface>
void solveAndWriteOutput(ContainerOfPointers<SubDomainSolver> subDomainSolvers,
                         ContainerOfPointers<Interface> interfaces)
{
    using InterfaceSolution = typename Interface::Traits::SolutionVector;

    IterativeMortarSolver<InterfaceSolution> solver;
    const auto [interfaceSolution, iterations] = solver.solve(interfaces, subDomainSolvers);

    std::vector<InterfaceSolution> interfaceSolutions;
    extractInterfaceSolutions(interfaces, interfaceSolutions, interfaceSolution);

    MortarVTKOutput vtkWriter(subDomainSolvers, interfaces);
    vtkWriter.addInterfaceFields("mortar_variable", interfaceSolutions);
    vtkWriter.write();
}

template<class Projector, class SubDomainSolver, class InterfaceTraits>
void solveIteratively()
{
    auto [sdSolvers, interfaces] = makeSubDomainSolversAndInterfaces<Projector,
                                                                     InterfaceTraits,
                                                                     SubDomainSolver>();
    solveAndWriteOutput(sdSolvers, interfaces);
}

} // namespace Dumux

struct SPEInterfaceTraits
{
    static constexpr int basisOrder = MORTARBASISORDER;
    using Scalar = double;
    using Grid = INTERFACEGRID;
    using GridView = typename Grid::LeafGridView;
    using BlockType = Dune::FieldVector<Scalar, 1>;
    using SolutionVector = Dune::BlockVector<BlockType>;
    using FEBasis = Dune::Functions::LagrangeBasis<GridView, basisOrder>;
    using GridGeometry = Dumux::FEGridGeometry<FEBasis>;
    static constexpr Dune::VTK::DataMode VTKDataMode = Dune::VTK::conforming;
};

int main(int argc, char** argv)
{
    using namespace Dumux;

    // we use UMFPack here because on very fine grids the iterative solvers struggle
    using InterfaceTraits = SPEInterfaceTraits;
    using SubDomainSolver =  Dumux::SubDomainSolver<
        Properties::TTag::SubDomainSolverOnePMpfa,
        Dumux::UMFPackBackend,
        true
    >;

    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);
    Parameters::init(argc, argv);

    const auto projectorType = getParam<std::string>("Mortar.ProjectorType");
    if (projectorType == "Flat")
    {
        using P = FlatProjector<typename InterfaceTraits::SolutionVector>;
        solveIteratively<P, SubDomainSolver, InterfaceTraits>();
    }
    else if (projectorType == "Sharp")
    {
        using P = SharpProjector<typename InterfaceTraits::SolutionVector>;
        solveIteratively<P, SubDomainSolver, InterfaceTraits>();
    }
    else
        DUNE_THROW(Dune::NotImplemented, "Unknown projector type");

    if (mpiHelper.rank() == 0)
        Parameters::print();

    return 0;
}
