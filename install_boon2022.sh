#!/usr/bin/env python3

# 
# This installs the module boon2022 and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |         dumux         |  origin/releases/3.4  |  f78393cff93e246d736819d9f6efc261ef733a1f  |  2021-11-30 18:47:58 +0000  |
# |      dune-alugrid     |  origin/releases/2.8  |  ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f  |  2021-08-22 11:59:22 +0200  |
# |      dune-common      |  origin/releases/2.8  |  fb179f9420efb44ba76bd6219823e436ef63122a  |  2021-09-24 05:38:46 +0000  |
# |     dune-foamgrid     |     origin/master     |  43bfdb6181fae187fd803eca935a030d8d5ab0bc  |  2021-07-03 20:20:08 +0000  |
# |     dune-geometry     |  origin/releases/2.8  |  e7bfb66e48496aa28e47974c33ea9a4579bf723b  |  2021-08-31 17:51:20 +0000  |
# |       dune-grid       |  origin/releases/2.8  |  e3371946f18df31d4ad542e5e7b51f652954edbc  |  2021-10-26 11:13:47 +0000  |
# |       dune-istl       |  origin/releases/2.8  |  fffb544a61d2c65a0d2fc7c751f36909f06be8f5  |  2021-08-31 13:58:37 +0000  |
# |  dune-localfunctions  |  origin/releases/2.8  |  f6628171b2773065ab43f97a77f47cd8c4283d8f  |  2021-08-31 14:03:38 +0000  |
# |      dune-uggrid      |  origin/releases/2.8  |  bc2d1229420367563410ce9e519f5ff82b45266f  |  2021-08-31 19:10:14 +0000  |
# |     dune-typetree     |  origin/releases/2.8  |  cf6b5b4d3abe5dfa748815a69f6a8bda02af36f0  |  2021-08-31 18:58:40 +0000  |
# |     dune-functions    |  origin/releases/2.8  |  baf111ec91dc2a45124fd168aaca94758ab127c3  |  2021-11-07 05:27:03 +0000  |

import os
import sys
import subprocess

top = "."
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(f'Skip cloning {url} since target folder "{targetFolder}" already exists.')


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/releases/3.4", "f78393cff93e246d736819d9f6efc261ef733a1f")

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid.git", "origin/releases/2.8", "ffd6cd15dc7092593c3d5827c2a2b43b81d7a32f")

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.8", "fb179f9420efb44ba76bd6219823e436ef63122a")

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/master", "43bfdb6181fae187fd803eca935a030d8d5ab0bc")

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.8", "e7bfb66e48496aa28e47974c33ea9a4579bf723b")

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.8", "e3371946f18df31d4ad542e5e7b51f652954edbc")

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.8", "fffb544a61d2c65a0d2fc7c751f36909f06be8f5")

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.8", "f6628171b2773065ab43f97a77f47cd8c4283d8f")

print("Installing dune-uggrid")
installModule("dune-uggrid", "https://gitlab.dune-project.org/staging/dune-uggrid.git", "origin/releases/2.8", "bc2d1229420367563410ce9e519f5ff82b45266f")

print("Installing dune-typetree")
installModule("dune-typetree", "https://gitlab.dune-project.org/staging/dune-typetree", "origin/releases/2.8", "cf6b5b4d3abe5dfa748815a69f6a8bda02af36f0")

print("Installing dune-functions")
installModule("dune-functions", "https://gitlab.dune-project.org/staging/dune-functions", "origin/releases/2.8", "baf111ec91dc2a45124fd168aaca94758ab127c3")

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux/docker.opts', 'all'],
    '.'
)
